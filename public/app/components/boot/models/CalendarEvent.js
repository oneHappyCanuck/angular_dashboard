define(['components/boot/module'], function(module){

    "use strict";


    module.factory('CalendarEvent', function($resource){
        return $resource( 'api/events.json', {_id:'@id'})
    });

});