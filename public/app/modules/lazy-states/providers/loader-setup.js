define(['modules/lazy-states/module', 'lodash'], function(module, _){

    "use strict";

    module.provider('loaderSetup', function($futureStateProvider, $ocLazyLoadProvider) {

        var init = function() {

            $ocLazyLoadProvider.config({
                // NOTE: uncomment to see more lazy-loading messages from the $ocLazyLoad
                // debug: true,
                jsLoader: requirejs,
                loadedModules: ['app'],
                modules: [{
                    name: 'app.inbox',
                    files: ['components/inbox/module',
                        'components/inbox/models/InboxMessage'
                    ]
                }, {
                    name: 'app.calendar',
                    files: ['components/calendar/module',
                        'components/calendar/directives/fullCalendar',
                        'components/calendar/directives/dragableEvent',
                        'components/calendar/controllers/CalendarCtrl'
                    ]
                }]
            });

            // Factory for use with ui-router-extras $futureStateProvider
            var ocLazyLoadStateFactory = function ($q, $ocLazyLoad, futureState) {
                var deferred = $q.defer();
                $ocLazyLoad.load(futureState.module).then(function (name) {
                    deferred.resolve();
                }, function () {
                    deferred.reject();
                });
                return deferred.promise;
            };

            $futureStateProvider.stateFactory('ocLazyLoad', ocLazyLoadStateFactory);

        };

        return {
            init: init,
            $get: function() {
                return this;
            }
        }

    });
});