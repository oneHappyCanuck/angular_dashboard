define([
    'angular',
    'angular-couch-potato',
    'lodash'
    ], function (ng, couchPotato, _) {

    "use strict";


    var module = ng.module('app.boot', ['ngResource']);

    // Optional: depends on whether you use couch potato primitives on the boot module
    // couchPotato.configureApp(module);

    return module;
});