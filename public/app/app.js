'use strict';

/**
 * @ngdoc overview
 * @name app [smartadminApp]
 * @description
 * # app [smartadminApp]
 *
 * Main module of the application.
 */

define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'ui-router-extras',
    'angular-animate',
    'angular-bootstrap',
    'smartwidgets',
    'notification',

    'components/boot/module',
    'modules/lazy-states/module',
    'modules/lazy-states/providers/lazy-states',
    'modules/lazy-states/providers/loader-setup'

], function (ng, couchPotato) {

    var app = ng.module('app', [
        'ngSanitize',

        'scs.couch-potato',
        'ngAnimate',

        // NOTE: ui-router acts in unison with the extras and ocLazyLoad packages
        'ui.router',
        'ct.ui.router.extras',

        'ui.bootstrap',
        // App
        'app.auth',
        'app.layout',
        'app.chat',
        'app.dashboard',

        'app.graphs',
        'app.tables',
        'app.forms',
        'app.ui',
        'app.widgets',
        'app.maps',
        'app.appViews',
        'app.misc',
        'app.smartAdmin',

        // NOTE: uncomment for eager loading
        // 'app.inbox',
        // 'app.calendar',

        // NOTE: comment to disable lazy loading'
        'app.boot',
        'app.lazy-states'
    ]);

    couchPotato.configureApp(app);

    app.config(function ($provide, $httpProvider, $futureStateProvider, loaderSetupProvider, lazyStatesProvider) {

        $futureStateProvider.addResolve([function() {

            loaderSetupProvider.init();

            lazyStatesProvider.loadAll();


        }]);

        // Intercept http calls.
        $provide.factory('ErrorHttpInterceptor', function ($q) {
            var errorCounter = 0;
            function notifyError(rejection){
                console.log(rejection);
                $.bigBox({
                    title: rejection.status + ' ' + rejection.statusText,
                    content: rejection.data,
                    color: "#C46A69",
                    icon: "fa fa-warning shake animated",
                    number: ++errorCounter,
                    timeout: 6000
                });
            }

            return {
                // On request failure
                requestError: function (rejection) {
                    // show notification
                    notifyError(rejection);

                    // Return the promise rejection.
                    return $q.reject(rejection);
                },

                // On response failure
                responseError: function (rejection) {
                    // show notification
                    notifyError(rejection);
                    // Return the promise rejection.
                    return $q.reject(rejection);
                }
            };
        });

        // Add the interceptor to the $httpProvider.
        $httpProvider.interceptors.push('ErrorHttpInterceptor');

    });

    app.run(function ($couchPotato, $rootScope, $state, $stateParams) {
        app.lazy = $couchPotato;
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        // editableOptions.theme = 'bs3';
    });

    return app;
});
