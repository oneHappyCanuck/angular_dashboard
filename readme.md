# Angular Dashboard #

SmartAdmin customized to include lazy ui-router states.

## Lazy Architecture ##

A lazy-states module was added with 2 providers to provide a lazy architecture. Please see 2 providers, namely,
[`lazyStatesProvider`](angular_dashboard/src/master/public/app/modules/lazy-states/providers/lazy-states.js)
and [`loaderSetupProvider`](angular_dashboard/src/master/public/app/modules/lazy-states/providers/loader-setup.js).

The first modules that were adapted to lazy loading are `inbox` and `calendar`. The process
of converting these or any other modules to be lazy loaded includes the following steps:

1. Define the files involved for the angular module in `loader-setup.js`, for instance, `app.calendar` state comprises
`components/calendar/module`, `components/calendar/directives/fullCalendar`, `components/calendar/directives/dragableEvent`,
and `components/calendar/controllers/CalendarCtrl`.
2. Define a `$futureState.futureState` entry corresponding to the angular module with a type of `ocLazyLoad`, for instance,
```js
$futureStateProvider.futureState({
    'stateName': 'app.calendar',
    'url': '/calendar',
    'type': 'ocLazyLoad',
    'module': 'app.calendar'
});
```
3. Remove the eager loading of the corresponding module from [`includes.js`](public/app/includes.js).
4. One intricacy that can arise while doing such migrations is that part of a lazy-loaded module was eagerly required
by the `dashboard` or `layout` module. In that case, move those out of lazy loaded module to `app.boot` module, newly added
under components. Use the moved over components under boot components from there instead. In case of `inbox` and `calendar`,
there were couple such dependencies.
