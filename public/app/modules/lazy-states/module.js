define(['angular',
    'angular-ui-router',
    'ui-router-extras',
    'oclazyload',
    'components/boot/module'
    ], function (ng) {

    "use strict";


    var module = ng.module('app.lazy-states', ['ui.router', 'ct.ui.router.extras', 'oc.lazyLoad', 'app.boot']);

    module.config(function() {

    });

    return module;

});