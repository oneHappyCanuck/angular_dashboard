



var appConfig = {};

appConfig.menu_speed = 200;


appConfig.smartSkin = "smart-style-0";

appConfig.skins = [
    {name: "smart-style-0",
        logo: "styles/img/logo.png",
        class: "btn btn-block btn-xs txt-color-white margin-right-5",
        style: "background-color:#4E463F;",
        label: "Smart Default"},

    {name: "smart-style-1",
        logo: "styles/img/logo-white.png",
        class: "btn btn-block btn-xs txt-color-white",
        style: "background:#3A4558;",
        label: "Dark Elegance"},

    {name: "smart-style-2",
        logo: "styles/img/logo-blue.png",
        class: "btn btn-xs btn-block txt-color-darken margin-top-5",
        style: "background:#fff;",
        label: "Ultra Light"},

    {name: "smart-style-3",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background:#f78c40",
        label: "Google Skin"},

    {name: "smart-style-4",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: #bbc0cf; border: 1px solid #59779E; color: #17273D !important;",
        label: "PixelSmash"},

    {name: "smart-style-5",
        logo: "styles/img/logo-pale.png",
        class: "btn btn-xs btn-block txt-color-white margin-top-5",
        style: "background: rgba(153, 179, 204, 0.2); border: 1px solid rgba(121, 161, 221, 0.8); color: #17273D !important;",
        label: "Glass"}
];



appConfig.sound_path = "sound/";
appConfig.sound_on = true; 


/*
* DEBUGGING MODE
* debugState = true; will spit all debuging message inside browser console.
* The colors are best displayed in chrome browser.
*/


appConfig.debugState = false;	
appConfig.debugStyle = 'font-weight: bold; color: #00f;';
appConfig.debugStyle_green = 'font-weight: bold; font-style:italic; color: #46C246;';
appConfig.debugStyle_red = 'font-weight: bold; color: #ed1c24;';
appConfig.debugStyle_warning = 'background-color:yellow';
appConfig.debugStyle_success = 'background-color:green; font-weight:bold; color:#fff;';
appConfig.debugStyle_error = 'background-color:#ed1c24; font-weight:bold; color:#fff;';


appConfig.voice_command = true;
appConfig.voice_command_auto = false;

/*
 *  Sets the language to the default 'en-US'. (supports over 50 languages 
 *  by google)
 * 
 *  Afrikaans         ['af-ZA']
 *  Bahasa Indonesia  ['id-ID']
 *  Bahasa Melayu     ['ms-MY']
 *  CatalГ            ['ca-ES']
 *  ДЊeЕЎtina         ['cs-CZ']
 *  Deutsch           ['de-DE']
 *  English           ['en-AU', 'Australia']
 *                    ['en-CA', 'Canada']
 *                    ['en-IN', 'India']
 *                    ['en-NZ', 'New Zealand']
 *                    ['en-ZA', 'South Africa']
 *                    ['en-GB', 'United Kingdom']
 *                    ['en-US', 'United States']
 *  EspaГ±ol          ['es-AR', 'Argentina']
 *                    ['es-BO', 'Bolivia']
 *                    ['es-CL', 'Chile']
 *                    ['es-CO', 'Colombia']
 *                    ['es-CR', 'Costa Rica']
 *                    ['es-EC', 'Ecuador']
 *                    ['es-SV', 'El Salvador']
 *                    ['es-ES', 'EspaГ±a']
 *                    ['es-US', 'Estados Unidos']
 *                    ['es-GT', 'Guatemala']
 *                    ['es-HN', 'Honduras']
 *                    ['es-MX', 'MГ©xico']
 *                    ['es-NI', 'Nicaragua']
 *                    ['es-PA', 'PanamГЎ']
 *                    ['es-PY', 'Paraguay']
 *                    ['es-PE', 'PerГє']
 *                    ['es-PR', 'Puerto Rico']
 *                    ['es-DO', 'RepГєblica Dominicana']
 *                    ['es-UY', 'Uruguay']
 *                    ['es-VE', 'Venezuela']
 *  Euskara           ['eu-ES']
 *  FranГ§ais         ['fr-FR']
 *  Galego            ['gl-ES']
 *  Hrvatski          ['hr_HR']
 *  IsiZulu           ['zu-ZA']
 *  ГЌslenska         ['is-IS']
 *  Italiano          ['it-IT', 'Italia']
 *                    ['it-CH', 'Svizzera']
 *  Magyar            ['hu-HU']
 *  Nederlands        ['nl-NL']
 *  Norsk bokmГҐl     ['nb-NO']
 *  Polski            ['pl-PL']
 *  PortuguГЄs        ['pt-BR', 'Brasil']
 *                    ['pt-PT', 'Portugal']
 *  RomГўnДѓ          ['ro-RO']
 *  SlovenДЌina       ['sk-SK']
 *  Suomi             ['fi-FI']
 *  Svenska           ['sv-SE']
 *  TГјrkГ§e          ['tr-TR']
 *  Р±СЉР»РіР°СЂСЃРєРё['bg-BG']
 *  PСѓСЃСЃРєРёР№     ['ru-RU']
 *  РЎСЂРїСЃРєРё      ['sr-RS']
 *  н•њкµ­м–ґ         ['ko-KR']
 *  дё­ж–‡            ['cmn-Hans-CN', 'ж™®йЂљиЇќ (дё­е›Ѕе¤§й™†)']
 *                    ['cmn-Hans-HK', 'ж™®йЂљиЇќ (й¦™жёЇ)']
 *                    ['cmn-Hant-TW', 'дё­ж–‡ (еЏ°зЃЈ)']
 *                    ['yue-Hant-HK', 'зІµиЄћ (й¦™жёЇ)']
 *  ж—Ґжњ¬иЄћ         ['ja-JP']
 *  Lingua latД«na    ['la']
 */
appConfig.voice_command_lang = 'en-US';
/*
 *  Use localstorage to remember on/off (best used with HTML Version)
 */ 
appConfig.voice_localStorage = false;
/*
 * Voice Commands
 * Defines all voice command variables and functions
 */ 
if (appConfig.voice_command) {
        
     	appConfig.commands = {
                
        'show dashboard' : function() { window.location.hash = "dashboard" },
        'show inbox' : function() {  window.location.hash = "inbox/" },
        'show graphs' : function() {  window.location.hash = "graphs/flot" },
        'show flotchart' : function() { window.location.hash = "graphs/flot" },
        'show morris chart' : function() { window.location.hash = "graphs/morris" },
        'show inline chart' : function() { window.location.hash = "graphs/inline-charts" },
        'show dygraphs' : function() { window.location.hash = "graphs/dygraphs" },
        'show tables' : function() { window.location.hash = "tables/table" },
        'show data table' : function() { window.location.hash = "tables/datatable" },
        'show jquery grid' : function() { window.location.hash = "tables/jqgrid" },
        'show form' : function() { window.location.hash = "forms/form-elements" },
        'show form layouts' : function() { window.location.hash = "forms/form-templates" },
        'show form validation' : function() { window.location.hash = "forms/validation" },
        'show form elements' : function() { window.location.hash = "forms/bootstrap-forms" },
        'show form plugins' : function() { window.location.hash = "forms/plugins" },
        'show form wizards' : function() { window.location.hash = "forms/wizards" },
        'show bootstrap editor' : function() { window.location.hash = "forms/other-editors" },
        'show dropzone' : function() { window.location.hash = "forms/dropzone" },
        'show image cropping' : function() { window.location.hash = "forms/image-editor" },
        'show general elements' : function() { window.location.hash = "ui/general-elements" },
        'show buttons' : function() { window.location.hash = "ui/buttons" },
        'show fontawesome' : function() { window.location.hash = "ui/icons/fa" },
        'show glyph icons' : function() { window.location.hash = "ui/icons/glyph" },
        'show flags' : function() { window.location.hash = "ui/icons/flags" },
        'show grid' : function() { window.location.hash = "ui/grid" },
        'show tree view' : function() { window.location.hash = "ui/treeview" },
        'show nestable lists' : function() { window.location.hash = "ui/nestable-list" },
        'show jquery U I' : function() { window.location.hash = "ui/jqui" },
        'show typography' : function() { window.location.hash = "ui/typography" },
        'show calendar' : function() { window.location.hash = "calendar" },
        'show widgets' : function() { window.location.hash = "widgets" },
        'show gallery' : function() { window.location.hash = "gallery" },
        'show maps' : function() { window.location.hash = "gmap-xml" },
        'go back' :  function() { history.back(1); }, 
        'scroll up' : function () { $('html, body').animate({ scrollTop: 0 }, 100); },
        'scroll down' : function () { $('html, body').animate({ scrollTop: $(document).height() }, 100);},
        'hide navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'show navigation' : function() { 
            if ($( ":root" ).hasClass("container") && !$( ":root" ).hasClass("menu-on-top")){
                $('span.minifyme').trigger("click");
            } else {
                $('#hide-menu > span > a').trigger("click"); 
            }
        },
        'mute' : function() {
            appConfig.sound_on = false;
            $.smallBox({
                title : "MUTE",
                content : "All sounds have been muted!",
                color : "#a90329",
                timeout: 4000,
                icon : "fa fa-volume-off"
            });
        },
        'sound on' : function() {
            appConfig.sound_on = true;
            $.speechApp.playConfirmation();
            $.smallBox({
                title : "UNMUTE",
                content : "All sounds have been turned on!",
                color : "#40ac2b",
                sound_file: 'voice_alert',
                timeout: 5000,
                icon : "fa fa-volume-up"
            });
        },
        'stop' : function() {
            smartSpeechRecognition.abort();
            $( ":root" ).removeClass("voice-command-active");
            $.smallBox({
                title : "VOICE COMMAND OFF",
                content : "Your voice commands has been successfully turned off. Click on the <i class='fa fa-microphone fa-lg fa-fw'></i> icon to turn it back on.",
                color : "#40ac2b",
                sound_file: 'voice_off',
                timeout: 8000,
                icon : "fa fa-microphone-slash"
            });
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }
        },
        'help' : function() {

            $('#voiceModal').removeData('modal').modal( { remote: "app/layout/partials/voice-commands.tpl.html", show: true } );
            if ($('#speech-btn .popover').is(':visible')) {
                $('#speech-btn .popover').fadeOut(250);
            }

        },      
        'got it' : function() {
            $('#voiceModal').modal('hide');
        },  
        'logout' : function() {
            $.speechApp.stop();
            window.location = $('#logout > span > a').attr("href");
        }
    }; 
    
};


/*
* END APP.appConfig
*/;
define("appConfig", function(){});

define('app',["angular","angular-couch-potato","angular-ui-router","angular-animate","angular-bootstrap","smartwidgets","notification"],function(a,b){var c=a.module("app",["ngSanitize","scs.couch-potato","ngAnimate","ui.router","ct.ui.router.extras","oc.lazyLoad","ui.bootstrap","app.auth","app.layout","app.chat","app.dashboard","app.graphs","app.tables","app.forms","app.ui","app.widgets","app.maps","app.appViews","app.misc","app.smartAdmin","app.lazy-states","smart-templates"]);return b.configureApp(c),c.config(["$provide","$httpProvider",function(a,b){a.factory("ErrorHttpInterceptor",["$q",function(a){function b(a){console.log(a),$.bigBox({title:a.status+" "+a.statusText,content:a.data,color:"#C46A69",icon:"fa fa-warning shake animated",number:++c,timeout:6e3})}var c=0;return{requestError:function(c){return b(c),a.reject(c)},responseError:function(c){return b(c),a.reject(c)}}}]),b.interceptors.push("ErrorHttpInterceptor")}]),c.run(["$couchPotato","$rootScope","$state","$stateParams",function(a,b,d,e){c.lazy=a,b.$state=d,b.$stateParams=e}]),c});
define('auth/module',["angular","angular-couch-potato","angular-ui-router","angular-google-plus","angular-easyfb"],function(a,b){var c=a.module("app.auth",["ui.router"]);b.configureApp(c);var d={googleClientId:"678402726462-ah1p6ug0klf9jm8cplefmphfupg3bg2h.apps.googleusercontent.com",facebookAppId:"620275558085318"};return c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("realLogin",{url:"/real-login",views:{root:{templateUrl:"build/auth/login/login.html",controller:"LoginCtrl",resolve:{deps:b.resolveDependencies(["auth/models/User","auth/directives/loginInfo","auth/login/LoginCtrl","auth/login/directives/facebookSignin","auth/login/directives/googleSignin"])}}},data:{title:"Login",rootId:"extra-page"}}).state("login",{url:"/login",views:{root:{templateUrl:"build/auth/views/login.html"}},data:{title:"Login",htmlId:"extr-page"},resolve:{deps:b.resolveDependencies(["modules/forms/directives/validate/smartValidateForm"])}}).state("register",{url:"/register",views:{root:{templateUrl:"build/auth/views/register.html"}},data:{title:"Register",htmlId:"extr-page"},resolve:{deps:b.resolveDependencies(["modules/forms/directives/validate/smartValidateForm"])}}).state("forgotPassword",{url:"/forgot-password",views:{root:{templateUrl:"build/auth/views/forgot-password.html"}},data:{title:"Forgot Password",htmlId:"extr-page"},resolve:{deps:b.resolveDependencies(["modules/forms/directives/validate/smartValidateForm"])}}).state("lock",{url:"/lock",views:{root:{templateUrl:"build/auth/views/lock.html"}},data:{title:"Locked Screen",htmlId:"lock-page"}})}]).constant("authKeys",d),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('auth/models/User',["auth/module"],function(a){return a.registerFactory("User",["$http","$q",function(a,b){var c=b.defer(),d={initialized:c.promise,username:void 0,picture:void 0};return a.get("api/user.json").then(function(a){d.username=a.data.username,d.picture=a.data.picture,c.resolve(d)}),d}])});
define('layout/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.layout",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider","$urlRouterProvider",function(a,b,c){a.state("app",{"abstract":!0,views:{root:{templateUrl:"build/layout/layout.tpl.html",resolve:{deps:b.resolveDependencies(["auth/directives/loginInfo","modules/graphs/directives/inline/sparklineContainer","components/chat/api/ChatApi","components/chat/directives/asideChatWidget"])}}}}),c.otherwise("/dashboard")}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('layout/actions/minifyMenu',["layout/module"],function(a){a.registerDirective("minifyMenu",function(){return{restrict:"A",link:function(a,b){var c=$("body"),d=function(){c.hasClass("menu-on-top")||(c.toggleClass("minified"),c.removeClass("hidden-menu"),$("html").removeClass("hidden-menu-mobile-lock"))};b.on("click",d)}}})});
define('layout/actions/toggleMenu',["layout/module"],function(a){a.registerDirective("toggleMenu",function(){return{restrict:"A",link:function(a,b){var c=$("body"),d=function(){c.hasClass("menu-on-top")?c.hasClass("menu-on-top")&&c.hasClass("mobile-view-activated")&&($("html").toggleClass("hidden-menu-mobile-lock"),c.toggleClass("hidden-menu"),c.removeClass("minified")):($("html").toggleClass("hidden-menu-mobile-lock"),c.toggleClass("hidden-menu"),c.removeClass("minified"))};b.on("click",d),a.$on("requestToggleMenu",function(){d()})}}})});
define('layout/actions/fullScreen',["layout/module"],function(a){a.registerDirective("fullScreen",function(){return{restrict:"A",link:function(a,b){var c=$("body"),d=function(){c.hasClass("full-screen")?(c.removeClass("full-screen"),document.exitFullscreen?document.exitFullscreen():document.mozCancelFullScreen?document.mozCancelFullScreen():document.webkitExitFullscreen&&document.webkitExitFullscreen()):(c.addClass("full-screen"),document.documentElement.requestFullscreen?document.documentElement.requestFullscreen():document.documentElement.mozRequestFullScreen?document.documentElement.mozRequestFullScreen():document.documentElement.webkitRequestFullscreen?document.documentElement.webkitRequestFullscreen():document.documentElement.msRequestFullscreen&&document.documentElement.msRequestFullscreen())};b.on("click",d)}}})});
define('layout/actions/resetWidgets',["layout/module"],function(a){a.registerDirective("resetWidgets",["$state",function(){return{restrict:"A",link:function(a,b){b.on("click",function(){$.SmartMessageBox({title:"<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",content:"Would you like to RESET all your saved widgets and clear LocalStorage?1",buttons:"[No][Yes]"},function(a){"Yes"==a&&localStorage&&(localStorage.clear(),location.reload())})})}}}])});
define('layout/actions/searchMobile',["layout/module"],function(a){a.registerDirective("searchMobile",function(){return{restrict:"A",compile:function(a){a.removeAttr("search-mobile data-search-mobile"),a.on("click",function(a){$("body").addClass("search-mobile"),a.preventDefault()}),$("#cancel-search-js").on("click",function(a){$("body").removeClass("search-mobile"),a.preventDefault()})}}})});
define('layout/directives/demo/demoStates',["layout/module","lodash","notification"],function(a,b){a.registerDirective("demoStates",["$rootScope",function(a){return{restrict:"E",replace:!0,templateUrl:"build/layout/directives/demo/demo-states.tpl.html",scope:!0,link:function(a,b){b.parent().css({position:"relative"}),b.on("click","#demo-setting",function(){b.toggleClass("activate")})},controller:["$scope",function(c){var d=$("body");c.$watch("fixedHeader",function(a){localStorage.setItem("sm-fixed-header",a),d.toggleClass("fixed-header",a),0==a&&(c.fixedRibbon=!1,c.fixedNavigation=!1)}),c.$watch("fixedNavigation",function(a){localStorage.setItem("sm-fixed-navigation",a),d.toggleClass("fixed-navigation",a),a?(c.insideContainer=!1,c.fixedHeader=!0):c.fixedRibbon=!1}),c.$watch("fixedRibbon",function(a){localStorage.setItem("sm-fixed-ribbon",a),d.toggleClass("fixed-ribbon",a),a&&(c.fixedHeader=!0,c.fixedNavigation=!0,c.insideContainer=!1)}),c.$watch("fixedPageFooter",function(a){localStorage.setItem("sm-fixed-page-footer",a),d.toggleClass("fixed-page-footer",a)}),c.$watch("insideContainer",function(a){localStorage.setItem("sm-inside-container",a),d.toggleClass("container",a),a&&(c.fixedRibbon=!1,c.fixedNavigation=!1)}),c.$watch("rtl",function(a){localStorage.setItem("sm-rtl",a),d.toggleClass("smart-rtl",a)}),c.$watch("menuOnTop",function(b){a.$broadcast("$smartLayoutMenuOnTop",b),localStorage.setItem("sm-menu-on-top",b),d.toggleClass("menu-on-top",b),b&&d.removeClass("minified")}),c.$watch("colorblindFriendly",function(a){localStorage.setItem("sm-colorblind-friendly",a),d.toggleClass("colorblind-friendly",a)}),c.fixedHeader="true"==localStorage.getItem("sm-fixed-header"),c.fixedNavigation="true"==localStorage.getItem("sm-fixed-navigation"),c.fixedRibbon="true"==localStorage.getItem("sm-fixed-ribbon"),c.fixedPageFooter="true"==localStorage.getItem("sm-fixed-page-footer"),c.insideContainer="true"==localStorage.getItem("sm-inside-container"),c.rtl="true"==localStorage.getItem("sm-rtl"),c.menuOnTop="true"==localStorage.getItem("sm-menu-on-top")||d.hasClass("menu-on-top"),c.colorblindFriendly="true"==localStorage.getItem("sm-colorblind-friendly"),c.skins=appConfig.skins,c.smartSkin=localStorage.getItem("sm-skin")||appConfig.smartSkin,c.setSkin=function(a){c.smartSkin=a.name,d.removeClass(b.pluck(c.skins,"name").join(" ")),d.addClass(a.name),localStorage.setItem("sm-skin",a.name),$("#logo img").attr("src",a.logo)},"smart-style-0"!=c.smartSkin&&c.setSkin(b.find(c.skins,{name:c.smartSkin})),c.factoryReset=function(){$.SmartMessageBox({title:"<i class='fa fa-refresh' style='color:green'></i> Clear Local Storage",content:"Would you like to RESET all your saved widgets and clear LocalStorage?1",buttons:"[No][Yes]"},function(a){"Yes"==a&&localStorage&&(localStorage.clear(),location.reload())})}}]}}])});
define('layout/directives/smartInclude',["layout/module"],function(a){return a.registerDirective("smartInclude",function(){return{replace:!0,restrict:"A",templateUrl:function(a,b){return b.smartInclude},compile:function(a){a[0].className=a[0].className.replace(/placeholder[^\s]+/g,"")}}})});
define('layout/directives/smartDeviceDetect',["layout/module"],function(a){a.registerDirective("smartDeviceDetect",function(){return{restrict:"A",compile:function(a){a.removeAttr("smart-device-detect data-smart-device-detect");var b=/iphone|ipad|ipod|android|blackberry|mini|windows\sce|palm/i.test(navigator.userAgent.toLowerCase());a.toggleClass("desktop-detected",!b),a.toggleClass("mobile-detected",b)}}})});
define('layout/directives/smartFastClick',["layout/module","require","fastclick"],function(a,b){a.registerDirective("smartFastClick",function(){var a=b("fastclick");return{restrict:"A",compile:function(b){b.removeAttr("smart-fast-click data-smart-fast-click"),a.attach(b),a.notNeeded()||b.addClass("needsclick")}}})});
define('layout/directives/smartLayout',["layout/module","lodash"],function(a,b){a.registerDirective("smartLayout",["$rootScope","$timeout","$interval","$q","SmartCss",function(a,c,d,e){var f=!1,g=e.defer();g.promise.then(function(){f=!0});var h,i,j,k,l=$(window),m=($(document),$("html")),n=$("body");return function o(){h=$("#header"),i=$("#left-panel"),j=$("#ribbon"),k=$(".page-footer"),b.every([h,i,j,k],function(a){return angular.isNumber(a.height())})?g.resolve():c(o,100)}(),{priority:2014,restrict:"A",compile:function(e){function o(){var b=n.hasClass("menu-on-top")&&i.is(":visible")?i.height():0,c=!n.hasClass("menu-on-top")&&i.is(":visible")?i.width()+i.offset().left:0,d=$("#content"),e=d.outerWidth(!0)-d.width(),f=d.outerHeight(!0)-d.height();u=l.width()-c-e,v=l.height()-b-f-h.height()-j.height()-k.height(),w=z-u,x=y-v,(Math.abs(w)||Math.abs(x)||A)&&(a.$broadcast("$smartContentResize",{width:u,height:v,deltaX:w,deltaY:x}),z=u,y=v,A=!1)}function p(a){g.promise.then(function(){r(a)})}function q(){B=!1}function r(a){c(function(){B=!0},a)}function s(){n.toggleClass("mobile-view-activated",l.width()<979),l.width()<979&&n.removeClass("minified"),o()}function t(a){a.data&&a.data.htmlId?m.attr("id",a.data.htmlId):m.removeAttr("id")}e.removeAttr("smart-layout data-smart-layout");var u,v,w,x,y=0,z=0,A=!1,B=!1;d(function(){B&&s()},300);var C=b.debounce(function(){p(300)},300);p(10),a.$on("$stateChangeStart",function(a,b){t(b),q()});var D=1;a.$on("$viewContentLoading",function(){D++}),a.$on("$stateChangeSuccess",function(){A=!0}),a.$on("$viewContentLoaded",function(){D--,0==D&&f&&C()})}}}])});
define('layout/directives/smartSpeech',["layout/module","jquery"],function(a,b){b.root_=b("body");var c=window;if(appConfig.voice_command)var d=appConfig.commands;var e=c.SpeechRecognition||c.webkitSpeechRecognition||c.mozSpeechRecognition||c.msSpeechRecognition||c.oSpeechRecognition;b.speechApp=function(a){return a.start=function(){smartSpeechRecognition.addCommands(d),smartSpeechRecognition?(smartSpeechRecognition.start(),b.root_.addClass("voice-command-active"),b.speechApp.playON(),appConfig.voice_localStorage&&localStorage.setItem("sm-setautovoice","true")):alert("speech plugin not loaded")},a.stop=function(){smartSpeechRecognition&&(smartSpeechRecognition.abort(),b.root_.removeClass("voice-command-active"),b.speechApp.playOFF(),appConfig.voice_localStorage&&localStorage.setItem("sm-setautovoice","false"),b("#speech-btn .popover").is(":visible")&&b("#speech-btn .popover").fadeOut(250))},a.playON=function(){var a=document.createElement("audio");navigator.userAgent.match("Firefox/")?a.setAttribute("src",appConfig.sound_path+"voice_on.ogg"):a.setAttribute("src",appConfig.sound_path+"voice_on.mp3"),a.addEventListener("load",function(){a.play()},!0),appConfig.sound_on&&(a.pause(),a.play())},a.playOFF=function(){var a=document.createElement("audio");navigator.userAgent.match("Firefox/")?a.setAttribute("src",appConfig.sound_path+"voice_off.ogg"):a.setAttribute("src",appConfig.sound_path+"voice_off.mp3"),b.get(),a.addEventListener("load",function(){a.play()},!0),appConfig.sound_on&&(a.pause(),a.play())},a.playConfirmation=function(){var a=document.createElement("audio");navigator.userAgent.match("Firefox/")?a.setAttribute("src",appConfig.sound_path+"voice_alert.ogg"):a.setAttribute("src",appConfig.sound_path+"voice_alert.mp3"),b.get(),a.addEventListener("load",function(){a.play()},!0),appConfig.sound_on&&(a.pause(),a.play())},a}({}),function(a){if(!e)return c.smartSpeechRecognition=null,a;var d,f,g=[],h={start:[],error:[],end:[],result:[],resultMatch:[],resultNoMatch:[],errorNetwork:[],errorPermissionBlocked:[],errorPermissionDenied:[]},i=0,j=/\s*\((.*?)\)\s*/g,k=/(\(\?:[^)]+\))\?/g,l=/(\(\?)?:\w+/g,m=/\*\w+/g,n=/[\-{}\[\]+?.,\\\^$|#]/g,o=function(a){return a=a.replace(n,"\\$&").replace(j,"(?:$1)?").replace(l,function(a,b){return b?a:"([^\\s]+)"}).replace(m,"(.*?)").replace(k,"\\s*$1?\\s*"),new RegExp("^"+a+"$","i")},p=function(a){a.forEach(function(a){a.callback.apply(a.context)})},q=function(){r()||c.smartSpeechRecognition.init({},!1)},r=function(){return d!==a};c.smartSpeechRecognition={init:function(j,k){k=k===a?!0:!!k,d&&d.abort&&d.abort(),d=new e,d.maxAlternatives=5,d.continuous=!0,d.lang=appConfig.voice_command_lang||"en-US",d.onstart=function(){p(h.start),appConfig.debugState&&(c.console.log("%c ✔ SUCCESS: User allowed access the microphone service to start ",appConfig.debugStyle_success),c.console.log("Language setting is set to: "+d.lang,appConfig.debugStyle)),b.root_.removeClass("service-not-allowed"),b.root_.addClass("service-allowed")},d.onerror=function(a){switch(p(h.error),a.error){case"network":p(h.errorNetwork);break;case"not-allowed":case"service-not-allowed":f=!1,b.root_.removeClass("service-allowed"),b.root_.addClass("service-not-allowed"),appConfig.debugState&&c.console.log("%c WARNING: Microphone was not detected (either user denied access or it is not installed properly) ",appConfig.debugStyle_warning),p((new Date).getTime()-i<200?h.errorPermissionBlocked:h.errorPermissionDenied)}},d.onend=function(){if(p(h.end),f){var a=(new Date).getTime()-i;1e3>a?setTimeout(c.smartSpeechRecognition.start,1e3-a):c.smartSpeechRecognition.start()}},d.onresult=function(a){p(h.result);for(var d,e=a.results[a.resultIndex],f=0;f<e.length;f++){d=e[f].transcript.trim(),appConfig.debugState&&c.console.log("Speech recognized: %c"+d,appConfig.debugStyle);for(var i=0,j=g.length;j>i;i++){var k=g[i].command.exec(d);if(k){var l=k.slice(1);appConfig.debugState&&(c.console.log("command matched: %c"+g[i].originalPhrase,appConfig.debugStyle),l.length&&c.console.log("with parameters",l)),g[i].callback.apply(this,l),p(h.resultMatch);var m=["sound on","mute","stop"];return m.indexOf(g[i].originalPhrase)<0&&(console.log(2),b.smallBox({title:g[i].originalPhrase,content:"loading...",color:"#333",sound_file:"voice_alert",timeout:2e3}),b("#speech-btn .popover").is(":visible")&&b("#speech-btn .popover").fadeOut(250)),!0}}}return p(h.resultNoMatch),b.smallBox({title:'Error: <strong> " '+d+' " </strong> no match found!',content:"Please speak clearly into the microphone",color:"#a90329",timeout:5e3,icon:"fa fa-microphone"}),b("#speech-btn .popover").is(":visible")&&b("#speech-btn .popover").fadeOut(250),!1},k&&(g=[]),j.length&&this.addCommands(j)},start:function(b){q(),b=b||{},f=b.autoRestart!==a?!!b.autoRestart:!0,i=(new Date).getTime(),d.start()},abort:function(){f=!1,r&&d.abort()},debug:function(a){appConfig.debugState=arguments.length>0?!!a:!0},setLanguage:function(a){q(),d.lang=a},addCommands:function(a){var b,d;q();for(var e in a)if(a.hasOwnProperty(e)){if(b=c[a[e]]||a[e],"function"!=typeof b)continue;d=o(e),g.push({command:d,callback:b,originalPhrase:e})}appConfig.debugState&&c.console.log("Commands successfully loaded: %c"+g.length,appConfig.debugStyle)},removeCommands:function(b){return b===a?void(g=[]):(b=Array.isArray(b)?b:[b],void(g=g.filter(function(a){for(var c=0;c<b.length;c++)if(b[c]===a.originalPhrase)return!1;return!0})))},addCallback:function(b,d,e){if(h[b]!==a){var f=c[d]||d;"function"==typeof f&&h[b].push({callback:f,context:e||this})}}}}.call(this);var f=function(){smartSpeechRecognition.addCommands(d),smartSpeechRecognition?(smartSpeechRecognition.start(),b.root_.addClass("voice-command-active"),appConfig.voice_localStorage&&localStorage.setItem("sm-setautovoice","true")):alert("speech plugin not loaded")};e&&appConfig.voice_command&&"true"==localStorage.getItem("sm-setautovoice")&&f(),e&&appConfig.voice_command_auto&&appConfig.voice_command&&f(),a.registerDirective("speechRecognition",["$log",function(){var a=function(a,c){if(e&&appConfig.voice_command){var f=b('<div class="modal fade" id="voiceModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"></div></div></div>');f.appendTo("body"),c.on("click",function(a){b.root_.hasClass("voice-command-active")?b.speechApp.stop():(b.speechApp.start(),b("#speech-btn .popover").fadeIn(350)),a.preventDefault()}),b(document).mouseup(function(a){b("#speech-btn .popover").is(a.target)||0!==b("#speech-btn .popover").has(a.target).length||b("#speech-btn .popover").fadeOut(250)}),b("#speech-help-btn").on("click",function(){d.help()})}else b("#speech-btn").addClass("display-none")};return{restrict:"AE",link:a}}])});
define('layout/directives/smartRouterAnimationWrap',["layout/module","lodash"],function(a,b){a.registerDirective("smartRouterAnimationWrap",["$rootScope","$timeout",function(a,c){return{restrict:"A",compile:function(d,e){function f(){j=!0,d.css({height:d.height()+"px",overflow:"hidden"}).addClass("active"),$(h).addClass("animated faster fadeOutDown")}function g(){j&&(d.css({height:"auto",overflow:"visible"}).removeClass("active"),$(h).addClass("animated faster fadeInUp"),j=!1,c(function(){$(h).removeClass("animated")},10))}d.removeAttr("smart-router-animation-wrap data-smart-router-animation-wrap wrap-for data-wrap-for"),d.addClass("router-animation-container"),$('<div class="router-animation-loader"><i class="fa fa-gear fa-4x fa-spin"></i></div>').appendTo(d);var h=e.wrapFor,i=e.smartRouterAnimationWrap.split(/\s/),j=!1,k=a.$on("$stateChangeStart",function(a,c,d,e){var g=b.any(i,function(a){return b.has(c.views,a)||b.has(e.views,a)});g&&f()}),l=a.$on("$viewContentLoaded",function(){g()});d.on("$destroy",function(){k(),l()})}}}])});
define('layout/directives/smartFitAppView',["layout/module","lodash"],function(a){a.registerDirective("smartFitAppView",["$rootScope","SmartCss",function(a,b){return{restrict:"A",compile:function(c,d){c.removeAttr("smart-fit-app-view data-smart-fit-app-view leading-y data-leading-y");var e=d.leadingY?parseInt(d.leadingY):0,f=d.smartFitAppView;if(b.appViewSize&&b.appViewSize.height){var g=b.appViewSize.height-e<252?252:b.appViewSize.height-e;b.add(f,"height",g+"px")}var h=a.$on("$smartContentResize",function(a,c){var d=c.height-e<252?252:c.height-e;b.add(f,"height",d+"px")});c.on("$destroy",function(){h(),b.remove(f,"height")})}}}])});
define('layout/directives/radioToggle',["layout/module"],function(a){a.registerDirective("radioToggle",["$log",function(){return{scope:{model:"=ngModel",value:"@value"},link:function(a,b){b.parent().on("click",function(){a.model=a.value,a.$apply()})}}}])});
define('layout/directives/dismisser',["layout/module"],function(a){a.registerDirective("dismisser",function(){return{restrict:"A",compile:function(a){a.removeAttr("dismisser data-dissmiser");var b='<button class="close">&times;</button>';a.prepend(b),a.on("click",">button.close",function(){a.fadeOut("fast",function(){$(this).remove()})})}}})});
define('layout/directives/smartMenu',["layout/module","jquery"],function(a){!function(a){a.fn.smartCollapseToggle=function(){return this.each(function(){var b=a("body"),c=a(this);b.hasClass("menu-on-top")||(b.hasClass("mobile-view-activated"),c.toggleClass("open"),b.hasClass("minified")?c.closest("nav ul ul").length&&(c.find(">a .collapse-sign .fa").toggleClass("fa-minus-square-o fa-plus-square-o"),c.find("ul:first").slideToggle(appConfig.menu_speed||200)):(c.find(">a .collapse-sign .fa").toggleClass("fa-minus-square-o fa-plus-square-o"),c.find("ul:first").slideToggle(appConfig.menu_speed||200)))})}}(jQuery),a.registerDirective("smartMenu",["$state","$rootScope",function(a,b){return{restrict:"A",link:function(a,c){var d=$("body"),e=c.find("li[data-menu-collapse]");e.each(function(a,b){var c=$(b);c.on("click",">a",function(a){c.siblings(".open").smartCollapseToggle(),c.smartCollapseToggle(),!c.hasClass("open")&&c.find("li.active").length>0&&c.addClass("active"),a.preventDefault()}).find(">a").append('<b class="collapse-sign"><em class="fa fa-plus-square-o"></em></b>'),c.find("li.active").length&&(c.smartCollapseToggle(),c.find("li.active").parents("li").addClass("active"))}),c.on("click","a[data-ui-sref]",function(){$(this).parents("li").addClass("active").each(function(){$(this).siblings("li.open").smartCollapseToggle(),$(this).siblings("li").removeClass("active")}),d.hasClass("mobile-view-activated")&&b.$broadcast("requestToggleMenu")}),a.$on("$smartLayoutMenuOnTop",function(a,b){b&&e.filter(".open").smartCollapseToggle()})}}}])});
define('layout/directives/bigBreadcrumbs',["layout/module","lodash"],function(a,b){a.registerDirective("bigBreadcrumbs",function(){return{restrict:"E",replace:!0,template:'<div><h1 class="page-title txt-color-blueDark"></h1></div>',scope:{items:"=",icon:"@"},link:function(a,c){var d=b.first(a.items),e=a.icon||"home";c.find("h1").append('<i class="fa-fw fa fa-'+e+'"></i> '+d),b.rest(a.items).forEach(function(a){c.find("h1").append(" <span>> "+a+"</span>")})}}})});
define('layout/directives/stateBreadcrumbs',["layout/module"],function(a){a.registerDirective("stateBreadcrumbs",["$rootScope","$state",function(a,b){return{restrict:"E",replace:!0,template:'<ol class="breadcrumb"><li>Home</li></ol>',link:function(c,d){function e(a){var b="<li>Home</li>";angular.forEach(a,function(a){b+="<li>"+a+"</li>"}),d.html(b)}function f(a,c){var d=b.get(a);d&&d.data&&d.data.title&&-1==c.indexOf(d.data.title)&&c.unshift(d.data.title);var e=a.replace(/.?\w+$/,"");return e?f(e,c):c}function g(a){var b;b=a.data&&a.data.breadcrumbs?a.data.breadcrumbs:f(a.name,[]),e(b)}g(b.current),a.$on("$stateChangeStart",function(a,b){g(b)})}}}])});
define('layout/directives/smartPageTitle',["layout/module"],function(a){a.registerDirective("smartPageTitle",["$rootScope","$timeout",function(a,b){return{restrict:"A",compile:function(c,d){c.removeAttr("smart-page-title data-smart-page-title");var e=d.smartPageTitle,f=function(a,c){var d=e;c.data&&c.data.title&&(d=c.data.title+" | "+d),b(function(){$("html head title").text(d)})};a.$on("$stateChangeStart",f)}}}])});
define('layout/directives/hrefVoid',["layout/module"],function(a){a.registerDirective("hrefVoid",function(){return{restrict:"A",link:function(a,b){b.attr("href","#"),b.on("click",function(a){a.preventDefault(),a.stopPropagation()})}}})});
define('layout/service/SmartCss',["layout/module","lodash"],function(a,b){a.registerFactory("SmartCss",["$rootScope","$timeout",function(a,c){var d=function(){var a=document.createElement("style");return a.appendChild(document.createTextNode("")),document.head.appendChild(a),a.sheet}(),e={},f={writeRule:function(a){if(f.deleteRuleFor(a),b.has(e,a)){var c=a+"{ "+b.map(e[a],function(a,b){return b+":"+a+";"}).join(" ")+"}";d.insertRule(c)}},add:function(a,d,g,h){b.has(e,a)||(e[a]={}),void 0==g||null==g||""==g?delete e[a][d]:e[a][d]=g,0==b.keys(e[a]).length&&delete e[a],h||(h=0),c(function(){f.writeRule(a)},h)},remove:function(a,b,c){f.add(a,b,null,c)},deleteRuleFor:function(a){b(d.rules).forEach(function(b,c){b.selectorText==a&&d.deleteRule(c)})},appViewSize:null};return a.$on("$smartContentResize",function(a,b){f.appViewSize=b}),f}])});
define('modules/widgets/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.widgets",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a){a.state("app.widgets",{url:"/widgets",data:{title:"Widgets"},views:{"content@app":{templateUrl:"build/modules/widgets/views/widgets-demo.html"}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/widgets/directives/widgetGrid',["modules/widgets/module","lodash"],function(a,b){a.registerDirective("widgetGrid",["$rootScope","$compile","$q","$state","$timeout",function(a,c,d,e,f){var g,h,i,j={grid:"article",widgets:".jarviswidget",localStorage:!0,deleteSettingsKey:"#deletesettingskey-options",settingsKeyLabel:"Reset settings?",deletePositionKey:"#deletepositionkey-options",positionKeyLabel:"Reset position?",sortable:!0,buttonsHidden:!1,toggleButton:!0,toggleClass:"fa fa-minus | fa fa-plus",toggleSpeed:200,onToggle:function(){},deleteButton:!0,deleteMsg:"Warning: This action cannot be undone!",deleteClass:"fa fa-times",deleteSpeed:200,onDelete:function(){},editButton:!0,editPlaceholder:".jarviswidget-editbox",editClass:"fa fa-cog | fa fa-save",editSpeed:200,onEdit:function(){},colorButton:!0,fullscreenButton:!0,fullscreenClass:"fa fa-expand | fa fa-compress",fullscreenDiff:3,onFullscreen:function(){},customButton:!1,customClass:"folder-10 | next-10",customStart:function(){alert("Hello you, this is a custom button...")},customEnd:function(){alert("bye, till next time...")},buttonOrder:"%refresh% %custom% %edit% %toggle% %fullscreen% %delete%",opacity:1,dragHandle:"> header",placeholderClass:"jarviswidget-placeholder",indicator:!0,indicatorTime:600,ajax:!0,timestampPlaceholder:".jarviswidget-timestamp",timestampFormat:"Last update: %m%/%d%/%y% %h%:%i%:%s%",refreshButton:!0,refreshButtonClass:"fa fa-refresh",labelError:"Sorry but there was a error:",labelUpdated:"Last Update:",labelRefresh:"Refresh",labelDelete:"Delete widget:",afterLoad:function(){},rtl:!1,onChange:function(){},onSave:function(){},ajaxnav:!0},k=[],l=!1,m=function(a,c){l?l||(l=!0,f(function(){l=!1,m(a,c)},200)):b.intersection(c,k).length!=c.length&&(k=b.union(c,k),a.data("jarvisWidgets")&&a.data("jarvisWidgets").destroy(),a.jarvisWidgets(j),o(c))},n=function(a,c){a.data("jarvisWidgets")&&a.data("jarvisWidgets").destroy(),k=b.xor(k,c)},o=function(a){angular.forEach(a,function(a){$("#"+a+' [data-toggle="dropdown"]').each(function(){var a=$(this).parent();if($(this).removeAttr("data-toggle"),!a.attr("dropdown")){$(this).removeAttr("href"),a.attr("dropdown","");var b=c(a)(a.scope());a.replaceWith(b)}})})};return{restrict:"A",compile:function(b){b.removeAttr("widget-grid data-widget-grid");var c=[];h=a.$on("$viewContentLoaded",function(){f(function(){m(b,c)},100)}),i=a.$on("$stateChangeStart",function(){g(),h(),i(),n(b,c)}),g=a.$on("jarvisWidgetAdded",function(a,d){-1==c.indexOf(d.attr("id"))&&(c.push(d.attr("id")),f(function(){m(b,c)},100))})}}}])});
define('modules/widgets/directives/jarvisWidget',["modules/widgets/module"],function(a){a.registerDirective("jarvisWidget",["$rootScope",function(a){return{restrict:"A",compile:function(b){b.data("widget-color")&&b.addClass("jarviswidget-color-"+b.data("widget-color")),b.find(".widget-body").prepend('<div class="jarviswidget-editbox"><input class="form-control" type="text"></div>'),b.addClass("jarviswidget jarviswidget-sortable"),a.$emit("jarvisWidgetAdded",b)}}}])});
define('dashboard/module',["angular","angular-couch-potato","angular-ui-router","angular-resource"],function(a,b){var c=a.module("app.dashboard",["ui.router","ngResource"]);return c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.dashboard",{url:"/dashboard",views:{"content@app":{controller:"DashboardCtrl",templateUrl:"build/dashboard/dashboard.html",resolve:{deps:b.resolveDependencies(["dashboard/DashboardCtrl","modules/graphs/directives/inline/sparklineContainer","modules/graphs/directives/inline/easyPieChartContainer","components/chat/directives/chatWidget","components/chat/directives/chatUsers","modules/graphs/directives/vectormap/vectorMap","modules/graphs/directives/flot/flotBasic"])}}},data:{title:"Dashboard"}})}]),b.configureApp(c),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('components/boot/module',["angular","angular-couch-potato","lodash"],function(a){var b=a.module("app.boot",[]);return b});
define('components/boot/directives/unreadMessagesCount',["components/boot/module","lodash"],function(a,b){a.registerDirective("unreadMessagesCount",["InboxConfig",function(a){return{restrict:"A",link:function(c,d){a.success(function(a){d.html(b.find(a.folders,{key:"inbox"}).unread)})}}}])});
define('components/boot/models/InboxConfig',["components/boot/module"],function(a){return a.registerFactory("InboxConfig",["$http",function(a){return a.get("api/inbox.json")}])});
define('components/boot/models/CalendarEvent',["components/boot/module"],function(a){a.registerFactory("CalendarEvent",["$resource",function(a){return a("api/events.json",{_id:"@id"})}])});
define('components/language/Language',["app"],function(a){return a.factory("Language",["$http",function(a){function b(b,c){a.get("api/langs/"+b+".json").success(function(a){c(a)}).error(function(){$log.log("Error"),c([])})}function c(b){a.get("api/languages.json").success(function(a){b(a)}).error(function(){$log.log("Error"),b([])})}return{getLang:function(a,c){b(a,c)},getLanguages:function(a){c(a)}}}])});
define('components/language/languageSelector',["app"],function(a){a.registerDirective("languageSelector",["Language",function(){return{restrict:"EA",replace:!0,templateUrl:"build/components/language/language-selector.tpl.html",scope:!0}}])});
define('components/language/language-controller',["app"],function(a){function b(a,b,c,d){b.lang={},d.getLanguages(function(a){b.currentLanguage=a[0],b.languages=a,d.getLang(a[0].key,function(a){b.lang=a})}),a.selectLanguage=function(a){b.currentLanguage=a,d.getLang(a.key,function(a){b.lang=a})},b.getWord=function(a){return angular.isDefined(b.lang[a])?b.lang[a]:a}}return b.$inject=["$scope","$rootScope","$log","Language"],a.controller("LanguagesCtrl",b)});
define('components/projects/Project',["app"],function(a){return a.factory("Project",["$http",function(a){return{list:a.get("api/projects.json")}}])});
define('components/projects/recentProjects',["app"],function(a){return a.directive("recentProjects",["Project",function(a){return{restrict:"EA",replace:!0,templateUrl:"build/components/projects/recent-projects.tpl.html",scope:!0,link:function(b){a.list.then(function(a){b.projects=a.data}),b.clearProjects=function(){b.projects=[]}}}}])});
define('components/activities/activities-controller',["app"],function(a){function b(a,b,c){a.activeTab="default",a.currentActivityItems=[],c.get(function(b){a.activities=b.activities}),a.isActive=function(b){return a.activeTab===b},a.setTab=function(b){a.activeTab=b,c.getbytype(b,function(b){a.currentActivityItems=b.data})}}return b.$inject=["$scope","$log","activityService"],a.controller("ActivitiesCtrl",b)});
define('components/activities/activities-dropdown-toggle-directive',["app"],function(a){return a.directive("activitiesDropdownToggle",["$log",function(){var a=function(a,b){var c=null;b.on("click",function(){var a=$(this).find(".badge");a.hasClass("bg-color-red")&&a.removeClass("bg-color-red").text(0),c=$(this).next(".ajax-dropdown"),c.is(":visible")?(c.fadeOut(150),$(this).removeClass("active")):(c.fadeIn(150),$(this).addClass("active"))}),$(document).mouseup(function(a){c&&!c.is(a.target)&&0===c.has(a.target).length&&(c.fadeOut(150),b.removeClass("active"))})};return{restrict:"EA",link:a}}])});
define('components/activities/activities-service',["app"],function(a){return a.factory("activityService",["$http","$log",function(a,b){function c(c){a.get("api/activities/activity.json").success(function(a){c(a)}).error(function(){b.log("Error"),c([])})}function d(c,d){a.get("api/activities/activity-"+c+".json").success(function(a){d(a)}).error(function(){b.log("Error"),d([])})}return{get:function(a){c(a)},getbytype:function(a,b){d(a,b)}}}])});
define('components/shortcut/shortcut-directive',["app"],function(a){return a.directive("toggleShortcut",["$log","$timeout",function(a,b){var c=function(a){function b(){d.animate({height:"hide"},300,"easeOutCirc"),$("body").removeClass("shortcut-on")}function c(){d.animate({height:"show"},200,"easeOutCirc"),$("body").addClass("shortcut-on")}var d=$("#shortcut");a.on("click",function(){d.is(":visible")?b():c()}),d.find("a").click(function(a){a.preventDefault(),window.location=$(this).attr("href"),setTimeout(b,300)}),$(document).mouseup(function(a){d&&!d.is(a.target)&&0===d.has(a.target).length&&b()})},d=function(a,d){b(function(){c(d)})};return{restrict:"EA",link:d}}])});
define('components/todo/TodoCtrl',["app"],function(a){a.registerController("TodoCtrl",["$scope","$timeout","Todo",function(a,b,c){a.newTodo=void 0,a.states=["Critical","Important","Completed"],a.todos=c.query({}),a.toggleCompleted=function(a){a.completedAt?(a.state="Critical",a.completedAt=null):(a.state="Completed",a.completedAt=new Date),a.$update()},a.updateTodoState=function(a,b){a.state=b,a.completedAt="Completed"==b?JSON.stringify(new Date):null,a.$update()},a.toggleAdd=function(){a.newTodo=a.newTodo?void 0:new c({state:"Important"})},a.createTodo=function(){a.newTodo.$save(function(b){a.todos.push(b),a.newTodo=void 0})},a.deleteTodo=function(b){b.$delete(function(){a.todos.splice(a.todos.indexOf(b),1)})}}])});
define('components/todo/models/Todo',["app"],function(a){return a.factory("Todo",["$resource",function(a){return a("api/todos.json",{id:"@_id"})}])});
define('components/todo/directives/todoList',["app","lodash","jquery-ui"],function(a){a.registerDirective("todoList",["$timeout","Todo",function(){return{restrict:"E",replace:!0,templateUrl:"build/components/todo/directives/todo-list.tpl.html",scope:{title:"@",icon:"@",state:"@"},controller:["$scope",function(a){a.updateTodoState=a.$parent.updateTodoState,a.toggleCompleted=a.$parent.toggleCompleted,a.deleteTodo=a.$parent.deleteTodo,a.$parent.todos.$promise.then(function(b){a.todos=b}),a.filter={state:a.state}}],link:function(a,b){b.find(".todo").sortable({handle:".handle",connectWith:".todo",receive:function(b,c){var d=c.item.scope().todo,e=c.item.closest("[state]").attr("state");d&&e?a.updateTodoState(d,e):console.log("Wat",d,e),c.sender.sortable("cancel")}}).disableSelection()}}}])});
define('components/chat/module',["angular","angular-couch-potato","angular-sanitize"],function(a,b){var c=a.module("app.chat",["ngSanitize"]);return b.configureApp(c),c.run(["$couchPotato","$templateCache",function(a,b){c.lazy=a,b.put("template/popover/popover.html",'<div class="popover {{placement}}" ng-class="{ in: isOpen(), fade: animation() }">\n  <div class="arrow"></div>\n\n  <div class="popover-inner">\n      <h3 class="popover-title" ng-bind-html="title | unsafe" ng-show="title"></h3>\n      <div class="popover-content"ng-bind-html="content | unsafe"></div>\n  </div>\n</div>\n')}]).filter("unsafe",["$sce",function(a){return function(b){return a.trustAsHtml(b)}}]),c});
define('modules/graphs/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.graphs",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.graphs",{"abstract":!0,data:{title:"Graphs"}}).state("app.graphs.flot",{url:"/graphs/flot",data:{title:"Flot Charts"},views:{"content@app":{controller:"FlotCtrl",templateUrl:"build/modules/graphs/views/flot-charts.html",resolve:{deps:b.resolveDependencies(["modules/graphs/controllers/FlotCtrl","modules/graphs/directives/flot/flotBarChart","modules/graphs/directives/flot/flotSinChart","modules/graphs/directives/flot/flotHorizontalBarChart","modules/graphs/directives/flot/flotSalesChart","modules/graphs/directives/flot/flotFillChart","modules/graphs/directives/flot/flotPieChart","modules/graphs/directives/flot/flotSiteStatsChart","modules/graphs/directives/flot/flotAutoUpdatingChart"])}}}}).state("app.graphs.morris",{url:"/graphs/morris",data:{title:"Morris Charts"},views:{"content@app":{templateUrl:"build/modules/graphs/views/morris-charts.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/morris/morrisSalesGraph","modules/graphs/directives/morris/morrisAreaGraph","modules/graphs/directives/morris/morrisBarGraph","modules/graphs/directives/morris/morrisNormalBarGraph","modules/graphs/directives/morris/morrisStackedBarGraph","modules/graphs/directives/morris/morrisYearGraph","modules/graphs/directives/morris/morrisDonutGraph","modules/graphs/directives/morris/morrisTimeGraph","modules/graphs/directives/morris/morrisLineGraphA","modules/graphs/directives/morris/morrisLineGraphB","modules/graphs/directives/morris/morrisLineGraphC","modules/graphs/directives/morris/morrisNoGridGraph"])}}}}).state("app.graphs.inline",{url:"/graphs/inline",data:{title:"Inline Charts"},views:{"content@app":{templateUrl:"build/modules/graphs/views/inline-charts.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/inline/sparklineContainer","modules/graphs/directives/inline/easyPieChartContainer"])}}}}).state("app.graphs.dygraphs",{url:"/graphs/dygraphs",data:{title:"Dygraphs Charts"},views:{"content@app":{templateUrl:"build/modules/graphs/views/dygraphs-charts.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/dygraphs/dygraphsNoRollPeriod","modules/graphs/directives/dygraphs/dygraphsNoRollTimestamp"])}}}}).state("app.graphs.chartjs",{url:"/graphs/chartjs",data:{title:"Chartjs"},views:{"content@app":{templateUrl:"build/modules/graphs/views/chartjs-charts.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/chartjs/chartjsLineChart","modules/graphs/directives/chartjs/chartjsBarChart","modules/graphs/directives/chartjs/chartjsPolarChart","modules/graphs/directives/chartjs/chartjsDoughnutChart","modules/graphs/directives/chartjs/chartjsRadarChart","modules/graphs/directives/chartjs/chartjsPieChart"])}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/tables/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.tables",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.tables",{"abstract":!0,data:{title:"Tables"}}).state("app.tables.normal",{url:"/tables/normal",data:{title:"Normal Tables"},views:{"content@app":{templateUrl:"build/modules/tables/views/normal.html"}}}).state("app.tables.datatables",{url:"/tables/datatables",data:{title:"Data Tables"},views:{"content@app":{templateUrl:"build/modules/tables/views/datatables.html",resolve:{deps:b.resolveDependencies(["modules/tables/directives/datatables/datatableBasic","modules/tables/directives/datatables/datatableColumnFilter","modules/tables/directives/datatables/datatableColumnReorder","modules/tables/directives/datatables/datatableTableTools"])}}}}).state("app.tables.jqgrid",{url:"/tables/jqgrid",data:{title:"Jquery Grid"},views:{"content@app":{controller:"JqGridCtrl",templateUrl:"build/modules/tables/views/jqgrid.html",resolve:{deps:b.resolveDependencies(["modules/tables/controllers/JqGridCtrl","modules/tables/directives/jqgrid/jqGrid"])}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/forms/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.forms",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.form",{"abstract":!0,data:{title:"Forms"}}).state("app.form.elements",{url:"/form/elements",data:{title:"Form Elements"},views:{"content@app":{templateUrl:"build/modules/forms/views/form-elements.html"}}}).state("app.form.layouts",{url:"/form/layouts",data:{title:"Form Layouts"},views:{"content@app":{controller:"FormLayoutsCtrl",templateUrl:"build/modules/forms/views/form-layouts.html",resolve:{deps:b.resolveDependencies(["modules/forms/controllers/FormLayoutsCtrl","modules/forms/directives/form-layouts/smartCheckoutForm","modules/forms/directives/form-layouts/smartOrderForm","modules/forms/directives/form-layouts/smartReviewForm","modules/forms/directives/form-layouts/smartRegistrationForm","modules/forms/directives/form-layouts/smartCommentForm","modules/forms/directives/form-layouts/smartContactsForm","modules/forms/directives/input/smartMaskedInput","modules/forms/directives/input/smartDatepicker"])}}}}).state("app.form.validation",{url:"/form/validation",data:{title:"Form Validation"},views:{"content@app":{templateUrl:"build/modules/forms/views/form-validation.html"}}}).state("app.form.bootstrapForms",{url:"/form/bootstrap-forms",data:{title:"Bootstrap Forms"},views:{"content@app":{templateUrl:"build/modules/forms/views/bootstrap-forms.html"}}}).state("app.form.bootstrapValidation",{url:"/form/bootstrap-validation",data:{title:"Bootstrap Validation"},views:{"content@app":{templateUrl:"build/modules/forms/views/bootstrap-validation.html",resolve:{deps:b.resolveDependencies(["modules/forms/directives/bootstrap-validation/bootstrapMovieForm","modules/forms/directives/bootstrap-validation/bootstrapTogglingForm","modules/forms/directives/bootstrap-validation/bootstrapAttributeForm","modules/forms/directives/bootstrap-validation/bootstrapButtonGroupForm","modules/forms/directives/bootstrap-validation/bootstrapProductForm","modules/forms/directives/bootstrap-validation/bootstrapProfileForm","modules/forms/directives/bootstrap-validation/bootstrapContactForm"])}}}}).state("app.form.plugins",{url:"/form/plugins",data:{title:"Form Plugins"},views:{"content@app":{templateUrl:"build/modules/forms/views/form-plugins.html",controller:"FormPluginsCtrl",resolve:{deps:b.resolveDependencies(["modules/forms/directives/input/smartSpinner","modules/forms/directives/input/smartDatepicker","modules/forms/directives/input/smartTimepicker","modules/forms/directives/input/smartClockpicker","modules/forms/directives/input/smartNouislider","modules/forms/directives/input/smartIonslider","modules/forms/directives/input/smartDuallistbox","modules/forms/directives/input/smartColorpicker","modules/forms/directives/input/smartKnob","modules/forms/directives/input/smartUislider","modules/forms/directives/input/smartSelect2","modules/forms/directives/input/smartMaskedInput","modules/forms/directives/input/smartTagsinput","modules/forms/directives/input/smartXEditable","modules/forms/controllers/FormXeditableCtrl","modules/forms/controllers/FormPluginsCtrl"])}}}}).state("app.form.wizards",{url:"/form/wizards",data:{title:"Wizards"},views:{"content@app":{templateUrl:"build/modules/forms/views/form-wizards.html",controller:"FormWizardCtrl",resolve:{deps:b.resolveDependencies(["modules/forms/directives/validate/smartValidateForm","modules/forms/directives/wizard/smartWizard","modules/forms/directives/wizard/smartFueluxWizard","modules/forms/directives/input/smartMaskedInput","modules/forms/controllers/FormWizardCtrl"])}}}}).state("app.form.editors",{url:"/form/editors",data:{title:"Editors"},views:{"content@app":{templateUrl:"build/modules/forms/views/form-editors.html",resolve:{deps:b.resolveDependencies(["modules/forms/directives/editors/smartMarkdownEditor","modules/forms/directives/editors/smartSummernoteEditor","modules/forms/directives/editors/smartEditSummernote","modules/forms/directives/editors/smartDestroySummernote"])}}}}).state("app.form.dropzone",{url:"/form/dropzone",data:{title:"Dropzone"},views:{"content@app":{templateUrl:"build/modules/forms/views/dropzone.html",resolve:{deps:b.resolveDependencies(["modules/forms/directives/upload/smartDropzone"])}}}}).state("app.form.imageEditor",{url:"/form/image-editor",data:{title:"Image Editor"},views:{"content@app":{templateUrl:"build/modules/forms/views/image-editor.html",controller:"ImageEditorCtrl",resolve:{deps:b.resolveDependencies(["modules/forms/controllers/ImageEditorCtrl","modules/forms/directives/image-editor/smartJcrop"])}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/ui/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=angular.module("app.ui",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.ui",{"abstract":!0,data:{title:"UI Elements"}}).state("app.ui.general",{url:"/ui/general",data:{title:"General Elements"},views:{"content@app":{templateUrl:"build/modules/ui/views/general-elements.html",controller:"GeneralElementsCtrl",resolve:{deps:b.resolveDependencies(["modules/ui/controllers/GeneralElementsCtrl","modules/ui/directives/smartHtmlPopover","modules/ui/directives/smartProgressbar","modules/ui/directives/smartRideCarousel"])}}}}).state("app.ui.buttons",{url:"/ui/buttons",data:{title:"Buttons"},views:{"content@app":{templateUrl:"build/modules/ui/views/buttons.html",controller:"GeneralElementsCtrl",resolve:{deps:b.resolveDependencies(["modules/ui/controllers/GeneralElementsCtrl","modules/ui/directives/smartHtmlPopover","modules/ui/directives/smartProgressbar","modules/ui/directives/smartRideCarousel"])}}}}).state("app.ui.iconsFa",{url:"/ui/icons-font-awesome",data:{title:"Font Awesome"},views:{"content@app":{templateUrl:"build/modules/ui/views/icons-fa.html",resolve:{deps:b.resolveDependencies(["modules/ui/directives/smartClassFilter"])}}}}).state("app.ui.iconsGlyph",{url:"/ui/icons-glyph",data:{title:"Glyph Icons"},views:{"content@app":{templateUrl:"build/modules/ui/views/icons-glyph.html",resolve:{deps:b.resolveDependencies(["modules/ui/directives/smartClassFilter"])}}}}).state("app.ui.iconsFlags",{url:"/ui/icons-flags",data:{title:"Flags"},views:{"content@app":{templateUrl:"build/modules/ui/views/icons-flags.html",resolve:{deps:b.resolveDependencies(["modules/ui/directives/smartClassFilter"])}}}}).state("app.ui.grid",{url:"/ui/grid",data:{title:"Grid"},views:{"content@app":{templateUrl:"build/modules/ui/views/grid.html"}}}).state("app.ui.treeView",{url:"/ui/tree-view",data:{title:"Tree View"},views:{"content@app":{templateUrl:"build/modules/ui/views/tree-view.html",controller:"TreeviewCtrl",resolve:{deps:b.resolveDependencies(["modules/ui/controllers/TreeviewCtrl","modules/ui/directives/smartTreeview"])}}}}).state("app.ui.nestableLists",{url:"/ui/nestable-lists",data:{title:"Nestable Lists"},views:{"content@app":{templateUrl:"build/modules/ui/views/nestable-lists.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/inline/easyPieChartContainer","modules/ui/directives/smartNestable"])}}}}).state("app.ui.jqueryUi",{url:"/ui/jquery-ui",data:{title:"JQuery UI"},views:{"content@app":{templateUrl:"build/modules/ui/views/jquery-ui.html",controller:"JquiCtrl",resolve:{deps:b.resolveDependencies(["modules/ui/controllers/JquiCtrl","modules/forms/directives/input/smartUislider","modules/forms/directives/input/smartSpinner","modules/ui/directives/smartJquiDialog","modules/ui/directives/smartJquiDialogLauncher","modules/ui/directives/smartProgressbar","modules/ui/directives/smartJquiAccordion","modules/ui/directives/smartJquiAutocomplete","modules/ui/directives/smartJquiAjaxAutocomplete","modules/ui/directives/smartJquiMenu","modules/ui/directives/smartJquiTabs","modules/ui/directives/smartJquiDynamicTabs"])}}}}).state("app.ui.typography",{url:"/ui/typography",data:{title:"JQuery UI"},views:{"content@app":{templateUrl:"build/modules/ui/views/typography.html"}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/maps/module',["angular","angular-couch-potato","angular-google-maps","angular-ui-router"],function(a,b){var c=a.module("app.maps",["ui.router","google-maps".ns()]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider","uiGmapGoogleMapApiProvider",function(a,b,c){c.configure({key:"AIzaSyD5Ybgnvx6jbYGpq7IncsWFc6rkUNrzUzw",v:"3.17"}),a.state("app.maps",{url:"/maps",data:{title:"Maps"},views:{"content@app":{controller:"MapsDemoCtrl",templateUrl:"build/modules/maps/views/maps-demo.html",resolve:{deps:b.resolveDependencies(["layout/directives/smartFitAppView","modules/maps/controllers/MapsDemoCtrl","modules/maps/directives/smartMapInstance","modules/maps/models/SmartMapInstances","modules/maps/models/SmartMapStyle"]),api:["uiGmapGoogleMapApi",function(a){return a}]}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/app-views/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.appViews",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.appViews",{"abstract":!0,data:{title:"App views"}}).state("app.appViews.projects",{url:"/projects",data:{title:"Projects"},views:{"content@app":{templateUrl:"build/modules/app-views/views/project-list.html",controller:"ProjectsDemoCtrl",resolve:{projects:["$http",function(a){return a.get("api/project-list.json")}],deps:b.resolveDependencies(["modules/graphs/directives/inline/sparklineContainer","modules/app-views/controllers/ProjectsDemoCtrl","modules/tables/directives/datatables/datatableBasic"])}}}}).state("app.appViews.blogDemo",{url:"/blog",data:{title:"Blog"},views:{"content@app":{templateUrl:"build/modules/app-views/views/blog-demo.html"}}}).state("app.appViews.galleryDemo",{url:"/gallery",data:{title:"Gallery"},views:{"content@app":{templateUrl:"build/modules/app-views/views/gallery-demo.html",resolve:{deps:b.resolveDependencies(["modules/ui/directives/smartSuperBox"])}}}}).state("app.appViews.forumDemo",{url:"/forum",data:{title:"Forum"},views:{"content@app":{templateUrl:"build/modules/app-views/views/forum-demo.html"}}}).state("app.appViews.forumTopicDemo",{url:"/forum-topic",data:{title:"Forum Topic"},views:{"content@app":{templateUrl:"build/modules/app-views/views/forum-topic-demo.html"}}}).state("app.appViews.forumPostDemo",{url:"/forum-post",data:{title:"Forum Post"},views:{"content@app":{templateUrl:"build/modules/app-views/views/forum-post-demo.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/inline/sparklineContainer","modules/graphs/directives/inline/easyPieChartContainer","modules/forms/directives/editors/smartSummernoteEditor"])}}}}).state("app.appViews.profileDemo",{url:"/profile",data:{title:"Profile"},views:{"content@app":{templateUrl:"build/modules/app-views/views/profile-demo.html"}}}).state("app.appViews.timelineDemo",{url:"/timeline",data:{title:"Timeline"},views:{"content@app":{templateUrl:"build/modules/app-views/views/timeline-demo.html",resolve:{deps:b.resolveDependencies(["modules/graphs/directives/inline/sparklineContainer","modules/graphs/directives/inline/easyPieChartContainer"])}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/misc/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.misc",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a,b){a.state("app.misc",{"abstract":!0,data:{title:"Miscellaneous"}}).state("app.misc.pricingTable",{url:"/pricing-table",data:{title:"Pricing Table"},views:{"content@app":{templateUrl:"build/modules/misc/views/pricing-table.html"}}}).state("app.misc.invoice",{url:"/invoice",data:{title:"Invoice"},views:{"content@app":{templateUrl:"build/modules/misc/views/invoice.html"}}}).state("app.misc.error404",{url:"/404",data:{title:"Error 404"},views:{"content@app":{templateUrl:"build/modules/misc/views/error404.html"}}}).state("app.misc.error500",{url:"/500",data:{title:"Error 500"},views:{"content@app":{templateUrl:"build/modules/misc/views/error500.html"}}}).state("app.misc.blank",{url:"/blank",data:{title:"Blank"},views:{"content@app":{templateUrl:"build/modules/misc/views/blank.html"}}}).state("app.misc.emailTemplate",{url:"/email-template",data:{title:"Email Template"},views:{"content@app":{templateUrl:"build/modules/misc/views/email-template.html"}}}).state("app.misc.search",{url:"/search",data:{title:"Search"},views:{"content@app":{templateUrl:"build/modules/misc/views/search.html"}}}).state("app.misc.ckeditor",{url:"/ckeditor",data:{title:"CK Editor"},views:{"content@app":{templateUrl:"build/modules/misc/views/ckeditor.html",resolve:{deps:b.resolveDependencies(["modules/forms/directives/editors/smartCkEditor"])}}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
define('modules/smart-admin/module',["angular","angular-couch-potato","angular-ui-router"],function(a,b){var c=a.module("app.smartAdmin",["ui.router"]);return b.configureApp(c),c.config(["$stateProvider","$couchPotatoProvider",function(a){a.state("app.smartAdmin",{"abstract":!0,data:{title:"SmartAdmin Intel"}}).state("app.smartAdmin.appLayout",{url:"/app-layout",data:{title:"App Layout"},views:{"content@app":{templateUrl:"build/modules/smart-admin/views/app-layout.html"}}}).state("app.smartAdmin.diffVer",{url:"/different-versions",data:{title:"Different Versions"},views:{"content@app":{templateUrl:"build/modules/smart-admin/views/different-versions.html"}}})}]),c.run(["$couchPotato",function(a){c.lazy=a}]),c});
/**
 * UI-Router Extras: Sticky states, Future States, Deep State Redirect, Transition promise
 * @version v0.0.10
 * @link http://christopherthielen.github.io/ui-router-extras/
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */

(function (window, angular, undefined) {
angular.module("ct.ui.router.extras", [ 'ui.router' ]);


var DEBUG = false;

var forEach = angular.forEach;
var extend = angular.extend;
var isArray = angular.isArray;

var map = function (collection, callback) {
  
  var result = [];
  forEach(collection, function (item, index) {
    result.push(callback(item, index));
  });
  return result;
};

var keys = function (collection) {
  
  return map(collection, function (collection, key) {
    return key;
  });
};

var filter = function (collection, callback) {
  
  var result = [];
  forEach(collection, function (item, index) {
    if (callback(item, index)) {
      result.push(item);
    }
  });
  return result;
};

var filterObj = function (collection, callback) {
  
  var result = {};
  forEach(collection, function (item, index) {
    if (callback(item, index)) {
      result[index] = item;
    }
  });
  return result;
};

// Duplicates code in UI-Router common.js
function ancestors(first, second) {
  var path = [];

  for (var n in first.path) {
    if (first.path[n] !== second.path[n]) break;
    path.push(first.path[n]);
  }
  return path;
}

// Duplicates code in UI-Router common.js
function objectKeys(object) {
  if (Object.keys) {
    return Object.keys(object);
  }
  var result = [];

  angular.forEach(object, function (val, key) {
    result.push(key);
  });
  return result;
}

// Duplicates code in UI-Router common.js
function arraySearch(array, value) {
  if (Array.prototype.indexOf) {
    return array.indexOf(value, Number(arguments[2]) || 0);
  }
  var len = array.length >>> 0, from = Number(arguments[2]) || 0;
  from = (from < 0) ? Math.ceil(from) : Math.floor(from);

  if (from < 0) from += len;

  for (; from < len; from++) {
    if (from in array && array[from] === value) return from;
  }
  return -1;
}

// Duplicates code in UI-Router common.js
// Added compatibility code  (isArray check) to support both 0.2.x and 0.3.x series of UI-Router.
function inheritParams(currentParams, newParams, $current, $to) {
  var parents = ancestors($current, $to), parentParams, inherited = {}, inheritList = [];

  for (var i in parents) {
    if (!parents[i].params) continue;
    // This test allows compatibility with 0.2.x and 0.3.x (optional and object params)
    parentParams = isArray(parents[i].params) ? parents[i].params : objectKeys(parents[i].params);
    if (!parentParams.length) continue;

    for (var j in parentParams) {
      if (arraySearch(inheritList, parentParams[j]) >= 0) continue;
      inheritList.push(parentParams[j]);
      inherited[parentParams[j]] = currentParams[parentParams[j]];
    }
  }
  return extend({}, inherited, newParams);
}

function inherit(parent, extra) {
  return extend(new (extend(function () { }, {prototype: parent}))(), extra);
}

var ignoreDsr;
function resetIgnoreDsr() {
  ignoreDsr = undefined;
}

// Decorate $state.transitionTo to gain access to the last transition.options variable.
// This is used to process the options.ignoreDsr option
angular.module("ct.ui.router.extras").config([ "$provide", function ($provide) {
  var $state_transitionTo;
  $provide.decorator("$state", ['$delegate', '$q', function ($state, $q) {
    $state_transitionTo = $state.transitionTo;
    $state.transitionTo = function (to, toParams, options) {
      if (options.ignoreDsr) {
        ignoreDsr = options.ignoreDsr;
      }

      return $state_transitionTo.apply($state, arguments).then(
        function (result) {
          resetIgnoreDsr();
          return result;
        },
        function (err) {
          resetIgnoreDsr();
          return $q.reject(err);
        }
      );
    };
    return $state;
  }]);
}]);

angular.module("ct.ui.router.extras").service("$deepStateRedirect", [ '$rootScope', '$state', '$injector', function ($rootScope, $state, $injector) {
  var lastSubstate = {};
  var lastParams = {};
  var deepStateRedirectsByName = {};

  var REDIRECT = "Redirect", ANCESTOR_REDIRECT = "AncestorRedirect";

  function computeDeepStateStatus(state) {
    var name = state.name;
    if (deepStateRedirectsByName.hasOwnProperty(name))
      return deepStateRedirectsByName[name];
    recordDeepStateRedirectStatus(name);
  }

  function recordDeepStateRedirectStatus(stateName) {
    var state = $state.get(stateName);
    if (state && state.deepStateRedirect) {
      deepStateRedirectsByName[stateName] = REDIRECT;
      if (lastSubstate[stateName] === undefined)
        lastSubstate[stateName] = stateName;
    }

    var lastDot = stateName.lastIndexOf(".");
    if (lastDot != -1) {
      var parentStatus = recordDeepStateRedirectStatus(stateName.substr(0, lastDot));
      if (parentStatus && deepStateRedirectsByName[stateName] === undefined) {
        deepStateRedirectsByName[stateName] = ANCESTOR_REDIRECT;
      }
    }
    return deepStateRedirectsByName[stateName] || false;
  }

  $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
    function shouldRedirect() {
      if (ignoreDsr) return false;

      var deepStateStatus = computeDeepStateStatus(toState);
      var substate = lastSubstate[toState.name];

      // We're changing directly to one of the redirect (tab) states and we have a last substate recorded
      var isDSR = (deepStateStatus === REDIRECT && substate && substate != toState.name ? true : false);
      if (isDSR && angular.isFunction(toState.deepStateRedirect))
        return $injector.invoke(toState.deepStateRedirect, toState);

      return isDSR;
    }

    if (shouldRedirect()) { // send them to the last known state for that tab
      event.preventDefault();
      $state.go(lastSubstate[toState.name], lastParams[toState.name]);
    }
  });

  $rootScope.$on("$stateChangeSuccess", function (event, toState, toParams, fromState, fromParams) {
    var deepStateStatus = computeDeepStateStatus(toState);
    if (deepStateStatus) {
      var name = toState.name;
      angular.forEach(lastSubstate, function (deepState, redirectState) {
        if (name == redirectState || name.indexOf(redirectState + ".") != -1) {
          lastSubstate[redirectState] = name;
          lastParams[redirectState] = angular.copy(toParams);
        }
      });
    }
  });

  return {};
}]);

angular.module("ct.ui.router.extras").run(['$deepStateRedirect', function ($deepStateRedirect) {
  // Make sure $deepStateRedirect is instantiated
}]);

$StickyStateProvider.$inject = [ '$stateProvider' ];
function $StickyStateProvider($stateProvider) {
  // Holds all the states which are inactivated.  Inactivated states can be either sticky states, or descendants of sticky states.
  var inactiveStates = {}; // state.name -> (state)
  var stickyStates = {}; // state.name -> true
  var $state;

  // Called by $stateProvider.registerState();
  // registers a sticky state with $stickyStateProvider
  this.registerStickyState = function (state) {
    stickyStates[state.name] = state;
    // console.log("Registered sticky state: ", state);
  };

  this.enableDebug = function (enabled) {
    DEBUG = enabled;
  };

  this.$get = [  '$rootScope', '$state', '$stateParams', '$injector', '$log',
    function ($rootScope, $state, $stateParams, $injector, $log) {
      // Each inactive states is either a sticky state, or a child of a sticky state.
      // This function finds the closest ancestor sticky state, then find that state's parent.
      // Map all inactive states to their closest parent-to-sticky state.
      function mapInactives() {
        var mappedStates = {};
        angular.forEach(inactiveStates, function (state, name) {
          var stickyAncestors = getStickyStateStack(state);
          for (var i = 0; i < stickyAncestors.length; i++) {
            var parent = stickyAncestors[i].parent;
            mappedStates[parent.name] = mappedStates[parent.name] || [];
            mappedStates[parent.name].push(state);
          }
          if (mappedStates['']) {
            // This is necessary to compute Transition.inactives when there are sticky states are children to root state.
            mappedStates['__inactives'] = mappedStates[''];  // jshint ignore:line
          }
        });
        return mappedStates;
      }

      // Given a state, returns all ancestor states which are sticky.
      // Walks up the view's state's ancestry tree and locates each ancestor state which is marked as sticky.
      // Returns an array populated with only those ancestor sticky states.
      function getStickyStateStack(state) {
        var stack = [];
        if (!state) return stack;
        do {
          if (state.sticky) stack.push(state);
          state = state.parent;
        } while (state);
        stack.reverse();
        return stack;
      }

      // Used by processTransition to determine if what kind of sticky state transition this is.
      // returns { from: (bool), to: (bool) }
      function getStickyTransitionType(fromPath, toPath, keep) {
        if (fromPath[keep] === toPath[keep]) return { from: false, to: false };
        var stickyFromState = keep < fromPath.length && fromPath[keep].self.sticky;
        var stickyToState = keep < toPath.length && toPath[keep].self.sticky;
        return { from: stickyFromState, to: stickyToState };
      }

      // Returns a sticky transition type necessary to enter the state.
      // Transition can be: reactivate, updateStateParams, or enter

      // Note: if a state is being reactivated but params dont match, we treat
      // it as a Exit/Enter, thus the special "updateStateParams" transition.
      // If a parent inactivated state has "updateStateParams" transition type, then
      // all descendant states must also be exit/entered, thus the first line of this function.
      function getEnterTransition(state, stateParams, ancestorParamsChanged) {
        if (ancestorParamsChanged) return "updateStateParams";
        var inactiveState = inactiveStates[state.self.name];
        if (!inactiveState) return "enter";
//      if (inactiveState.locals == null || inactiveState.locals.globals == null) debugger;
        var paramsMatch = equalForKeys(stateParams, inactiveState.locals.globals.$stateParams, state.ownParams);
//      if (DEBUG) $log.debug("getEnterTransition: " + state.name + (paramsMatch ? ": reactivate" : ": updateStateParams"));
        return paramsMatch ? "reactivate" : "updateStateParams";
      }

      // Given a state and (optional) stateParams, returns the inactivated state from the inactive sticky state registry.
      function getInactivatedState(state, stateParams) {
        var inactiveState = inactiveStates[state.name];
        if (!inactiveState) return null;
        if (!stateParams) return inactiveState;
        var paramsMatch = equalForKeys(stateParams, inactiveState.locals.globals.$stateParams, state.ownParams);
        return paramsMatch ? inactiveState : null;
      }

      // Duplicates logic in $state.transitionTo, primarily to find the pivot state (i.e., the "keep" value)
      function equalForKeys(a, b, keys) {
        if (!keys) {
          keys = [];
          for (var n in a) keys.push(n); // Used instead of Object.keys() for IE8 compatibility
        }

        for (var i = 0; i < keys.length; i++) {
          var k = keys[i];
          if (a[k] != b[k]) return false; // Not '===', values aren't necessarily normalized
        }
        return true;
      }

      var stickySupport = {
        getInactiveStates: function () {
          var states = [];
          angular.forEach(inactiveStates, function (state) {
            states.push(state);
          });
          return states;
        },
        getInactiveStatesByParent: function () {
          return mapInactives();
        },
        // Main API for $stickyState, used by $state.
        // Processes a potential transition, returns an object with the following attributes:
        // {
        //    inactives: Array of all states which will be inactive if the transition is completed. (both previously and newly inactivated)
        //    enter: Enter transition type for all added states.  This is a sticky array to "toStates" array in $state.transitionTo.
        //    exit: Exit transition type for all removed states.  This is a sticky array to "fromStates" array in $state.transitionTo.
        // }
        processTransition: function (transition) {
          // This object is returned
          var result = { inactives: [], enter: [], exit: [], keep: 0 };
          var fromPath = transition.fromState.path,
            fromParams = transition.fromParams,
            toPath = transition.toState.path,
            toParams = transition.toParams,
            options = transition.options;
          var keep = 0, state = toPath[keep];

          if (options.inherit) {
            toParams = inheritParams($stateParams, toParams || {}, $state.$current, transition.toState);
          }

          while (state && state === fromPath[keep] && equalForKeys(toParams, fromParams, state.ownParams)) {
            state = toPath[++keep];
          }

          result.keep = keep;

          var idx, deepestUpdatedParams, deepestReactivate, reactivatedStatesByName = {}, pType = getStickyTransitionType(fromPath, toPath, keep);
          var ancestorUpdated = false; // When ancestor params change, treat reactivation as exit/enter

          // Calculate the "enter" transitions for new states in toPath
          // Enter transitions will be either "enter", "reactivate", or "updateStateParams" where
          //   enter: full resolve, no special logic
          //   reactivate: use previous locals
          //   updateStateParams: like 'enter', except exit the inactive state before entering it.
          for (idx = keep; idx < toPath.length; idx++) {
            var enterTrans = !pType.to ? "enter" : getEnterTransition(toPath[idx], transition.toParams, ancestorUpdated);
            ancestorUpdated = (ancestorUpdated || enterTrans == 'updateStateParams');
            result.enter[idx] = enterTrans;
            // If we're reactivating a state, make a note of it, so we can remove that state from the "inactive" list
            if (enterTrans == 'reactivate')
              deepestReactivate = reactivatedStatesByName[toPath[idx].name] = toPath[idx];
            if (enterTrans == 'updateStateParams')
              deepestUpdatedParams = toPath[idx];
          }
          deepestReactivate = deepestReactivate ? deepestReactivate.self.name + "." : "";
          deepestUpdatedParams = deepestUpdatedParams ? deepestUpdatedParams.self.name + "." : "";

          // Inactive states, before the transition is processed, mapped to the parent to the sticky state.
          var inactivesByParent = mapInactives();

          // root ("") is always kept. Find the remaining names of the kept path.
          var keptStateNames = [""].concat(map(fromPath.slice(0, keep), function (state) {
            return state.self.name;
          }));

          // Locate currently and newly inactive states (at pivot and above) and store them in the output array 'inactives'.
          angular.forEach(keptStateNames, function (name) {
            var inactiveChildren = inactivesByParent[name];
            for (var i = 0; inactiveChildren && i < inactiveChildren.length; i++) {
              var child = inactiveChildren[i];
              // Don't organize state as inactive if we're about to reactivate it.
              if (!reactivatedStatesByName[child.name] &&
                (!deepestReactivate || (child.self.name.indexOf(deepestReactivate) !== 0)) &&
                (!deepestUpdatedParams || (child.self.name.indexOf(deepestUpdatedParams) !== 0)))
                result.inactives.push(child);
            }
          });

          // Calculate the "exit" transition for states not kept, in fromPath.
          // Exit transition can be one of:
          //   exit: standard state exit logic
          //   inactivate: register state as an inactive state
          for (idx = keep; idx < fromPath.length; idx++) {
            var exitTrans = "exit";
            if (pType.from) {
              // State is being inactivated, note this in result.inactives array
              result.inactives.push(fromPath[idx]);
              exitTrans = "inactivate";
            }
            result.exit[idx] = exitTrans;
          }

          return result;
        },

        // Adds a state to the inactivated sticky state registry.
        stateInactivated: function (state) {
          // Keep locals around.
          inactiveStates[state.self.name] = state;
          // Notify states they are being Inactivated (i.e., a different
          // sticky state tree is now active).
          state.self.status = 'inactive';
          if (state.self.onInactivate)
            $injector.invoke(state.self.onInactivate, state.self, state.locals.globals);
        },

        // Removes a previously inactivated state from the inactive sticky state registry
        stateReactivated: function (state) {
          if (inactiveStates[state.self.name]) {
            delete inactiveStates[state.self.name];
          }
          state.self.status = 'entered';
//        if (state.locals == null || state.locals.globals == null) debugger;
          if (state.self.onReactivate)
            $injector.invoke(state.self.onReactivate, state.self, state.locals.globals);
        },

        // Exits all inactivated descendant substates when the ancestor state is exited.
        // When transitionTo is exiting a state, this function is called with the state being exited.  It checks the
        // registry of inactivated states for descendants of the exited state and also exits those descendants.  It then
        // removes the locals and de-registers the state from the inactivated registry.
        stateExiting: function (exiting, exitQueue, onExit) {
          var exitingNames = {};
          angular.forEach(exitQueue, function (state) {
            exitingNames[state.self.name] = true;
          });

          angular.forEach(inactiveStates, function (inactiveExiting, name) {
            // TODO: Might need to run the inactivations in the proper depth-first order?
            if (!exitingNames[name] && inactiveExiting.includes[exiting.name]) {
              if (DEBUG) $log.debug("Exiting " + name + " because it's a substate of " + exiting.name + " and wasn't found in ", exitingNames);
              if (inactiveExiting.self.onExit)
                $injector.invoke(inactiveExiting.self.onExit, inactiveExiting.self, inactiveExiting.locals.globals);
              inactiveExiting.locals = null;
              inactiveExiting.self.status = 'exited';
              delete inactiveStates[name];
            }
          });

          if (onExit)
            $injector.invoke(onExit, exiting.self, exiting.locals.globals);
          exiting.locals = null;
          exiting.self.status = 'exited';
          delete inactiveStates[exiting.self.name];
        },

        // Removes a previously inactivated state from the inactive sticky state registry
        stateEntering: function (entering, params, onEnter) {
          var inactivatedState = getInactivatedState(entering);
          if (inactivatedState && !getInactivatedState(entering, params)) {
            var savedLocals = entering.locals;
            this.stateExiting(inactivatedState);
            entering.locals = savedLocals;
          }
          entering.self.status = 'entered';

          if (onEnter)
            $injector.invoke(onEnter, entering.self, entering.locals.globals);
        }
      };

      return stickySupport;
    }];
}

angular.module("ct.ui.router.extras").provider("$stickyState", $StickyStateProvider);

/**
 * Sticky States makes entire state trees "sticky". Sticky state trees are retained until their parent state is
 * exited. This can be useful to allow multiple modules, peers to each other, each module having its own independent
 * state tree.  The peer modules can be activated and inactivated without any loss of their internal context, including
 * DOM content such as unvalidated/partially filled in forms, and even scroll position.
 *
 * DOM content is retained by declaring a named ui-view in the parent state, and filling it in with a named view from the
 * sticky state.
 *
 * Technical overview:
 *
 * ---PATHS---
 * UI-Router uses state paths to manage entering and exiting of individual states.  Each state "A.B.C.X" has its own path, starting
 * from the root state ("") and ending at the state "X".  The path is composed the final state "X"'s ancestors, e.g.,
 * [ "", "A", "B", "C", "X" ].
 *
 * When a transition is processed, the previous path (fromState.path) is compared with the requested destination path
 * (toState.path).  All states that the from and to paths have in common are "kept" during the transition.  The last
 * "kept" element in the path is the "pivot".
 *
 * ---VIEWS---
 * A View in UI-Router consists of a controller and a template.  Each view belongs to one state, and a state can have many
 * views.  Each view plugs into a ui-view element in the DOM of one of the parent state's view(s).
 *
 * View context is managed in UI-Router using a 'state locals' concept. When a state's views are fully loaded, those views
 * are placed on the states 'locals' object.  Each locals object prototypally inherits from its parent state's locals object.
 * This means that state "A.B.C.X"'s locals object also has all of state "A.B.C"'s locals as well as those from "A.B" and "A".
 * The root state ("") defines no views, but it is included in the protypal inheritance chain.
 *
 * The locals object is used by the ui-view directive to load the template, render the content, create the child scope,
 * initialize the controller, etc.  The ui-view directives caches the locals in a closure variable.  If the locals are
 * identical (===), then the ui-view directive exits early, and does no rendering.
 *
 * In stock UI-Router, when a state is exited, that state's locals object is deleted and those views are cleaned up by
 * the ui-view directive shortly.
 *
 * ---Sticky States---
 * UI-Router Extras keeps views for inactive states live, even when UI-Router thinks it has exited them.  It does this
 * by creating a pseudo state called "__inactives" that is the parent of the root state.  It also then defines a locals
 * object on the "__inactives" state, which the root state protoypally inherits from.  By doing this, views for inactive
 * states are accessible through locals object's protoypal inheritance chain from any state in the system.
 *
 * ---Transitions---
 * UI-Router Extras decorates the $state.transitionTo function.  While a transition is in progress, the toState and
 * fromState internal state representations are modified in order to coerce stock UI-Router's transitionTo() into performing
 * the appropriate operations.  When the transition promise is completed, the original toState and fromState values are
 * restored.
 *
 * Stock UI-Router's $state.transitionTo function uses toState.path and fromState.path to manage entering and exiting
 * states.  UI-Router Extras takes advantage of those internal implementation details and prepares a toState.path and
 * fromState.path which coerces UI-Router into entering and exiting the correct states, or more importantly, not entering
 * and not exiting inactive or sticky states.  It also replaces state.self.onEnter and state.self.onExit for elements in
 * the paths when they are being inactivated or reactivated.
 */



// ------------------------ Sticky State module-level variables -----------------------------------------------
var _StickyState; // internal reference to $stickyStateProvider
var internalStates = {}; // Map { statename -> InternalStateObj } holds internal representation of all states
var root, // Root state, internal representation
  pendingTransitions = [], // One transition may supersede another.  This holds references to all pending transitions
  pendingRestore, // The restore function from the superseded transition
  inactivePseudoState; // This pseudo state holds all the inactive states' locals (resolved state data, such as views etc)

// Creates a blank surrogate state
function SurrogateState(type) {
  return {
    resolve: { },
    locals: {
      globals: root && root.locals && root.locals.globals
    },
    views: { },
    self: { },
    params: { },
    ownParams: [],
    surrogateType: type
  };
}

// ------------------------ Sticky State registration and initialization code ----------------------------------
// Grab a copy of the $stickyState service for use by the transition management code
angular.module("ct.ui.router.extras").run(["$stickyState", function ($stickyState) {
  _StickyState = $stickyState;
}]);

angular.module("ct.ui.router.extras").config(
  [ "$provide", "$stateProvider", '$stickyStateProvider',
    function ($provide, $stateProvider, $stickyStateProvider) {
      // inactivePseudoState (__inactives) holds all the inactive locals which includes resolved states data, i.e., views, scope, etc
      inactivePseudoState = angular.extend(new SurrogateState("__inactives"), { self: {  name: '__inactives'  } });
      // Reset other module scoped variables.  This is to primarily to flush any previous state during karma runs.
      root = pendingRestore = undefined;
      pendingTransitions = [];

      // Decorate any state attribute in order to get access to the internal state representation.
      $stateProvider.decorator('parent', function (state, parentFn) {
        // Capture each internal UI-Router state representations as opposed to the user-defined state object.
        // The internal state is, e.g., the state returned by $state.$current as opposed to $state.current
        internalStates[state.self.name] = state;
        // Add an accessor for the internal state from the user defined state
        state.self.$$state = function () {
          return internalStates[state.self.name];
        };

        // Register the ones marked as "sticky"
        if (state.self.sticky === true) {
          $stickyStateProvider.registerStickyState(state.self);
        }

        return parentFn(state);
      });

      var $state_transitionTo; // internal reference to the real $state.transitionTo function
      // Decorate the $state service, so we can decorate the $state.transitionTo() function with sticky state stuff.
      $provide.decorator("$state", ['$delegate', '$log', '$q', function ($state, $log, $q) {
        // Note: this code gets run only on the first state that is decorated
        root = $state.$current;
        root.parent = inactivePseudoState; // Make inactivePsuedoState the parent of root.  "wat"
        inactivePseudoState.parent = undefined; // Make inactivePsuedoState the real root.
        root.locals = inherit(inactivePseudoState.locals, root.locals); // make root locals extend the __inactives locals.
        delete inactivePseudoState.locals.globals;

        // Hold on to the real $state.transitionTo in a module-scope variable.
        $state_transitionTo = $state.transitionTo;

        // ------------------------ Decorated transitionTo implementation begins here ---------------------------
        $state.transitionTo = function (to, toParams, options) {
          // TODO: Move this to module.run?
          // TODO: I'd rather have root.locals prototypally inherit from inactivePseudoState.locals
          // Link root.locals and inactives.locals.  Do this at runtime, after root.locals has been set.
          if (!inactivePseudoState.locals)
            inactivePseudoState.locals = root.locals;
          var idx = pendingTransitions.length;
          if (pendingRestore) {
            pendingRestore();
            if (DEBUG) {
              $log.debug("Restored paths from pending transition");
            }
          }

          var fromState = $state.$current, fromParams = $state.params;
          var rel = options && options.relative || $state.$current; // Not sure if/when $state.$current is appropriate here.
          var toStateSelf = $state.get(to, rel); // exposes findState relative path functionality, returns state.self
          var savedToStatePath, savedFromStatePath, stickyTransitions;
          var reactivated = [], exited = [], terminalReactivatedState;

          var noop = function () {
          };
          // Sticky states works by modifying the internal state objects of toState and fromState, especially their .path(s).
          // The restore() function is a closure scoped function that restores those states' definitions to their original values.
          var restore = function () {
            if (savedToStatePath) {
              toState.path = savedToStatePath;
              savedToStatePath = null;
            }

            if (savedFromStatePath) {
              fromState.path = savedFromStatePath;
              savedFromStatePath = null;
            }

            angular.forEach(restore.restoreFunctions, function (restoreFunction) {
              restoreFunction();
            });
            // Restore is done, now set the restore function to noop in case it gets called again.
            restore = noop;
            // pendingRestore keeps track of a transition that is in progress.  It allows the decorated transitionTo
            // method to be re-entrant (for example, when superceding a transition, i.e., redirect).  The decorated
            // transitionTo checks right away if there is a pending transition in progress and restores the paths
            // if so using pendingRestore.
            pendingRestore = null;
            pendingTransitions.splice(idx, 1); // Remove this transition from the list
          };

          // All decorated transitions have their toState.path and fromState.path replaced.  Surrogate states also make
          // additional changes to the states definition before handing the transition off to UI-Router. In particular,
          // certain types of surrogate states modify the state.self object's onEnter or onExit callbacks.
          // Those surrogate states must then register additional restore steps using restore.addRestoreFunction(fn)
          restore.restoreFunctions = [];
          restore.addRestoreFunction = function addRestoreFunction(fn) {
            this.restoreFunctions.push(fn);
          };


          // --------------------- Surrogate State Functions ------------------------
          // During a transition, the .path arrays in toState and fromState are replaced.  Individual path elements
          // (states) which aren't being "kept" are replaced with surrogate elements (states).  This section of the code
          // has factory functions for all the different types of surrogate states.


          function stateReactivatedSurrogatePhase1(state) {
            var surrogate = angular.extend(new SurrogateState("reactivate_phase1"), { locals: state.locals });
            surrogate.self = angular.extend({}, state.self);
            return surrogate;
          }

          function stateReactivatedSurrogatePhase2(state) {
            var surrogate = angular.extend(new SurrogateState("reactivate_phase2"), state);
            var oldOnEnter = surrogate.self.onEnter;
            surrogate.resolve = {}; // Don't re-resolve when reactivating states (fixes issue #22)
            // TODO: Not 100% sure if this is necessary.  I think resolveState will load the views if I don't do this.
            surrogate.views = {}; // Don't re-activate controllers when reactivating states (fixes issue #22)
            surrogate.self.onEnter = function () {
              // ui-router sets locals on the surrogate to a blank locals (because we gave it nothing to resolve)
              // Re-set it back to the already loaded state.locals here.
              surrogate.locals = state.locals;
              _StickyState.stateReactivated(state);
            };
            restore.addRestoreFunction(function () {
              state.self.onEnter = oldOnEnter;
            });
            return surrogate;
          }

          function stateInactivatedSurrogate(state) {
            var surrogate = new SurrogateState("inactivate");
            surrogate.self = state.self;
            var oldOnExit = state.self.onExit;
            surrogate.self.onExit = function () {
              _StickyState.stateInactivated(state);
            };
            restore.addRestoreFunction(function () {
              state.self.onExit = oldOnExit;
            });
            return surrogate;
          }

          function stateEnteredSurrogate(state, toParams) {
            var oldOnEnter = state.self.onEnter;
            state.self.onEnter = function () {
              _StickyState.stateEntering(state, toParams, oldOnEnter);
            };
            restore.addRestoreFunction(function () {
              state.self.onEnter = oldOnEnter;
            });

            return state;
          }

          function stateExitedSurrogate(state) {
            var oldOnExit = state.self.onExit;
            state.self.onExit = function () {
              _StickyState.stateExiting(state, exited, oldOnExit);
            };
            restore.addRestoreFunction(function () {
              state.self.onExit = oldOnExit;
            });

            return state;
          }


          // --------------------- decorated .transitionTo() logic starts here ------------------------
          if (toStateSelf) {
            var toState = internalStates[toStateSelf.name]; // have the state, now grab the internal state representation
            if (toState) {
              // Save the toState and fromState paths to be restored using restore()
              savedToStatePath = toState.path;
              savedFromStatePath = fromState.path;

              var currentTransition = {toState: toState, toParams: toParams || {}, fromState: fromState, fromParams: fromParams || {}, options: options};

              pendingTransitions.push(currentTransition); // TODO: See if a list of pending transitions is necessary.
              pendingRestore = restore;

              // $StickyStateProvider.processTransition analyzes the states involved in the pending transition.  It
              // returns an object that tells us:
              // 1) if we're involved in a sticky-type transition
              // 2) what types of exit transitions will occur for each "exited" path element
              // 3) what types of enter transitions will occur for each "entered" path element
              // 4) which states will be inactive if the transition succeeds.
              stickyTransitions = _StickyState.processTransition(currentTransition);

              if (DEBUG) debugTransition($log, currentTransition, stickyTransitions);

              // Begin processing of surrogate to and from paths.
              var surrogateToPath = toState.path.slice(0, stickyTransitions.keep);
              var surrogateFromPath = fromState.path.slice(0, stickyTransitions.keep);

              // Clear out and reload inactivePseudoState.locals each time transitionTo is called
              angular.forEach(inactivePseudoState.locals, function (local, name) {
                if (name.indexOf("@") != -1) delete inactivePseudoState.locals[name];
              });

              // Find all states that will be inactive once the transition succeeds.  For each of those states,
              // place its view-locals on the __inactives pseudostate's .locals.  This allows the ui-view directive
              // to access them and render the inactive views.
              for (var i = 0; i < stickyTransitions.inactives.length; i++) {
                var iLocals = stickyTransitions.inactives[i].locals;
                angular.forEach(iLocals, function (view, name) {
                  if (iLocals.hasOwnProperty(name) && name.indexOf("@") != -1) { // Only grab this state's "view" locals
                    inactivePseudoState.locals[name] = view; // Add all inactive views not already included.
                  }
                });
              }

              // Find all the states the transition will be entering.  For each entered state, check entered-state-transition-type
              // Depending on the entered-state transition type, place the proper surrogate state on the surrogate toPath.
              angular.forEach(stickyTransitions.enter, function (value, idx) {
                var surrogate;
                if (value === "reactivate") {
                  // Reactivated states require TWO surrogates.  The "phase 1 reactivated surrogates" are added to both
                  // to.path and from.path, and as such, are considered to be "kept" by UI-Router.
                  // This is required to get UI-Router to add the surrogate locals to the protoypal locals object
                  surrogate = stateReactivatedSurrogatePhase1(toState.path[idx]);
                  surrogateToPath.push(surrogate);
                  surrogateFromPath.push(surrogate);  // so toPath[i] === fromPath[i]

                  // The "phase 2 reactivated surrogate" is added to the END of the .path, after all the phase 1
                  // surrogates have been added.
                  reactivated.push(stateReactivatedSurrogatePhase2(toState.path[idx]));
                  terminalReactivatedState = surrogate;
                } else if (value === "updateStateParams") {
                  // If the state params have been changed, we need to exit any inactive states and re-enter them.
                  surrogate = stateEnteredSurrogate(toState.path[idx]);
                  surrogateToPath.push(surrogate);
                  terminalReactivatedState = surrogate;
                } else if (value === "enter") {
                  // Standard enter transition.  We still wrap it in a surrogate.
                  surrogateToPath.push(stateEnteredSurrogate(toState.path[idx]));
                }
              });

              // Find all the states the transition will be exiting.  For each exited state, check the exited-state-transition-type.
              // Depending on the exited-state transition type, place a surrogate state on the surrogate fromPath.
              angular.forEach(stickyTransitions.exit, function (value, idx) {
                var exiting = fromState.path[idx];
                if (value === "inactivate") {
                  surrogateFromPath.push(stateInactivatedSurrogate(exiting));
                  exited.push(exiting);
                } else if (value === "exit") {
                  surrogateFromPath.push(stateExitedSurrogate(exiting));
                  exited.push(exiting);
                }
              });

              // Add surrogate for reactivated to ToPath again, this time without a matching FromPath entry
              // This is to get ui-router to call the surrogate's onEnter callback.
              if (reactivated.length) {
                angular.forEach(reactivated, function (surrogate) {
                  surrogateToPath.push(surrogate);
                });
              }

              // In some cases, we may be some state, but not its children states.  If that's the case, we have to
              // exit all the children of the deepest reactivated state.
              if (terminalReactivatedState) {
                var prefix = terminalReactivatedState.self.name + ".";
                var inactiveStates = _StickyState.getInactiveStates();
                var inactiveOrphans = [];
                inactiveStates.forEach(function (exiting) {
                  if (exiting.self.name.indexOf(prefix) === 0) {
                    inactiveOrphans.push(exiting);
                  }
                });
                inactiveOrphans.sort();
                inactiveOrphans.reverse();
                // Add surrogate exited states for all orphaned descendants of the Deepest Reactivated State
                surrogateFromPath = surrogateFromPath.concat(map(inactiveOrphans, function (exiting) {
                  return stateExitedSurrogate(exiting);
                }));
                exited = exited.concat(inactiveOrphans);
              }

              // Replace the .path variables.  toState.path and fromState.path are now ready for a sticky transition.
              toState.path = surrogateToPath;
              fromState.path = surrogateFromPath;

              var pathMessage = function (state) {
                return (state.surrogateType ? state.surrogateType + ":" : "") + state.self.name;
              };
              if (DEBUG) $log.debug("SurrogateFromPath: ", map(surrogateFromPath, pathMessage));
              if (DEBUG) $log.debug("SurrogateToPath:   ", map(surrogateToPath, pathMessage));
            }
          }

          // toState and fromState are all set up; now run stock UI-Router's $state.transitionTo().
          var transitionPromise = $state_transitionTo.apply($state, arguments);

          // Add post-transition promise handlers, then return the promise to the original caller.
          return transitionPromise.then(function transitionSuccess(state) {
            // First, restore toState and fromState to their original values.
            restore();
            if (DEBUG)  debugViewsAfterSuccess($log, internalStates[state.name], $state);

            state.status = 'active';  // TODO: This status is used in statevis.js, and almost certainly belongs elsewhere.

            return state;
          }, function transitionFailed(err) {
            restore();
            if (DEBUG &&
              err.message !== "transition prevented" &&
              err.message !== "transition aborted" &&
              err.message !== "transition superseded") {
              $log.debug("transition failed", err);
              console.log(err.stack);
            }
            return $q.reject(err);
          });
        };
        return $state;
      }]);
    }
  ]
);

function debugTransition($log, currentTransition, stickyTransition) {
  function message(path, index, state) {
    return (path[index] ? path[index].toUpperCase() + ": " + state.self.name : "(" + state.self.name + ")");
  }

  var inactiveLogVar = map(stickyTransition.inactives, function (state) {
    return state.self.name;
  });
  var enterLogVar = map(currentTransition.toState.path, function (state, index) {
    return message(stickyTransition.enter, index, state);
  });
  var exitLogVar = map(currentTransition.fromState.path, function (state, index) {
    return message(stickyTransition.exit, index, state);
  });

  var transitionMessage = currentTransition.fromState.self.name + ": " +
    angular.toJson(currentTransition.fromParams) + ": " +
    " -> " +
    currentTransition.toState.self.name + ": " +
    angular.toJson(currentTransition.toParams);

  $log.debug("   Current transition: ", transitionMessage);
  $log.debug("Before transition, inactives are:   : ", map(_StickyState.getInactiveStates(), function (s) {
    return s.self.name;
  }));
  $log.debug("After transition,  inactives will be: ", inactiveLogVar);
  $log.debug("Transition will exit:  ", exitLogVar);
  $log.debug("Transition will enter: ", enterLogVar);
}

function debugViewsAfterSuccess($log, currentState, $state) {
  $log.debug("Current state: " + currentState.self.name + ", inactive states: ", map(_StickyState.getInactiveStates(), function (s) {
    return s.self.name;
  }));

  var viewMsg = function (local, name) {
    return "'" + name + "' (" + local.$$state.name + ")";
  };
  var statesOnly = function (local, name) {
    return name != 'globals' && name != 'resolve';
  };
  var viewsForState = function (state) {
    var views = map(filterObj(state.locals, statesOnly), viewMsg).join(", ");
    return "(" + (state.self.name ? state.self.name : "root") + ".locals" + (views.length ? ": " + views : "") + ")";
  };

  var message = viewsForState(currentState);
  var parent = currentState.parent;
  while (parent && parent !== currentState) {
    if (parent.self.name === "") {
      // Show the __inactives before showing root state.
      message = viewsForState($state.$current.path[0]) + " / " + message;
    }
    message = viewsForState(parent) + " / " + message;
    currentState = parent;
    parent = currentState.parent;
  }

  $log.debug("Views: " + message);
}


angular.module('ct.ui.router.extras').provider('$futureState',
  [ '$stateProvider', '$urlRouterProvider',
    function _futureStateProvider($stateProvider, $urlRouterProvider) {
      var stateFactories = {}, futureStates = {}, futureUrlPrefixes = {};
      var transitionPending = false, resolveFunctions = [], initPromise, initDone = false;
      var provider = this;

      // This function registers a promiseFn, to be resolved before the url/state matching code
      // will reject a route.  The promiseFn is injected/executed using the runtime $injector.
      // The function should return a promise.
      // When all registered promises are resolved, then the route is re-sync'ed.

      // Example: function($http) {
      //  return $http.get('//server.com/api/DynamicFutureStates').then(function(data) {
      //    angular.forEach(data.futureStates, function(fstate) { $futureStateProvider.futureState(fstate); });
      //  };
      // }
      this.addResolve = function (promiseFn) {
        resolveFunctions.push(promiseFn);
      };

      // Register a state factory function for a particular future-state type.  This factory, given a future-state object,
      // should create a ui-router state.
      // The factory function is injected/executed using the runtime $injector.  The future-state is injected as 'futureState'.

      // Example:
      //    $futureStateProvider.stateFactory('test', function(futureState) {
      //      return {
      //        name: futureState.stateName,
      //        url: futureState.urlFragment,
      //        template: '<h3>Future State Template</h3>',
      //        controller: function() {
      //          console.log("Entered state " + futureState.stateName);
      //        }
      //      }
      //    });
      this.stateFactory = function (futureStateType, factory) {
        stateFactories[futureStateType] = factory;
      };

      this.futureState = function (futureState) {
        futureStates[futureState.stateName] = futureState;
        futureUrlPrefixes[futureState.urlPrefix] = futureState;
      };

      this.get = function () {
        return angular.extend({}, futureStates);
      };

      /* options is an object with at least a name or url attribute */
      function findFutureState($state, options) {
        if (options.name) {
          var nameComponents = options.name.split(/\./);
          while (nameComponents.length) {
            var stateName = nameComponents.join(".");
            if ($state.get(stateName))
              return null; // State is already defined; nothing to do
            if (futureStates[stateName])
              return futureStates[stateName];
            nameComponents.pop();
          }
        }

        if (options.url) {
          var urlComponents = options.url.split(/\//);
          while (urlComponents.length) {
            var urlPrefix = urlComponents.join("/");
            if (futureUrlPrefixes[urlPrefix])
              return futureUrlPrefixes[urlPrefix];
            urlComponents.pop();
          }
        }
      }

      function lazyLoadState($injector, futureState) {
        if (!futureState) {
          var deferred = $q.defer();
          deferred.reject("No lazyState passed in " + futureState);
          return deferred.promise;
        }

        var type = futureState.type;
        var factory = stateFactories[type];
        if (!factory) throw Error("No state factory for futureState.type: " + (futureState && futureState.type));
        return $injector.invoke(factory, factory, { futureState: futureState });
      }

      function futureState_otherwise($injector, $location) {
        var resyncing = false;
        var $log = $injector.get("$log");

        var otherwiseFunc = [ '$state',
          function otherwiseFunc($state) {
            $log.debug("Unable to map " + $location.path());
            $location.url("/");
          }];

        var lazyLoadMissingState =
          ['$rootScope', '$urlRouter', '$state',
            function lazyLoadMissingState($rootScope, $urlRouter, $state) {
              if (!initDone) {
                // Asynchronously load state definitions, then resync URL
                initPromise().then(function initialResync() {
                  resyncing = true;
                  $urlRouter.sync();
                  resyncing = false;
                });
                initDone = true;
                return;
              }


              var futureState = findFutureState($state, { url: $location.path() });
              if (!futureState) {
                return $injector.invoke(otherwiseFunc);
              }

              transitionPending = true;
              // Config loaded.  Asynchronously lazy-load state definition from URL fragment, if mapped.
              lazyLoadState($injector, futureState).then(function lazyLoadedStateCallback(state) {
                // TODO: Should have a specific resolve value that says 'dont register a state because I already did'
                if (state && !$state.get(state))
                  $stateProvider.state(state);
                resyncing = true;
                $urlRouter.sync();
                resyncing = false;
                transitionPending = false;
              }, function lazyLoadStateAborted() {
                transitionPending = false;
                $state.go("top");
              });
            }];
        if (transitionPending) return;

        var nextFn = resyncing ? otherwiseFunc : lazyLoadMissingState;
        return $injector.invoke(nextFn);
      }

      $urlRouterProvider.otherwise(futureState_otherwise);

      var serviceObject = {
        getResolvePromise: function () {
          return initPromise();
        }
      };

      // Used in .run() block to init
      this.$get = [ '$injector', '$state', '$q', '$rootScope', '$urlRouter', '$timeout', '$log',
        function futureStateProvider_get($injector, $state, $q, $rootScope, $urlRouter, $timeout, $log) {
          function init() {
            $rootScope.$on("$stateNotFound", function futureState_notFound(event, unfoundState, fromState, fromParams) {
              if (transitionPending) return;
              $log.debug("event, unfoundState, fromState, fromParams", event, unfoundState, fromState, fromParams);

              var futureState = findFutureState($state, { name: unfoundState.to });
              if (!futureState) return;

              event.preventDefault();
              transitionPending = true;

              var promise = lazyLoadState($injector, futureState);
              promise.then(function (state) {
                // TODO: Should have a specific resolve value that says 'dont register a state because I already did'
                if (state)
                  $stateProvider.state(state);
                $state.go(unfoundState.to, unfoundState.toParams);
                transitionPending = false;
              }, function (error) {
                console.log("failed to lazy load state ", error);
                $state.go(fromState, fromParams);
                transitionPending = false;
              });
            });

            // Do this better.  Want to load remote config once, before everything else
            if (!initPromise) {
              var promises = [];
              angular.forEach(resolveFunctions, function (promiseFn) {
                promises.push($injector.invoke(promiseFn));
              });
              initPromise = function () {
                return $q.all(promises);
              };
//          initPromise = _.once(function flattenFutureStates() {
//            var allPromises = $q.all(promises);
//            return allPromises.then(function(data) { 
//              return _.flatten(data); 
//            });
//          });
            }

            // TODO: analyze this. I'm calling $urlRouter.sync() in two places for retry-initial-transition.
            // TODO: I should only need to do this once.  Pick the better place and remove the extra resync.
            initPromise().then(function retryInitialState() {
              $timeout(function () {
                if ($state.transition) {
                  $state.transition.then($urlRouter.sync, $urlRouter.sync);
                } else {
                  $urlRouter.sync();
                }
              });
            });
          }

          init();

          serviceObject.state = $stateProvider.state;
          serviceObject.futureState = provider.futureState;
          serviceObject.get = provider.get;

          return serviceObject;
        }];
    }]);

angular.module('ct.ui.router.extras').run(['$futureState',
  // Just inject $futureState so it gets initialized.
  function ($futureState) {
  }
]);

angular.module('ct.ui.router.extras').service("$previousState",
  [ '$rootScope', '$state',
    function ($rootScope, $state) {
      var previous = null;
      var memos = {};

      var lastPrevious = null;

      $rootScope.$on("$stateChangeStart", function (evt, toState, toStateParams, fromState, fromStateParams) {
        // State change is starting.  Keep track of the CURRENT previous state in case we have to restore it
        lastPrevious = previous;
        previous = { state: fromState, params: fromStateParams };
      });

      $rootScope.$on("$stateChangeError", function () {
        // State change did not occur due to an error.  Restore the previous previous state.
        previous = lastPrevious;
        lastPrevious = null;
      });

      $rootScope.$on("$stateChangeSuccess", function () {
        lastPrevious = null;
      });

      var $previousState = {
        get: function (memoName) {
          return memoName ? memos[memoName] : previous;
        },
        go: function (memoName, options) {
          var to = $previousState.get(memoName);
          return $state.go(to.state, to.params, options);
        },
        memo: function (memoName) {
          memos[memoName] = previous;
        },
        forget: function (memoName) {
          delete memos[memoName];
        }
      };

      return $previousState;
    }
  ]
);

angular.module('ct.ui.router.extras').run(['$previousState', function ($previousState) {
  // Inject $previousState so it can register $rootScope events
}]);


angular.module("ct.ui.router.extras").config( [ "$provide",  function ($provide) {
      // Decorate the $state service, so we can replace $state.transitionTo()
      $provide.decorator("$state", ['$delegate', '$rootScope', '$q', '$injector',
        function ($state, $rootScope, $q, $injector) {
          // Keep an internal reference to the real $state.transitionTo function
          var $state_transitionTo = $state.transitionTo;
          // $state.transitionTo can be re-entered.  Keep track of re-entrant stack
          var transitionDepth = -1;
          var tDataStack = [];
          var restoreFnStack = [];

          // This function decorates the $injector, adding { $transition$: tData } to invoke() and instantiate() locals.
          // It returns a function that restores $injector to its previous state.
          function decorateInjector(tData) {
            var oldinvoke = $injector.invoke;
            var oldinstantiate = $injector.instantiate;
            $injector.invoke = function (fn, self, locals) {
              return oldinvoke(fn, self, angular.extend({$transition$: tData}, locals));
            };
            $injector.instantiate = function (fn, locals) {
              return oldinstantiate(fn, angular.extend({$transition$: tData}, locals));
            };

            return function restoreItems() {
              $injector.invoke = oldinvoke;
              $injector.instantiate = oldinstantiate;
            };
          }

          function popStack() {
            restoreFnStack.pop()();
            tDataStack.pop();
            transitionDepth--;
          }

          // This promise callback (for when the real transitionTo is successful) runs the restore function for the
          // current stack level, then broadcasts the $transitionSuccess event.
          function transitionSuccess(deferred, tSuccess) {
            return function successFn(data) {
              popStack();
              $rootScope.$broadcast("$transitionSuccess", tSuccess);
              return deferred.resolve(data);
            };
          }

          // This promise callback (for when the real transitionTo fails) runs the restore function for the
          // current stack level, then broadcasts the $transitionError event.
          function transitionFailure(deferred, tFail) {
            return function failureFn(error) {
              popStack();
              $rootScope.$broadcast("$transitionError", tFail, error);
              return deferred.reject(error);
            };
          }

          // Decorate $state.transitionTo.
          $state.transitionTo = function (to, toParams, options) {
            // Create a deferred/promise which can be used earlier than UI-Router's transition promise.
            var deferred = $q.defer();
            // Place the promise in a transition data, and place it on the stack to be used in $stateChangeStart
            var tData = tDataStack[++transitionDepth] = {
              promise: deferred.promise
            };
            // placeholder restoreFn in case transitionTo doesn't reach $stateChangeStart (state not found, etc)
            restoreFnStack[transitionDepth] = function() { };
            // Invoke the real $state.transitionTo
            var tPromise = $state_transitionTo.apply($state, arguments);

            // insert our promise callbacks into the chain.
            return tPromise.then(transitionSuccess(deferred, tData), transitionFailure(deferred, tData));
          };

          // This event is handled synchronously in transitionTo call stack
          $rootScope.$on("$stateChangeStart", function (evt, toState, toParams, fromState, fromParams) {
              var depth = transitionDepth;
              // To/From is now normalized by ui-router.  Add this information to the transition data object.
              var tData = angular.extend(tDataStack[depth], {
                to: { state: toState, params: toParams },
                from: { state: fromState, params: fromParams }
              });

              var restoreFn = decorateInjector(tData);
              restoreFnStack[depth] = restoreFn;
              $rootScope.$broadcast("$transitionStart", tData);
            }
          );

          return $state;
        }]);
    }
  ]
);

})(window, window.angular);
define("ui-router-extras", ["angular-ui-router"], function(){});

/**
 * oclazyload - Load modules on demand (lazy load) with angularJS
 * @version v0.5.1
 * @link https://github.com/ocombe/ocLazyLoad
 * @license MIT
 * @author Olivier Combe <olivier.combe@gmail.com>
 */
!function(){function e(e){var n=[];return angular.forEach(e.requires,function(e){-1===l.indexOf(e)&&n.push(e)}),n}function n(e){try{return angular.module(e)}catch(n){if(/No module/.test(n)||n.message.indexOf("$injector:nomod")>-1)return!1}}function r(e){try{return angular.module(e)}catch(n){throw(/No module/.test(n)||n.message.indexOf("$injector:nomod")>-1)&&(n.message='The module "'+e+'" that you are trying to load does not exist. '+n.message),n}}function a(e,n,r,a){if(n){var o,i,u,l;for(o=0,i=n.length;i>o;o++)if(u=n[o],angular.isArray(u)){if(null!==e){if(!e.hasOwnProperty(u[0]))throw new Error("unsupported provider "+u[0]);l=e[u[0]]}var s=t(u,r);if("invoke"!==u[1])s&&angular.isDefined(l)&&l[u[1]].apply(l,u[2]);else{var c=function(e){var n=f.indexOf(r+"-"+e);(-1===n||a)&&(-1===n&&f.push(r+"-"+e),angular.isDefined(l)&&l[u[1]].apply(l,u[2]))};if(angular.isFunction(u[2][0]))c(u[2][0]);else if(angular.isArray(u[2][0]))for(var d=0,g=u[2][0].length;g>d;d++)angular.isFunction(u[2][0][d])&&c(u[2][0][d])}}}}function o(e,n,r){if(n){var t,u,s,f=[];for(t=n.length-1;t>=0;t--)if(u=n[t],"string"!=typeof u&&(u=i(u)),u&&-1===c.indexOf(u)){var g=-1===l.indexOf(u);if(s=angular.module(u),g&&(l.push(u),o(e,s.requires,r)),s._runBlocks.length>0)for(d[u]=[];s._runBlocks.length>0;)d[u].push(s._runBlocks.shift());angular.isDefined(d[u])&&(g||r.rerun)&&(f=f.concat(d[u])),a(e,s._invokeQueue,u,r.reconfig),a(e,s._configBlocks,u,r.reconfig),h(g?"ocLazyLoad.moduleLoaded":"ocLazyLoad.moduleReloaded",u),n.pop(),c.push(u)}var p=e.getInstanceInjector();angular.forEach(f,function(e){p.invoke(e)})}}function t(e,n){var r=e[2][0],a=e[1],o=!1;angular.isUndefined(s[n])&&(s[n]={}),angular.isUndefined(s[n][a])&&(s[n][a]=[]);var t=function(e){o=!0,s[n][a].push(e),h("ocLazyLoad.componentLoaded",[n,a,e])};if(angular.isString(r)&&-1===s[n][a].indexOf(r))t(r);else{if(!angular.isObject(r))return!1;angular.forEach(r,function(e){angular.isString(e)&&-1===s[n][a].indexOf(e)&&t(e)})}return o}function i(e){var n=null;return angular.isString(e)?n=e:angular.isObject(e)&&e.hasOwnProperty("name")&&angular.isString(e.name)&&(n=e.name),n}function u(e){function n(e){return e&&t.push(e)}var r,o,t=[e],i=["ng:app","ng-app","x-ng-app","data-ng-app"],u=/\sng[:\-]app(:\s*([\w\d_]+);?)?\s/;angular.forEach(i,function(r){i[r]=!0,n(document.getElementById(r)),r=r.replace(":","\\:"),e[0].querySelectorAll&&(angular.forEach(e[0].querySelectorAll("."+r),n),angular.forEach(e[0].querySelectorAll("."+r+"\\:"),n),angular.forEach(e[0].querySelectorAll("["+r+"]"),n))}),angular.forEach(t,function(n){if(!r){var a=" "+e.className+" ",t=u.exec(a);t?(r=n,o=(t[2]||"").replace(/\s+/g,",")):angular.forEach(n.attributes,function(e){!r&&i[e.name]&&(r=n,o=e.value)})}}),r&&!function s(e){if(-1===l.indexOf(e)){l.push(e);var n=angular.module(e);a(null,n._invokeQueue,e),a(null,n._configBlocks,e),angular.forEach(n.requires,s)}}(o)}var l=["ng"],s={},f=[],c=[],d={},g=angular.module("oc.lazyLoad",["ng"]),h=angular.noop;g.provider("$ocLazyLoad",["$controllerProvider","$provide","$compileProvider","$filterProvider","$injector","$animateProvider",function(a,t,s,f,d,g){var p,m,v,y={},L={$controllerProvider:a,$compileProvider:s,$filterProvider:f,$provide:t,$injector:d,$animateProvider:g},w=document.getElementsByTagName("head")[0]||document.getElementsByTagName("body")[0],O=!1,E=!1;u(angular.element(window.document)),this.$get=["$log","$q","$templateCache","$http","$rootElement","$rootScope","$cacheFactory","$interval",function(a,t,u,s,f,d,g,j){var $,x=g("ocLazyLoad"),b=!1,D=!1;O||(a={},a.error=angular.noop,a.warn=angular.noop,a.info=angular.noop),L.getInstanceInjector=function(){return $?$:$=f.data("$injector")||angular.injector()},h=function(e,n){E&&d.$broadcast(e,n),O&&a.info(e,n)};var P=function(e,n,r){var a,o,i=t.defer(),u=function(e){var n=(new Date).getTime();return e.indexOf("?")>=0?"&"===e.substring(0,e.length-1)?e+"_dc="+n:e+"&_dc="+n:e+"?_dc="+n};switch(angular.isUndefined(x.get(n))&&x.put(n,i.promise),e){case"css":a=document.createElement("link"),a.type="text/css",a.rel="stylesheet",a.href=r.cache===!1?u(n):n;break;case"js":a=document.createElement("script"),a.src=r.cache===!1?u(n):n;break;default:i.reject(new Error('Requested type "'+e+'" is not known. Could not inject "'+n+'"'))}a.onload=a.onreadystatechange=function(){a.readyState&&!/^c|loade/.test(a.readyState)||o||(a.onload=a.onreadystatechange=null,o=1,h("ocLazyLoad.fileLoaded",n),i.resolve())},a.onerror=function(){i.reject(new Error("Unable to load "+n))},a.async=r.serie?0:1;var l=w.lastChild;if(r.insertBefore){var s=angular.element(r.insertBefore);s&&s.length>0&&(l=s[0])}if(w.insertBefore(a,l),"css"==e){if(!b){var f=navigator.userAgent.toLowerCase();if(/iP(hone|od|ad)/.test(navigator.platform)){var c=navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/),d=parseFloat([parseInt(c[1],10),parseInt(c[2],10),parseInt(c[3]||0,10)].join("."));D=6>d}else if(f.indexOf("android")>-1){var g=parseFloat(f.slice(f.indexOf("android")+8));D=4.4>g}else if(f.indexOf("safari")>-1&&-1==f.indexOf("chrome")){var p=parseFloat(f.match(/version\/([\.\d]+)/i)[1]);D=6>p}}if(D)var m=1e3,v=j(function(){try{a.sheet.cssRules,j.cancel(v),a.onload()}catch(e){--m<=0&&a.onerror()}},20)}return i.promise};angular.isUndefined(p)&&(p=function(e,n,r){var a=[];angular.forEach(e,function(e){a.push(P("js",e,r))}),t.all(a).then(function(){n()},function(e){n(e)})},p.ocLazyLoadLoader=!0),angular.isUndefined(m)&&(m=function(e,n,r){var a=[];angular.forEach(e,function(e){a.push(P("css",e,r))}),t.all(a).then(function(){n()},function(e){n(e)})},m.ocLazyLoadLoader=!0),angular.isUndefined(v)&&(v=function(e,n,r){var a=[];return angular.forEach(e,function(e){var n=t.defer();a.push(n.promise),s.get(e,r).success(function(r){angular.isString(r)&&r.length>0&&angular.forEach(angular.element(r),function(e){"SCRIPT"===e.nodeName&&"text/ng-template"===e.type&&u.put(e.id,e.innerHTML)}),angular.isUndefined(x.get(e))&&x.put(e,!0),n.resolve()}).error(function(r){n.reject(new Error('Unable to load template file "'+e+'": '+r))})}),t.all(a).then(function(){n()},function(e){n(e)})},v.ocLazyLoadLoader=!0);var z=function(e,n){var r=[],o=[],i=[],u=[],l=null;angular.extend(n||{},e);var s=function(e){l=x.get(e),angular.isUndefined(l)||n.cache===!1?/\.css[^\.]*$/.test(e)&&-1===r.indexOf(e)?r.push(e):/\.(htm|html)[^\.]*$/.test(e)&&-1===o.indexOf(e)?o.push(e):-1===i.indexOf(e)&&i.push(e):l&&u.push(l)};if(n.serie?s(n.files.shift()):angular.forEach(n.files,function(e){s(e)}),r.length>0){var f=t.defer();m(r,function(e){angular.isDefined(e)&&m.hasOwnProperty("ocLazyLoadLoader")?(a.error(e),f.reject(e)):f.resolve()},n),u.push(f.promise)}if(o.length>0){var c=t.defer();v(o,function(e){angular.isDefined(e)&&v.hasOwnProperty("ocLazyLoadLoader")?(a.error(e),c.reject(e)):c.resolve()},n),u.push(c.promise)}if(i.length>0){var d=t.defer();p(i,function(e){angular.isDefined(e)&&p.hasOwnProperty("ocLazyLoadLoader")?(a.error(e),d.reject(e)):d.resolve()},n),u.push(d.promise)}return n.serie&&n.files.length>0?t.all(u).then(function(){return z(e,n)}):t.all(u)};return{getModuleConfig:function(e){if(!angular.isString(e))throw new Error("You need to give the name of the module to get");return y[e]?y[e]:null},setModuleConfig:function(e){if(!angular.isObject(e))throw new Error("You need to give the module config object to set");return y[e.name]=e,e},getModules:function(){return l},isLoaded:function(e){var r=function(e){var r=l.indexOf(e)>-1;return r||(r=!!n(e)),r};if(angular.isString(e)&&(e=[e]),angular.isArray(e)){var a,o;for(a=0,o=e.length;o>a;a++)if(!r(e[a]))return!1;return!0}throw new Error("You need to define the module(s) name(s)")},load:function(u,s){var f,d,g=this,h=null,p=[],m=[],v=t.defer();if(angular.isUndefined(s)&&(s={}),angular.isArray(u))return angular.forEach(u,function(e){e&&m.push(g.load(e,s))}),t.all(m).then(function(){v.resolve(u)},function(e){v.reject(e)}),v.promise;if(f=i(u),"string"==typeof u?(h=g.getModuleConfig(u),h||(h={files:[u]},f=null)):"object"==typeof u&&(h=g.setModuleConfig(u)),null===h?(d='Module "'+f+'" is not configured, cannot load.',a.error(d),v.reject(new Error(d))):angular.isDefined(h.template)&&(angular.isUndefined(h.files)&&(h.files=[]),angular.isString(h.template)?h.files.push(h.template):angular.isArray(h.template)&&h.files.concat(h.template)),p.push=function(e){-1===this.indexOf(e)&&Array.prototype.push.apply(this,arguments)},angular.isDefined(f)&&n(f)&&-1!==l.indexOf(f)&&(p.push(f),angular.isUndefined(h.files)))return v.resolve(),v.promise;var y={};angular.extend(y,s,h);var w=function O(o){var u,l,s,f,c=[];if(u=i(o),null===u)return t.when();try{l=r(u)}catch(d){var h=t.defer();return a.error(d.message),h.reject(d),h.promise}return s=e(l),angular.forEach(s,function(e){if("string"==typeof e){var r=g.getModuleConfig(e);if(null===r)return void p.push(e);e=r}return n(e.name)?void("string"!=typeof o&&(f=e.files.filter(function(n){return g.getModuleConfig(e.name).files.indexOf(n)<0}),0!==f.length&&a.warn('Module "',u,'" attempted to redefine configuration for dependency. "',e.name,'"\n Additional Files Loaded:',f),c.push(z(e.files,y).then(function(){return O(e)})))):("object"==typeof e&&(e.hasOwnProperty("name")&&e.name&&(g.setModuleConfig(e),p.push(e.name)),e.hasOwnProperty("css")&&0!==e.css.length&&angular.forEach(e.css,function(e){P("css",e,y)})),void(e.hasOwnProperty("files")&&0!==e.files.length&&e.files&&c.push(z(e,y).then(function(){return O(e)}))))}),t.all(c)};return z(h,y).then(function(){null===f?v.resolve(u):(p.push(f),w(f).then(function(){try{c=[],o(L,p,y)}catch(e){return a.error(e.message),void v.reject(e)}v.resolve(u)},function(e){v.reject(e)}))},function(e){v.reject(e)}),v.promise}}}],this.config=function(e){if(angular.isDefined(e.jsLoader)||angular.isDefined(e.asyncLoader)){if(!angular.isFunction(e.jsLoader||e.asyncLoader))throw"The js loader needs to be a function";p=e.jsLoader||e.asyncLoader}if(angular.isDefined(e.cssLoader)){if(!angular.isFunction(e.cssLoader))throw"The css loader needs to be a function";m=e.cssLoader}if(angular.isDefined(e.templatesLoader)){if(!angular.isFunction(e.templatesLoader))throw"The template loader needs to be a function";v=e.templatesLoader}if(angular.isDefined(e.loadedModules)){var n=function(e){l.indexOf(e)<0&&(l.push(e),angular.forEach(angular.module(e).requires,n))};angular.forEach(e.loadedModules,n)}angular.isDefined(e.modules)&&(angular.isArray(e.modules)?angular.forEach(e.modules,function(e){y[e.name]=e}):y[e.modules.name]=e.modules),angular.isDefined(e.debug)&&(O=e.debug),angular.isDefined(e.events)&&(E=e.events)}}]),g.directive("ocLazyLoad",["$ocLazyLoad","$compile","$animate","$parse",function(e,n,r,a){return{restrict:"A",terminal:!0,priority:1e3,compile:function(o){var t=o[0].innerHTML;return o.html(""),function(o,i,u){var l=a(u.ocLazyLoad);o.$watch(function(){return l(o)||u.ocLazyLoad},function(a){angular.isDefined(a)&&e.load(a).then(function(){r.enter(n(t)(o),null,i)})},!0)}}}}]),Array.prototype.indexOf||(Array.prototype.indexOf=function(e,n){var r;if(null==this)throw new TypeError('"this" is null or not defined');var a=Object(this),o=a.length>>>0;if(0===o)return-1;var t=+n||0;if(1/0===Math.abs(t)&&(t=0),t>=o)return-1;for(r=Math.max(t>=0?t:o-Math.abs(t),0);o>r;){if(r in a&&a[r]===e)return r;r++}return-1})}();
define("oclazyload", ["angular"], function(){});

define('modules/lazy-states/module',["angular","angular-ui-router","ui-router-extras","oclazyload"],function(a){var b=a.module("app.lazy-states",["ui.router","ct.ui.router.extras"]);b.config(["$futureStateProvider","$ocLazyLoadProvider",function(a,b){b.config({jsLoader:requirejs,loadedModules:["app"],modules:[{name:"app.inbox",files:["components/inbox/module","components/inbox/models/InboxMessage"]},{name:"app.calendar",files:["components/calendar/module","components/calendar/directives/fullCalendar","components/calendar/directives/dragableEvent","components/calendar/controllers/CalendarCtrl"]}]});var c=function(a,b,c){var d=a.defer();return console.dir(c),b.load(c.module).then(function(){d.resolve()},function(){d.reject()}),d.promise};a.stateFactory("ocLazyLoad",c),a.futureState({stateName:"app.inbox.folder",url:"/inbox",type:"ocLazyLoad",module:"app.inbox"}),a.futureState({stateName:"app.calendar",url:"/calendar",type:"ocLazyLoad",module:"app.calendar"})}])});
define('smart-templates',["angular"],function(){angular.module("smart-templates",[]).run(["$templateCache",function(a){a.put("build/auth/directives/login-info.tpl.html",'<div class="login-info ng-cloak">\n    <span> <!-- User image size is adjusted inside CSS, it should stay as it -->\n        <a  href="" toggle-shortcut>\n            <img ng-src="{{user.picture}}" alt="me" class="online">\n                <span>{{user.username}}\n                </span>\n            <i class="fa fa-angle-down"></i>\n        </a>\n     </span>\n</div>'),a.put("build/components/calendar/directives/full-calendar.tpl.html",'<div jarvis-widget data-widget-color="blueDark">\n    <header>\n        <span class="widget-icon"> <i class="fa fa-calendar"></i> </span>\n\n        <h2> My Events </h2>\n\n        <div class="widget-toolbar">\n            <!-- add: non-hidden - to disable auto hide -->\n            <div class="btn-group" dropdown >\n                <button class="btn dropdown-toggle btn-xs btn-default">\n                    Showing <i class="fa fa-caret-down"></i>\n                </button>\n                <ul class="dropdown-menu js-status-update pull-right">\n                    <li>\n                        <a ng-click="changeView(\'month\')">Month</a>\n                    </li>\n                    <li>\n                        <a ng-click="changeView(\'agendaWeek\')">Agenda</a>\n                    </li>\n                    <li>\n                        <a ng-click="changeView(\'agendaDay\')">Today</a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </header>\n\n    <!-- widget div-->\n    <div>\n        <div class="widget-body no-padding">\n            <!-- content goes here -->\n            <div class="widget-body-toolbar">\n\n                <div id="calendar-buttons">\n\n                    <div class="btn-group">\n                        <a ng-click="prev()" class="btn btn-default btn-xs"><i\n                                class="fa fa-chevron-left"></i></a>\n                        <a ng-click="next()" class="btn btn-default btn-xs"><i\n                                class="fa fa-chevron-right"></i></a>\n                    </div>\n                </div>\n            </div>\n            <div id="calendar"></div>\n\n            <!-- end content -->\n        </div>\n\n    </div>\n    <!-- end widget div -->\n</div>\n'),a.put("build/components/calendar/views/calendar.tpl.html",'<!-- MAIN CONTENT -->\n<div id="content">\n\n    <div class="row">\n        <big-breadcrumbs items="[\'Home\', \'Calendar\']" class="col-xs-12 col-sm-7 col-md-7 col-lg-4"></big-breadcrumbs>\n        <div smart-include="build/layout/partials/sub-header.tpl.html"></div>\n    </div>\n    <!-- widget grid -->\n    <section id="widget-grid" widget-grid>\n        <!-- row -->\n        <div class="row" ng-controller="CalendarCtrl" >\n\n\n            <div class="col-sm-12 col-md-12 col-lg-3">\n                <!-- new widget -->\n                <div class="jarviswidget jarviswidget-color-blueDark">\n                    <header>\n                        <h2> Add Events </h2>\n                    </header>\n\n                    <!-- widget div-->\n                    <div>\n\n                        <div class="widget-body">\n                            <!-- content goes here -->\n\n                            <form id="add-event-form">\n                                <fieldset>\n\n                                    <div class="form-group">\n                                        <label>Select Event Icon</label>\n                                        <div class="btn-group btn-group-sm btn-group-justified" data-toggle="buttons" > <!--  -->\n                                            <label class="btn btn-default active">\n                                                <input type="radio" name="iconselect" id="icon-1" value="fa-info" radio-toggle ng-model="newEvent.icon">\n                                                <i class="fa fa-info text-muted"></i> </label>\n                                            <label class="btn btn-default">\n                                                <input type="radio" name="iconselect" id="icon-2" value="fa-warning" radio-toggle  ng-model="newEvent.icon">\n                                                <i class="fa fa-warning text-muted"></i> </label>\n                                            <label class="btn btn-default">\n                                                <input type="radio" name="iconselect" id="icon-3" value="fa-check" radio-toggle  ng-model="newEvent.icon">\n                                                <i class="fa fa-check text-muted"></i> </label>\n                                            <label class="btn btn-default">\n                                                <input type="radio" name="iconselect" id="icon-4" value="fa-user" radio-toggle  ng-model="newEvent.icon">\n                                                <i class="fa fa-user text-muted"></i> </label>\n                                            <label class="btn btn-default">\n                                                <input type="radio" name="iconselect" id="icon-5" value="fa-lock" radio-toggle  ng-model="newEvent.icon">\n                                                <i class="fa fa-lock text-muted"></i> </label>\n                                            <label class="btn btn-default">\n                                                <input type="radio" name="iconselect" id="icon-6" value="fa-clock-o" radio-toggle  ng-model="newEvent.icon">\n                                                <i class="fa fa-clock-o text-muted"></i> </label>\n                                        </div>\n                                    </div>\n\n                                    <div class="form-group">\n                                        <label>Event Title</label>\n                                        <input ng-model="newEvent.title" class="form-control"  id="title" name="title" maxlength="40" type="text" placeholder="Event Title">\n                                    </div>\n                                    <div class="form-group">\n                                        <label>Event Description</label>\n                                        <textarea  ng-model="newEvent.description" class="form-control" placeholder="Please be brief" rows="3" maxlength="40" id="description"></textarea>\n                                        <p class="note">Maxlength is set to 40 characters</p>\n                                    </div>\n\n                                    <div class="form-group">\n                                        <label>Select Event Color</label>\n                                        <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons" >\n                                            <label class="btn bg-color-darken active">\n                                                <input   ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" >\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                            <label class="btn bg-color-blue">\n                                                <input  ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white">\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                            <label class="btn bg-color-orange">\n                                                <input  ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white">\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                            <label class="btn bg-color-greenLight">\n                                                <input  ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white">\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                            <label class="btn bg-color-blueLight">\n                                                <input  ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white">\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                            <label class="btn bg-color-red">\n                                                <input  ng-model="newEvent.className" radio-toggle   type="radio" name="priority" id="option6" value="bg-color-red txt-color-white">\n                                                <i class="fa fa-check txt-color-white"></i> </label>\n                                        </div>\n                                    </div>\n\n                                </fieldset>\n                                <div class="form-actions">\n                                    <div class="row">\n                                        <div class="col-md-12">\n                                            <button class="btn btn-default" type="button" id="add-event" ng-click="addEvent()" >\n                                                Add Event\n                                            </button>\n                                        </div>\n                                    </div>\n                                </div>\n                            </form>\n\n                            <!-- end content -->\n                        </div>\n\n                    </div>\n                    <!-- end widget div -->\n                </div>\n                <!-- end widget -->\n\n                <div class="well well-sm" id="event-container">\n                    <form>\n                        <legend>\n                            Draggable Events\n                        </legend>\n                        <ul id=\'external-events\' class="list-unstyled">\n\n                            <li ng-repeat="event in eventsExternal" dragable-event>\n                                <span class="{{event.className}}" \n                                    data-description="{{event.description}}"\n                                    data-icon="{{event.icon}}"\n                                >\n                                {{event.title}}</span>\n                            </li>\n                            \n                        </ul>\n\n                        <!-- <ul id=\'external-events\' class="list-unstyled">\n                            <li>\n                                <span class="bg-color-darken txt-color-white" data-description="Currently busy" data-icon="fa-time">Office Meeting</span>\n                            </li>\n                            <li>\n                                <span class="bg-color-blue txt-color-white" data-description="No Description" data-icon="fa-pie">Lunch Break</span>\n                            </li>\n                            <li>\n                                <span class="bg-color-red txt-color-white" data-description="Urgent Tasks" data-icon="fa-alert">URGENT</span>\n                            </li>\n                        </ul> -->\n\n                        <div class="checkbox">\n                            <label>\n                                <input type="checkbox" id="drop-remove" class="checkbox style-0" checked="checked">\n                                <span>remove after drop</span> </label>\n\n                        </div>\n                    </form>\n\n                </div>\n            </div>\n\n\n            <article class="col-sm-12 col-md-12 col-lg-9">\n                <full-calendar id="main-calendar-widget" data-events="events"></full-calendar>\n            </article>\n        </div>\n    </section>\n</div>'),a.put("build/components/chat/directives/aside-chat-widget.tpl.html",'<ul>\n    <li>\n        <div class="display-users">\n            <input class="form-control chat-user-filter" placeholder="Filter" type="text">\n            <dl>\n                <dt>\n                    <a href="#" class="usr"\n                       data-chat-id="cha1"\n                       data-chat-fname="Sadi"\n                       data-chat-lname="Orlaf"\n                       data-chat-status="busy"\n                       data-chat-alertmsg="Sadi Orlaf is in a meeting. Please do not disturb!"\n                       data-chat-alertshow="true"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/5.png\' alt=\'Sadi Orlaf\'>\n											<div class=\'usr-card-content\'>\n												<h3>Sadi Orlaf</h3>\n												<p>Marketing Executive</p>\n											</div>\n										</div>\n									">\n                        <i></i>Sadi Orlaf\n                    </a>\n                </dt>\n                <dt>\n                    <a href="#" class="usr"\n                       data-chat-id="cha2"\n                       data-chat-fname="Jessica"\n                       data-chat-lname="Dolof"\n                       data-chat-status="online"\n                       data-chat-alertmsg=""\n                       data-chat-alertshow="false"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/1.png\' alt=\'Jessica Dolof\'>\n											<div class=\'usr-card-content\'>\n												<h3>Jessica Dolof</h3>\n												<p>Sales Administrator</p>\n											</div>\n										</div>\n									">\n                        <i></i>Jessica Dolof\n                    </a>\n                </dt>\n                <dt>\n                    <a href="#" class="usr"\n                       data-chat-id="cha3"\n                       data-chat-fname="Zekarburg"\n                       data-chat-lname="Almandalie"\n                       data-chat-status="online"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/3.png\' alt=\'Zekarburg Almandalie\'>\n											<div class=\'usr-card-content\'>\n												<h3>Zekarburg Almandalie</h3>\n												<p>Sales Admin</p>\n											</div>\n										</div>\n									">\n                        <i></i>Zekarburg Almandalie\n                    </a>\n                </dt>\n                <dt>\n                    <a href="#" class="usr"\n                       data-chat-id="cha4"\n                       data-chat-fname="Barley"\n                       data-chat-lname="Krazurkth"\n                       data-chat-status="away"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/4.png\' alt=\'Barley Krazurkth\'>\n											<div class=\'usr-card-content\'>\n												<h3>Barley Krazurkth</h3>\n												<p>Sales Director</p>\n											</div>\n										</div>\n									">\n                        <i></i>Barley Krazurkth\n                    </a>\n                </dt>\n                <dt>\n                    <a href="#" class="usr offline"\n                       data-chat-id="cha5"\n                       data-chat-fname="Farhana"\n                       data-chat-lname="Amrin"\n                       data-chat-status="incognito"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/female.png\' alt=\'Farhana Amrin\'>\n											<div class=\'usr-card-content\'>\n												<h3>Farhana Amrin</h3>\n												<p>Support Admin <small><i class=\'fa fa-music\'></i> Playing Beethoven Classics</small></p>\n											</div>\n										</div>\n									">\n                        <i></i>Farhana Amrin (offline)\n                    </a>\n                </dt>\n                <dt>\n                    <a href="#" class="usr offline"\n                       data-chat-id="cha6"\n                       data-chat-fname="Lezley"\n                       data-chat-lname="Jacob"\n                       data-chat-status="incognito"\n                       popover-trigger="mouseenter"\n                       popover-placement="right"\n                       popover="\n										<div class=\'usr-card\'>\n											<img src=\'styles/img/avatars/male.png\' alt=\'Lezley Jacob\'>\n											<div class=\'usr-card-content\'>\n												<h3>Lezley Jacob</h3>\n												<p>Sales Director</p>\n											</div>\n										</div>\n									">\n                        <i></i>Lezley Jacob (offline)\n                    </a>\n                </dt>\n            </dl>\n\n\n            <!--<a href="chat.html" class="btn btn-xs btn-default btn-block sa-chat-learnmore-btn">About the API</a>-->\n        </div>\n    </li>\n</ul>'),a.put("build/components/chat/directives/chat-users.tpl.html",'<div id="chat-container" ng-class="{open: open}">\n    <span class="chat-list-open-close" ng-click="openToggle()"><i class="fa fa-user"></i><b>!</b></span>\n\n    <div class="chat-list-body custom-scroll">\n        <ul id="chat-users">\n            <li ng-repeat="chatUser in chatUsers | filter: chatUserFilter">\n                <a ng-click="messageTo(chatUser)"><img ng-src="{{chatUser.picture}}">{{chatUser.username}} <span\n                        class="badge badge-inverse">{{chatUser.username.length}}</span><span class="state"><i\n                        class="fa fa-circle txt-color-green pull-right"></i></span></a>\n            </li>\n        </ul>\n    </div>\n    <div class="chat-list-footer">\n        <div class="control-group">\n            <form class="smart-form">\n                <section>\n                    <label class="input" >\n                        <input type="text" ng-model="chatUserFilter" id="filter-chat-list" placeholder="Filter">\n                    </label>\n                </section>\n            </form>\n        </div>\n    </div>\n</div>'),a.put("build/components/chat/directives/chat-widget.tpl.html",'<div id="chat-widget" jarvis-widget data-widget-color="blueDark" data-widget-editbutton="false"\n     data-widget-fullscreenbutton="false">\n\n\n    <header>\n        <span class="widget-icon"> <i class="fa fa-comments txt-color-white"></i> </span>\n\n        <h2> SmartMessage </h2>\n\n        <div class="widget-toolbar">\n            <!-- add: non-hidden - to disable auto hide -->\n\n            <div class="btn-group" data-dropdown>\n                <button class="btn dropdown-toggle btn-xs btn-success">\n                    Status <i class="fa fa-caret-down"></i>\n                </button>\n                <ul class="dropdown-menu pull-right js-status-update">\n                    <li>\n                        <a href-void><i class="fa fa-circle txt-color-green"></i> Online</a>\n                    </li>\n                    <li>\n                        <a href-void><i class="fa fa-circle txt-color-red"></i> Busy</a>\n                    </li>\n                    <li>\n                        <a href-void><i class="fa fa-circle txt-color-orange"></i> Away</a>\n                    </li>\n                    <li class="divider"></li>\n                    <li>\n                        <a href-void><i class="fa fa-power-off"></i> Log Off</a>\n                    </li>\n                </ul>\n            </div>\n        </div>\n    </header>\n\n    <!-- widget div-->\n    <div>\n        <div class="widget-body widget-hide-overflow no-padding">\n            <!-- content goes here -->\n\n            <chat-users></chat-users>\n\n            <!-- CHAT BODY -->\n            <div id="chat-body" class="chat-body custom-scroll">\n                <ul>\n                    <li class="message" ng-repeat="message in chatMessages">\n                        <img class="message-picture online" ng-src="{{message.user.picture}}">\n\n                        <div class="message-text">\n                            <time>\n                                {{message.date | date }}\n                            </time>\n                            <a ng-click="messageTo(message.user)" class="username">{{message.user.username}}</a>\n                            <div ng-bind-html="message.body"></div>\n\n                        </div>\n                    </li>\n                </ul>\n            </div>\n\n            <!-- CHAT FOOTER -->\n            <div class="chat-footer">\n\n                <!-- CHAT TEXTAREA -->\n                <div class="textarea-div">\n\n                    <div class="typearea">\n                        <textarea placeholder="Write a reply..." id="textarea-expand"\n                                  class="custom-scroll" ng-model="newMessage"></textarea>\n                    </div>\n\n                </div>\n\n                <!-- CHAT REPLY/SEND -->\n											<span class="textarea-controls">\n												<button class="btn btn-sm btn-primary pull-right" ng-click="sendMessage()">\n                                                    Reply\n                                                </button> <span class="pull-right smart-form"\n                                                                style="margin-top: 3px; margin-right: 10px;"> <label\n                                                    class="checkbox pull-right">\n                                                <input type="checkbox" name="subscription" id="subscription">\n                                                <i></i>Press <strong> ENTER </strong> to send </label> </span> <a\n                                                    href-void class="pull-left"><i\n                                                    class="fa fa-camera fa-fw fa-lg"></i></a> </span>\n\n            </div>\n\n            <!-- end content -->\n        </div>\n\n    </div>\n    <!-- end widget div -->\n</div>'),a.put("build/components/language/language-selector.tpl.html",'<ul class="header-dropdown-list hidden-xs ng-cloak" ng-controller="LanguagesCtrl">\n    <li class="dropdown">\n        <a class="dropdown-toggle" href> <img src="styles/img/blank.gif" class="flag flag-{{currentLanguage.key}}" alt="{{currentLanguage.alt}}"> <span> {{currentLanguage.title}} </span>\n            <i class="fa fa-angle-down"></i> </a>\n        <ul class="dropdown-menu pull-right">\n            <li ng-class="{active: language==currentLanguage}" ng-repeat="language in languages">\n                <a ng-click="selectLanguage(language)" ><img src="styles/img/blank.gif" class="flag flag-{{language.key}}"\n                                                   alt="{{language.alt}}"> {{language.title}}</a>\n            </li>\n        </ul>\n    </li>\n</ul>'),a.put("build/components/projects/recent-projects.tpl.html",'<div class="project-context hidden-xs dropdown">\n\n    <span class="label">{{getWord(\'Projects\')}}:</span>\n    <span class="project-selector dropdown-toggle">{{getWord(\'Recent projects\')}} <i ng-if="projects.length"\n            class="fa fa-angle-down"></i></span>\n\n    <ul class="dropdown-menu" ng-if="projects.length">\n        <li ng-repeat="project in projects">\n            <a href="{{project.href}}">{{project.title}}</a>\n        </li>\n        <li class="divider"></li>\n        <li>\n            <a ng-click="clearProjects()"><i class="fa fa-power-off"></i> Clear</a>\n        </li>\n    </ul>\n\n</div>'),a.put("build/components/shortcut/shortcut.tpl.html",'<div id="shortcut">\n	<ul>\n		<li>\n			<a href="#/inbox/" class="jarvismetro-tile big-cubes bg-color-blue"> <span class="iconbox"> <i class="fa fa-envelope fa-4x"></i> <span>Mail <span class="label pull-right bg-color-darken">14</span></span> </span> </a>\n		</li>\n		<li>\n			<a href="#/calendar" class="jarvismetro-tile big-cubes bg-color-orangeDark"> <span class="iconbox"> <i class="fa fa-calendar fa-4x"></i> <span>Calendar</span> </span> </a>\n		</li>\n		<li>\n			<a href="#/maps" class="jarvismetro-tile big-cubes bg-color-purple"> <span class="iconbox"> <i class="fa fa-map-marker fa-4x"></i> <span>Maps</span> </span> </a>\n		</li>\n		<li>\n			<a href="#/invoice" class="jarvismetro-tile big-cubes bg-color-blueDark"> <span class="iconbox"> <i class="fa fa-book fa-4x"></i> <span>Invoice <span class="label pull-right bg-color-darken">99</span></span> </span> </a>\n		</li>\n		<li>\n			<a href="#/gallery" class="jarvismetro-tile big-cubes bg-color-greenLight"> <span class="iconbox"> <i class="fa fa-picture-o fa-4x"></i> <span>Gallery </span> </span> </a>\n		</li>\n		<li>\n			<a href="#/profile" class="jarvismetro-tile big-cubes selected bg-color-pinkDark"> <span class="iconbox"> <i class="fa fa-user fa-4x"></i> <span>My Profile </span> </span> </a>\n		</li>\n	</ul>\n</div>'),a.put("build/components/todo/directives/todo-list.tpl.html",'<div>\n    <h5 class="todo-group-title"><i class="fa fa-{{icon}}"></i> {{title}} (\n        <small class="num-of-tasks">{{scopeItems.length}}</small>\n        )\n    </h5>\n    <ul class="todo">\n        <li ng-class="{complete: todo.completedAt}" ng-repeat="todo in todos | filter: filter track by todo._id" >\n    	<span class="handle"> <label class="checkbox">\n            <input type="checkbox" ng-click="toggleCompleted(todo)" ng-checked="todo.completedAt"\n                   name="checkbox-inline">\n            <i></i> </label> </span>\n\n            <p>\n                <strong>Ticket #{{$index + 1}}</strong> - {{todo.title}}\n                <span class="text-muted" ng-if="todo.description">{{todo.description}}</span>\n                <span class="date">{{todo.createdAt | date}} &dash; <a ng-click="deleteTodo(todo)" class="text-muted"><i\n                        class="fa fa-trash"></i></a></span>\n\n            </p>\n        </li>\n    </ul>\n</div>'),a.put("build/components/todo/todo-widget.tpl.html",'<div id="todo-widget" jarvis-widget data-widget-editbutton="false" data-widget-color="blue"\n     ng-controller="TodoCtrl">\n    <header>\n        <span class="widget-icon"> <i class="fa fa-check txt-color-white"></i> </span>\n\n        <h2> ToDo\'s </h2>\n\n        <div class="widget-toolbar">\n            <!-- add: non-hidden - to disable auto hide -->\n            <button class="btn btn-xs btn-default" ng-class="{active: newTodo}" ng-click="toggleAdd()"><i ng-class="{ \'fa fa-plus\': !newTodo, \'fa fa-times\': newTodo}"></i> Add</button>\n\n        </div>\n    </header>\n    <!-- widget div-->\n    <div>\n        <div class="widget-body no-padding smart-form">\n            <!-- content goes here -->\n            <div ng-show="newTodo">\n                <h5 class="todo-group-title"><i class="fa fa-plus-circle"></i> New Todo</h5>\n\n                <form name="newTodoForm" class="smart-form">\n                    <fieldset>\n                        <section>\n                            <label class="input">\n                                <input type="text" required class="input-lg" ng-model="newTodo.title"\n                                       placeholder="What needs to be done?">\n                            </label>\n                        </section>\n                        <section>\n                            <div class="col-xs-6">\n                                <label class="select">\n                                    <select class="input-sm" ng-model="newTodo.state"\n                                            ng-options="state as state for state in states"></select> <i></i> </label>\n                            </div>\n                        </section>\n                    </fieldset>\n                    <footer>\n                        <button ng-disabled="newTodoForm.$invalid" type="button" class="btn btn-primary"\n                                ng-click="createTodo()">\n                            Add\n                        </button>\n                        <button type="button" class="btn btn-default" ng-click="toggleAdd()">\n                            Cancel\n                        </button>\n                    </footer>\n                </form>\n            </div>\n\n            <todo-list state="Critical"  title="Critical Tasks" icon="warning"></todo-list>\n\n            <todo-list state="Important" title="Important Tasks" icon="exclamation"></todo-list>\n\n            <todo-list state="Completed" title="Completed Tasks" icon="check"></todo-list>\n\n            <!-- end content -->\n        </div>\n\n    </div>\n    <!-- end widget div -->\n</div>'),a.put("build/dashboard/live-feeds.tpl.html",'<div jarvis-widget id="live-feeds-widget" data-widget-togglebutton="false" data-widget-editbutton="false"\n     data-widget-fullscreenbutton="false" data-widget-colorbutton="false" data-widget-deletebutton="false">\n<!-- widget options:\nusage: <div class="jarviswidget" id="wid-id-0" data-widget-editbutton="false">\n\ndata-widget-colorbutton="false"\ndata-widget-editbutton="false"\ndata-widget-togglebutton="false"\ndata-widget-deletebutton="false"\ndata-widget-fullscreenbutton="false"\ndata-widget-custombutton="false"\ndata-widget-collapsed="true"\ndata-widget-sortable="false"\n\n-->\n<header>\n    <span class="widget-icon"> <i class="glyphicon glyphicon-stats txt-color-darken"></i> </span>\n\n    <h2>Live Feeds </h2>\n\n    <ul class="nav nav-tabs pull-right in" id="myTab">\n        <li class="active">\n            <a data-toggle="tab" href="#s1"><i class="fa fa-clock-o"></i> <span class="hidden-mobile hidden-tablet">Live Stats</span></a>\n        </li>\n\n        <li>\n            <a data-toggle="tab" href="#s2"><i class="fa fa-facebook"></i> <span class="hidden-mobile hidden-tablet">Social Network</span></a>\n        </li>\n\n        <li>\n            <a data-toggle="tab" href="#s3"><i class="fa fa-dollar"></i> <span class="hidden-mobile hidden-tablet">Revenue</span></a>\n        </li>\n    </ul>\n\n</header>\n\n<!-- widget div-->\n<div class="no-padding">\n\n    <div class="widget-body">\n        <!-- content -->\n        <div id="myTabContent" class="tab-content">\n            <div class="tab-pane fade active in padding-10 no-padding-bottom" id="s1">\n                <div class="row no-space">\n                    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">\n														<span class="demo-liveupdate-1"> <span\n                                                                class="onoffswitch-title">Live switch</span> <span\n                                                                class="onoffswitch">\n																<input type="checkbox" name="start_interval" ng-model="autoUpdate"\n                                                                       class="onoffswitch-checkbox" id="start_interval">\n																<label class="onoffswitch-label" for="start_interval">\n                                                                    <span class="onoffswitch-inner"\n                                                                          data-swchon-text="ON"\n                                                                          data-swchoff-text="OFF"></span>\n                                                                    <span class="onoffswitch-switch"></span>\n                                                                </label> </span> </span>\n\n                        <div id="updating-chart" class="chart-large txt-color-blue" flot-basic flot-data="liveStats" flot-options="liveStatsOptions"></div>\n\n                    </div>\n                    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 show-stats">\n\n                        <div class="row">\n                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"><span class="text"> My Tasks <span\n                                    class="pull-right">130/200</span> </span>\n\n                                <div class="progress">\n                                    <div class="progress-bar bg-color-blueDark" style="width: 65%;"></div>\n                                </div>\n                            </div>\n                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"><span class="text"> Transfered <span\n                                    class="pull-right">440 GB</span> </span>\n\n                                <div class="progress">\n                                    <div class="progress-bar bg-color-blue" style="width: 34%;"></div>\n                                </div>\n                            </div>\n                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"><span class="text"> Bugs Squashed<span\n                                    class="pull-right">77%</span> </span>\n\n                                <div class="progress">\n                                    <div class="progress-bar bg-color-blue" style="width: 77%;"></div>\n                                </div>\n                            </div>\n                            <div class="col-xs-6 col-sm-6 col-md-12 col-lg-12"><span class="text"> User Testing <span\n                                    class="pull-right">7 Days</span> </span>\n\n                                <div class="progress">\n                                    <div class="progress-bar bg-color-greenLight" style="width: 84%;"></div>\n                                </div>\n                            </div>\n\n                            <span class="show-stat-buttons"> <span class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a\n                                    href-void class="btn btn-default btn-block hidden-xs">Generate PDF</a> </span> <span\n                                    class="col-xs-12 col-sm-6 col-md-6 col-lg-6"> <a href-void\n                                                                                     class="btn btn-default btn-block hidden-xs">Report\n                                a bug</a> </span> </span>\n\n                        </div>\n\n                    </div>\n                </div>\n\n                <div class="show-stat-microcharts" data-sparkline-container data-easy-pie-chart-container>\n                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">\n\n                        <div class="easy-pie-chart txt-color-orangeDark" data-percent="33" data-pie-size="50">\n                            <span class="percent percent-sign">35</span>\n                        </div>\n                        <span class="easy-pie-title"> Server Load <i class="fa fa-caret-up icon-color-bad"></i> </span>\n                        <ul class="smaller-stat hidden-sm pull-right">\n                            <li>\n                                <span class="label bg-color-greenLight"><i class="fa fa-caret-up"></i> 97%</span>\n                            </li>\n                            <li>\n                                <span class="label bg-color-blueLight"><i class="fa fa-caret-down"></i> 44%</span>\n                            </li>\n                        </ul>\n                        <div class="sparkline txt-color-greenLight hidden-sm hidden-md pull-right"\n                             data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px"\n                             data-fill-color="transparent">\n                            130, 187, 250, 257, 200, 210, 300, 270, 363, 247, 270, 363, 247\n                        </div>\n                    </div>\n                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">\n                        <div class="easy-pie-chart txt-color-greenLight" data-percent="78.9" data-pie-size="50">\n                            <span class="percent percent-sign">78.9 </span>\n                        </div>\n                        <span class="easy-pie-title"> Disk Space <i class="fa fa-caret-down icon-color-good"></i></span>\n                        <ul class="smaller-stat hidden-sm pull-right">\n                            <li>\n                                <span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 76%</span>\n                            </li>\n                            <li>\n                                <span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 3%</span>\n                            </li>\n                        </ul>\n                        <div class="sparkline txt-color-blue hidden-sm hidden-md pull-right" data-sparkline-type="line"\n                             data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">\n                            257, 200, 210, 300, 270, 363, 130, 187, 250, 247, 270, 363, 247\n                        </div>\n                    </div>\n                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">\n                        <div class="easy-pie-chart txt-color-blue" data-percent="23" data-pie-size="50">\n                            <span class="percent percent-sign">23 </span>\n                        </div>\n                        <span class="easy-pie-title"> Transfered <i class="fa fa-caret-up icon-color-good"></i></span>\n                        <ul class="smaller-stat hidden-sm pull-right">\n                            <li>\n                                <span class="label bg-color-darken">10GB</span>\n                            </li>\n                            <li>\n                                <span class="label bg-color-blueDark"><i class="fa fa-caret-up"></i> 10%</span>\n                            </li>\n                        </ul>\n                        <div class="sparkline txt-color-darken hidden-sm hidden-md pull-right"\n                             data-sparkline-type="line" data-sparkline-height="33px" data-sparkline-width="70px"\n                             data-fill-color="transparent">\n                            200, 210, 363, 247, 300, 270, 130, 187, 250, 257, 363, 247, 270\n                        </div>\n                    </div>\n                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">\n                        <div class="easy-pie-chart txt-color-darken" data-percent="36" data-pie-size="50">\n                            <span class="percent degree-sign">36 <i class="fa fa-caret-up"></i></span>\n                        </div>\n                        <span class="easy-pie-title"> Temperature <i\n                                class="fa fa-caret-down icon-color-good"></i></span>\n                        <ul class="smaller-stat hidden-sm pull-right">\n                            <li>\n                                <span class="label bg-color-red"><i class="fa fa-caret-up"></i> 124</span>\n                            </li>\n                            <li>\n                                <span class="label bg-color-blue"><i class="fa fa-caret-down"></i> 40 F</span>\n                            </li>\n                        </ul>\n                        <div class="sparkline txt-color-red hidden-sm hidden-md pull-right" data-sparkline-type="line"\n                             data-sparkline-height="33px" data-sparkline-width="70px" data-fill-color="transparent">\n                            2700, 3631, 2471, 2700, 3631, 2471, 1300, 1877, 2500, 2577, 2000, 2100, 3000\n                        </div>\n                    </div>\n                </div>\n\n            </div>\n            <!-- end s1 tab pane -->\n\n            <div class="tab-pane fade" id="s2">\n                <div class="widget-body-toolbar bg-color-white">\n\n                    <form class="form-inline" role="form">\n\n                        <div class="form-group">\n                            <label class="sr-only" for="s123">Show From</label>\n                            <input type="email" class="form-control input-sm" id="s123" placeholder="Show From">\n                        </div>\n                        <div class="form-group">\n                            <input type="email" class="form-control input-sm" id="s124" placeholder="To">\n                        </div>\n\n                        <div class="btn-group hidden-phone pull-right">\n                            <a class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i\n                                    class="fa fa-cog"></i> More <span class="caret"> </span> </a>\n                            <ul class="dropdown-menu pull-right">\n                                <li>\n                                    <a href-void><i class="fa fa-file-text-alt"></i> Export to PDF</a>\n                                </li>\n                                <li>\n                                    <a href-void><i class="fa fa-question-sign"></i> Help</a>\n                                </li>\n                            </ul>\n                        </div>\n\n                    </form>\n\n                </div>\n                <div class="padding-10">\n                    <div id="statsChart" class="chart-large has-legend-unique" flot-basic flot-data="statsData" flot-options="statsDisplayOptions"></div>\n                </div>\n\n            </div>\n            <!-- end s2 tab pane -->\n\n            <div class="tab-pane fade" id="s3">\n\n                <div class="widget-body-toolbar bg-color-white smart-form" id="rev-toggles">\n\n                    <div class="inline-group">\n\n                        <label for="gra-0" class="checkbox">\n                            <input type="checkbox" id="gra-0" ng-model="targetsShow">\n                            <i></i> Target </label>\n                        <label for="gra-1" class="checkbox">\n                            <input type="checkbox" id="gra-1" ng-model="actualsShow">\n                            <i></i> Actual </label>\n                        <label for="gra-2" class="checkbox">\n                            <input type="checkbox" id="gra-2" ng-model="signupsShow">\n                            <i></i> Signups </label>\n                    </div>\n\n                    <div class="btn-group hidden-phone pull-right">\n                        <a class="btn dropdown-toggle btn-xs btn-default" data-toggle="dropdown"><i\n                                class="fa fa-cog"></i> More <span class="caret"> </span> </a>\n                        <ul class="dropdown-menu pull-right">\n                            <li>\n                                <a href-void><i class="fa fa-file-text-alt"></i> Export to PDF</a>\n                            </li>\n                            <li>\n                                <a href-void><i class="fa fa-question-sign"></i> Help</a>\n                            </li>\n                        </ul>\n                    </div>\n\n                </div>\n\n                <div class="padding-10">\n                    <div id="flotcontainer" class="chart-large has-legend-unique" flot-basic flot-data="revenewData" flot-options="revenewDisplayOptions"></div>\n                </div>\n            </div>\n            <!-- end s3 tab pane -->\n        </div>\n\n        <!-- end content -->\n    </div>\n\n</div>\n<!-- end widget div -->\n</div>\n'),a.put("build/layout/directives/demo/demo-states.tpl.html",'<div class="demo"><span id="demo-setting"><i class="fa fa-cog txt-color-blueDark"></i></span>\n\n    <form>\n        <legend class="no-padding margin-bottom-10">Layout Options</legend>\n        <section>\n            <label><input type="checkbox" ng-model="fixedHeader"\n                          class="checkbox style-0"><span>Fixed Header</span></label>\n            <label><input type="checkbox"\n                          ng-model="fixedNavigation"\n                          class="checkbox style-0"><span>Fixed Navigation</span></label>\n            <label><input type="checkbox"\n                          ng-model="fixedRibbon"\n                          class="checkbox style-0"><span>Fixed Ribbon</span></label>\n            <label><input type="checkbox"\n                          ng-model="fixedPageFooter"\n                          class="checkbox style-0"><span>Fixed Footer</span></label>\n            <label><input type="checkbox"\n                          ng-model="insideContainer"\n                          class="checkbox style-0"><span>Inside <b>.container</b></span></label>\n            <label><input type="checkbox"\n                          ng-model="rtl"\n                          class="checkbox style-0"><span>RTL</span></label>\n            <label><input type="checkbox"\n                          ng-model="menuOnTop"\n                          class="checkbox style-0"><span>Menu on <b>top</b></span></label>\n            <label><input type="checkbox"\n                          ng-model="colorblindFriendly"\n                          class="checkbox style-0"><span>For Colorblind <div\n                    class="font-xs text-right">(experimental)\n            </div></span>\n            </label><span id="smart-bgimages"></span></section>\n        <section><h6 class="margin-top-10 semi-bold margin-bottom-5">Clear Localstorage</h6><a\n                ng-click="factoryReset()" class="btn btn-xs btn-block btn-primary" id="reset-smart-widget"><i\n                class="fa fa-refresh"></i> Factory Reset</a></section>\n\n        <h6 class="margin-top-10 semi-bold margin-bottom-5">SmartAdmin Skins</h6>\n\n\n        <section id="smart-styles">\n            <a ng-repeat="skin in skins" ng-click="setSkin(skin)" class="{{skin.class}}" style="{{skin.style}}"><i ng-if="skin.name == $parent.smartSkin" class="fa fa-check fa-fw"></i> {{skin.label}}</a>\n        </section>\n    </form>\n</div>'),a.put("build/layout/layout.tpl.html",'<!-- HEADER -->\n<div data-smart-include="build/layout/partials/header.tpl.html" class="placeholder-header"></div>\n<!-- END HEADER -->\n\n\n<!-- Left panel : Navigation area -->\n<!-- Note: This width of the aside area can be adjusted through LESS variables -->\n<div data-smart-include="build/layout/partials/navigation.tpl.html" class="placeholder-left-panel"></div>\n\n<!-- END NAVIGATION -->\n\n<!-- MAIN PANEL -->\n<div id="main" role="main">\n\n    <demo-states></demo-states>\n\n    <!-- RIBBON -->\n    <div id="ribbon">\n\n				<span class="ribbon-button-alignment">\n					<span id="refresh" class="btn btn-ribbon" reset-widgets\n                          tooltip-placement="bottom"\n                          tooltip-html-unsafe="<i class=\'text-warning fa fa-warning\'></i> Warning! This will reset all your widget settings.">\n						<i class="fa fa-refresh"></i>\n					</span>\n				</span>\n\n        <!-- breadcrumb -->\n        <state-breadcrumbs></state-breadcrumbs>\n        <!-- end breadcrumb -->\n\n\n    </div>\n    <!-- END RIBBON -->\n\n\n    <div data-smart-router-animation-wrap="content content@app" data-wrap-for="#content">\n        <div data-ui-view="content" data-autoscroll="false"></div>\n    </div>\n\n</div>\n<!-- END MAIN PANEL -->\n\n<!-- PAGE FOOTER -->\n<div data-smart-include="build/layout/partials/footer.tpl.html"></div>\n\n<div data-smart-include="build/components/shortcut/shortcut.tpl.html"></div>\n\n<!-- END PAGE FOOTER -->\n\n\n'),a.put("build/layout/partials/footer.tpl.html",'<div class="page-footer">\n    <div class="row">\n        <div class="col-xs-12 col-sm-6">\n            <span class="txt-color-white">SmartAdmin WebApp © 2013-2014</span>\n        </div>\n\n        <div class="col-xs-6 col-sm-6 text-right hidden-xs">\n            <div class="txt-color-white inline-block">\n                <i class="txt-color-blueLight hidden-mobile">Last account activity <i class="fa fa-clock-o"></i>\n                    <strong>52 mins ago &nbsp;</strong> </i>\n\n                <div class="btn-group dropup">\n                    <button class="btn btn-xs dropdown-toggle bg-color-blue txt-color-white" data-toggle="dropdown">\n                        <i class="fa fa-link"></i> <span class="caret"></span>\n                    </button>\n                    <ul class="dropdown-menu pull-right text-left">\n                        <li>\n                            <div class="padding-5">\n                                <p class="txt-color-darken font-sm no-margin">Download Progress</p>\n\n                                <div class="progress progress-micro no-margin">\n                                    <div class="progress-bar progress-bar-success" style="width: 50%;"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class="divider"></li>\n                        <li>\n                            <div class="padding-5">\n                                <p class="txt-color-darken font-sm no-margin">Server Load</p>\n\n                                <div class="progress progress-micro no-margin">\n                                    <div class="progress-bar progress-bar-success" style="width: 20%;"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class="divider"></li>\n                        <li>\n                            <div class="padding-5">\n                                <p class="txt-color-darken font-sm no-margin">Memory Load <span class="text-danger">*critical*</span>\n                                </p>\n\n                                <div class="progress progress-micro no-margin">\n                                    <div class="progress-bar progress-bar-danger" style="width: 70%;"></div>\n                                </div>\n                            </div>\n                        </li>\n                        <li class="divider"></li>\n                        <li>\n                            <div class="padding-5">\n                                <button class="btn btn-block btn-default">refresh</button>\n                            </div>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>'),a.put("build/layout/partials/header.tpl.html",'<header id="header">\n<div id="logo-group">\n\n    <!-- PLACE YOUR LOGO HERE -->\n    <span id="logo"> <img src="styles/img/logo.png" alt="SmartAdmin"> </span>\n    <!-- END LOGO PLACEHOLDER -->\n\n    <!-- Note: The activity badge color changes when clicked and resets the number to 0\n    Suggestion: You may want to set a flag when this happens to tick off all checked messages / notifications -->\n    <span id="activity" class="activity-dropdown" activities-dropdown-toggle> \n        <i class="fa fa-user"></i> \n        <b class="badge bg-color-red">21</b> \n    </span>\n    <div smart-include="build/components/activities/activities.html"></div>\n</div>\n\n\n<recent-projects></recent-projects>\n\n\n\n<!-- pulled right: nav area -->\n<div class="pull-right">\n\n    <!-- collapse menu button -->\n    <div id="hide-menu" class="btn-header pull-right">\n        <span> <a toggle-menu title="Collapse Menu"><i\n                class="fa fa-reorder"></i></a> </span>\n    </div>\n    <!-- end collapse menu -->\n\n    <!-- #MOBILE -->\n    <!-- Top menu profile link : this shows only when top menu is active -->\n    <ul id="mobile-profile-img" class="header-dropdown-list hidden-xs padding-5">\n        <li class="">\n            <a href="#" class="dropdown-toggle no-margin userdropdown" data-toggle="dropdown">\n                <img src="styles/img/avatars/sunny.png" alt="John Doe" class="online"/>\n            </a>\n            <ul class="dropdown-menu pull-right">\n                <li>\n                    <a href-void class="padding-10 padding-top-0 padding-bottom-0"><i\n                            class="fa fa-cog"></i> Setting</a>\n                </li>\n                <li class="divider"></li>\n                <li>\n                    <a ui-sref="app.appViews.profileDemo" class="padding-10 padding-top-0 padding-bottom-0"> <i class="fa fa-user"></i>\n                        <u>P</u>rofile</a>\n                </li>\n                <li class="divider"></li>\n                <li>\n                    <a href-void class="padding-10 padding-top-0 padding-bottom-0"\n                       data-action="toggleShortcut"><i class="fa fa-arrow-down"></i> <u>S</u>hortcut</a>\n                </li>\n                <li class="divider"></li>\n                <li>\n                    <a href-void class="padding-10 padding-top-0 padding-bottom-0"\n                       data-action="launchFullscreen"><i class="fa fa-arrows-alt"></i> Full <u>S</u>creen</a>\n                </li>\n                <li class="divider"></li>\n                <li>\n                    <a href="#/login" class="padding-10 padding-top-5 padding-bottom-5" data-action="userLogout"><i\n                            class="fa fa-sign-out fa-lg"></i> <strong><u>L</u>ogout</strong></a>\n                </li>\n            </ul>\n        </li>\n    </ul>\n\n    <!-- logout button -->\n    <div id="logout" class="btn-header transparent pull-right">\n        <span> <a ui-sref="login" title="Sign Out" data-action="userLogout"\n                  data-logout-msg="You can improve your security further after logging out by closing this opened browser"><i\n                class="fa fa-sign-out"></i></a> </span>\n    </div>\n    <!-- end logout button -->\n\n    <!-- search mobile button (this is hidden till mobile view port) -->\n    <div id="search-mobile" class="btn-header transparent pull-right" data-search-mobile>\n        <span> <a href="#" title="Search"><i class="fa fa-search"></i></a> </span>\n    </div>\n    <!-- end search mobile button -->\n\n    <!-- input: search field -->\n    <form action="#/search" class="header-search pull-right">\n        <input id="search-fld" type="text" name="param" placeholder="Find reports and more" data-autocomplete=\'[\n					"ActionScript",\n					"AppleScript",\n					"Asp",\n					"BASIC",\n					"C",\n					"C++",\n					"Clojure",\n					"COBOL",\n					"ColdFusion",\n					"Erlang",\n					"Fortran",\n					"Groovy",\n					"Haskell",\n					"Java",\n					"JavaScript",\n					"Lisp",\n					"Perl",\n					"PHP",\n					"Python",\n					"Ruby",\n					"Scala",\n					"Scheme"]\'>\n        <button type="submit">\n            <i class="fa fa-search"></i>\n        </button>\n        <a href="$" id="cancel-search-js" title="Cancel Search"><i class="fa fa-times"></i></a>\n    </form>\n    <!-- end input: search field -->\n\n    <!-- fullscreen button -->\n    <div id="fullscreen" class="btn-header transparent pull-right">\n        <span> <a full-screen title="Full Screen"><i\n                class="fa fa-arrows-alt"></i></a> </span>\n    </div>\n    <!-- end fullscreen button -->\n\n    <!-- #Voice Command: Start Speech -->\n    <div id="speech-btn" class="btn-header transparent pull-right hidden-sm hidden-xs">\n        <div>\n            <a title="Voice Command" id="voice-command-btn" speech-recognition><i class="fa fa-microphone"></i></a>\n\n            <div class="popover bottom">\n                <div class="arrow"></div>\n                <div class="popover-content">\n                    <h4 class="vc-title">Voice command activated <br>\n                        <small>Please speak clearly into the mic</small>\n                    </h4>\n                    <h4 class="vc-title-error text-center">\n                        <i class="fa fa-microphone-slash"></i> Voice command failed\n                        <br>\n                        <small class="txt-color-red">Must <strong>"Allow"</strong> Microphone</small>\n                        <br>\n                        <small class="txt-color-red">Must have <strong>Internet Connection</strong></small>\n                    </h4>\n                    <a href-void class="btn btn-success" id="speech-help-btn">See Commands</a>\n                    <a href-void class="btn bg-color-purple txt-color-white"\n                       onclick="$(\'#speech-btn .popover\').fadeOut(50);">Close Popup</a>\n                </div>\n            </div>\n        </div>\n    </div>\n    <!-- end voice command -->\n\n\n\n    <!-- multiple lang dropdown : find all flags in the flags page -->\n    <language-selector></language-selector>\n    <!-- end multiple lang -->\n\n</div>\n<!-- end pulled right: nav area -->\n\n</header>'),a.put("build/layout/partials/navigation.tpl.html",'<aside id="left-panel">\n\n    <!-- User info -->\n    <div login-info></div>\n    <!-- end user info -->\n\n    <nav>\n        <!-- NOTE: Notice the gaps after each icon usage <i></i>..\n        Please note that these links work a bit different than\n        traditional href="" links. See documentation for details.\n        -->\n\n        <ul data-smart-menu>\n            <li data-ui-sref-active="active">\n                <a data-ui-sref="app.dashboard" title="Dashboard"><i class="fa fa-lg fa-fw fa-home"></i> <span\n                        class="menu-item-parent">{{getWord(\'Dashboard\')}}</span></a>\n            </li>\n            <li data-ui-sref-active="active">\n                <a data-ui-sref="app.inbox.folder" title="Inbox">\n                    <i class="fa fa-lg fa-fw fa-inbox"></i> <span class="menu-item-parent">{{getWord(\'Inbox\')}}</span><span\n                        unread-messages-count class="badge pull-right inbox-badge"></span></a>\n            </li>\n            <li data-menu-collapse>\n                <a href="#"><i class="fa fa-lg fa-fw fa-bar-chart-o"></i> <span class="menu-item-parent">{{getWord(\'Graphs\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.graphs.flot">{{getWord(\'Flot Chart\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.graphs.morris">{{getWord(\'Morris Charts\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.graphs.inline">{{getWord(\'Inline Charts\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.graphs.dygraphs">{{getWord(\'Dygraphs\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.graphs.chartjs">Chart.js <span\n                                class="badge pull-right inbox-badge bg-color-yellow">new</span></a>\n                    </li>\n                </ul>\n            </li>\n\n            <li data-menu-collapse>\n                <a href="#"><i class="fa fa-lg fa-fw fa-table"></i> <span class="menu-item-parent">{{getWord(\'Tables\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.tables.normal">{{getWord(\'Normal Tables\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.tables.datatables">{{getWord(\'Data Tables\')}} <span\n                                class="badge inbox-badge bg-color-greenLight">v1.10</span></a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.tables.jqgrid">{{getWord(\'Jquery Grid\')}}</a>\n                    </li>\n                </ul>\n            </li>\n\n            <li data-menu-collapse>\n                <a href="#"><i class="fa fa-lg fa-fw fa-pencil-square-o"></i> <span class="menu-item-parent">{{getWord(\'Forms\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.elements">{{getWord(\'Smart Form Elements\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.layouts">{{getWord(\'Smart Form Layouts\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.validation">{{getWord(\'Smart Form Validation\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.bootstrapForms">{{getWord(\'Bootstrap Form Elements\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.bootstrapValidation">{{getWord(\'Bootstrap Form Validation\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.plugins">{{getWord(\'Form Plugins\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.wizards">{{getWord(\'Wizards\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.editors">{{getWord(\'Bootstrap Editors\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.dropzone">{{getWord(\'Dropzone\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.form.imageEditor">{{getWord(\'Image Cropping\')}} <span class="badge pull-right inbox-badge bg-color-yellow">new</span></a>\n                    </li>\n                </ul>\n            </li>\n            <li data-menu-collapse>\n                <a href="#"><i class="fa fa-lg fa-fw fa-desktop"></i> <span class="menu-item-parent">{{getWord(\'UI Elements\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.general">{{getWord(\'General Elements\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.buttons">{{getWord(\'Buttons\')}}</a>\n                    </li>\n                    <li data-menu-collapse>\n                        <a href="#">{{getWord(\'Icons\')}}</a>\n                        <ul>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.ui.iconsFa"><i class="fa fa-plane"></i> {{getWord(\'Font Awesome\')}}</a>\n                            </li>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.ui.iconsGlyph" ><i class="glyphicon glyphicon-plane"></i> {{getWord(\'Glyph Icons\')}}</a>\n                            </li>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.ui.iconsFlags" ><i class="fa fa-flag"></i> {{getWord(\'Flags\')}}</a>\n                            </li>\n                        </ul>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.grid" >{{getWord(\'Grid\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.treeView">{{getWord(\'Tree View\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.nestableLists">{{getWord(\'Nestable Lists\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.jqueryUi">{{getWord(\'JQuery UI\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.ui.typography">{{getWord(\'Typography\')}}</a>\n                    </li>\n                    <li data-menu-collapse>\n                        <a href="#">{{getWord(\'Six Level Menu\')}}</a>\n                        <ul>\n                            <li data-menu-collapse>\n                                <a href="#"><i class="fa fa-fw fa-folder-open"></i> {{getWord(\'Item #2\')}}</a>\n                                <ul>\n                                    <li data-menu-collapse>\n                                        <a href="#"><i class="fa fa-fw fa-folder-open"></i> {{getWord(\'Sub #2.1\')}} </a>\n                                        <ul>\n                                            <li>\n                                                <a href="#"><i class="fa fa-fw fa-file-text"></i> {{getWord(\'Item #2.1.1\')}}</a>\n                                            </li>\n                                            <li data-menu-collapse>\n                                                <a href="#"><i class="fa fa-fw fa-plus"></i>{{getWord(\'Expand\')}}</a>\n                                                <ul>\n                                                    <li>\n                                                        <a href="#"><i class="fa fa-fw fa-file-text"></i> {{getWord(\'File\')}}</a>\n                                                    </li>\n                                                    <li>\n                                                        <a href="#"><i class="fa fa-fw fa-trash-o"></i> {{getWord(\'Delete\')}}</a></li>\n                                                </ul>\n                                            </li>\n                                        </ul>\n                                    </li>\n                                </ul>\n                            </li>\n                            <li data-menu-collapse>\n                                <a href="#"><i class="fa fa-fw fa-folder-open"></i> {{getWord(\'Item #3\')}}</a>\n\n                                <ul>\n                                    <li data-menu-collapse>\n                                        <a href="#"><i class="fa fa-fw fa-folder-open"></i> {{getWord(\'3ed Level\')}} </a>\n                                        <ul>\n                                            <li>\n                                                <a href="#"><i class="fa fa-fw fa-file-text"></i> {{getWord(\'File\')}}</a>\n                                            </li>\n                                            <li>\n                                                <a href="#"><i class="fa fa-fw fa-file-text"></i> {{getWord(\'File\')}}</a>\n                                            </li>\n                                        </ul>\n                                    </li>\n                                </ul>\n\n                            </li>\n                        </ul>\n                    </li>\n                </ul>\n            </li>\n\n            <li data-ui-sref-active="active">\n                <a data-ui-sref="app.calendar" title="Calendar"><i class="fa fa-lg fa-fw fa-calendar"><em>3</em></i> <span\n                        class="menu-item-parent">{{getWord(\'Calendar\')}}</span></a>\n            </li>\n            <li data-ui-sref-active="active">\n                <a data-ui-sref="app.widgets" title="Widgets"><i class="fa fa-lg fa-fw fa-list-alt"></i><span\n                        class="menu-item-parent">{{getWord(\'Widgets\')}}</span></a>\n            </li>\n\n            <li data-menu-collapse>\n                <a  href="#" >\n                    <i class="fa fa-lg fa-fw fa-puzzle-piece"></i> <span class="menu-item-parent">{{getWord(\'App Views\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.appViews.projects"><i class="fa fa-file-text-o"></i> {{getWord(\'Projects\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.appViews.blogDemo"><i class="fa fa-paragraph"></i> {{getWord(\'Blog\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.appViews.galleryDemo"><i class="fa fa-picture-o"></i> {{getWord(\'Gallery\')}}</a>\n                    </li>\n\n                    <li data-menu-collapse>\n                        <a href="#"><i class="fa fa-comments"></i> {{getWord(\'Forum Layout\')}}</a>\n                        <ul>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.appViews.forumDemo"><i class="fa fa-picture-o"></i> {{getWord(\'General View\')}}</a>\n                            </li>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.appViews.forumTopicDemo"><i class="fa fa-picture-o"></i> {{getWord(\'Topic View\')}}</a>\n                            </li>\n                            <li data-ui-sref-active="active">\n                                <a data-ui-sref="app.appViews.forumPostDemo"><i class="fa fa-picture-o"></i> {{getWord(\'Post View\')}}</a>\n                            </li>\n                        </ul>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.appViews.profileDemo"><i class="fa fa-group"></i> {{getWord(\'Profile\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.appViews.timelineDemo"><i class="fa fa-clock-o"></i> {{getWord(\'Timeline\')}}</a>\n                    </li>\n                </ul>\n            </li>\n\n            <li data-ui-sref-active="active">\n                <a data-ui-sref="app.maps"><i class="fa fa-lg fa-fw fa-map-marker"></i> <span class="menu-item-parent">{{getWord(\'GMap Skins\')}}</span><span class="badge bg-color-greenLight pull-right inbox-badge">9</span></a>\n            </li>\n\n            <li data-menu-collapse>\n                <a href="#"><i class="fa fa-lg fa-fw fa-windows"></i> <span class="menu-item-parent">{{getWord(\'Miscellaneous\')}}</span></a>\n                <ul >\n                    <li>\n                        <a href="http://bootstraphunter.com/smartadmin-landing/" target="_blank">{{getWord(\'Landing Page\')}} <i class="fa fa-external-link"></i></a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.pricingTable">{{getWord(\'Pricing Tables\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.invoice">{{getWord(\'Invoice\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="login">{{getWord(\'Login\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="register">{{getWord(\'Register\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="lock">{{getWord(\'Locked Screen\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.error404">{{getWord(\'Error 404\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.error500">{{getWord(\'Error 500\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.blank">{{getWord(\'Blank Page\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.emailTemplate">{{getWord(\'Email Template\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.search">{{getWord(\'Search Page\')}}</a>\n                    </li>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.misc.ckeditor">{{getWord(\'CK Editor\')}}</a>\n                    </li>\n                </ul>\n            </li>\n\n          <li data-menu-collapse class="top-menu-invisible">\n                <a href="#"><i class="fa fa-lg fa-fw fa-cube txt-color-blue"></i> <span class="menu-item-parent">{{getWord(\'SmartAdmin Intel\')}}</span></a>\n                <ul>\n                    <li data-ui-sref-active="active">\n                        <a data-ui-sref="app.smartAdmin.diffVer"><i class="fa fa-stack-overflow"></i> {{getWord(\'Different Versions\')}}</a>\n                    </li>\n                    <!--<li data-ui-sref-active="active">\n                        <a data-ui-sref="app.smartAdmin.appLayout"><i class="fa fa-cube"></i> {{getWord(\'App Settings\')}}</a>\n                    </li>-->\n                    <li>\n                        <a href="http://bootstraphunter.com/smartadmin/BUGTRACK/track_/documentation/index.html" target="_blank"><i class="fa fa-book"></i> {{getWord(\'Documentation\')}}</a>\n                    </li>\n                    <li>\n                        <a href="http://bootstraphunter.com/smartadmin/BUGTRACK/track_/" target="_blank"><i class="fa fa-bug"></i> {{getWord(\'Bug Tracker\')}}</a>\n                    </li>\n                </ul>\n            </li>\n            <li data-menu-collapse class="chat-users top-menu-invisible">\n                <a href="#"><i class="fa fa-lg fa-fw fa-comment-o"><em class="bg-color-pink flash animated">!</em></i> <span class="menu-item-parent">{{getWord(\'Smart Chat API\')}} <sup>{{getWord(\'beta\')}}</sup></span></a>\n                <div aside-chat-widget></div>\n            </li>\n        </ul>\n    </nav>\n\n  <span class="minifyme" data-action="minifyMenu" minify-menu>\n    <i class="fa fa-arrow-circle-left hit"></i>\n  </span>\n\n</aside>'),a.put("build/layout/partials/sub-header.tpl.html",'<div class="col-xs-12 col-sm-5 col-md-5 col-lg-8" data-sparkline-container>\n    <ul id="sparks" class="">\n        <li class="sparks-info">\n            <h5> My Income <span class="txt-color-blue">$47,171</span></h5>\n            <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">\n                1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471\n            </div>\n        </li>\n        <li class="sparks-info">\n            <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;45%</span></h5>\n            <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">\n                110,150,300,130,400,240,220,310,220,300, 270, 210\n            </div>\n        </li>\n        <li class="sparks-info">\n            <h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span></h5>\n            <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">\n                110,150,300,130,400,240,220,310,220,300, 270, 210\n            </div>\n        </li>\n    </ul>\n</div>\n			'),a.put("build/layout/partials/voice-commands.tpl.html",'<!-- TRIGGER BUTTON:\n<a href="/my-ajax-page.html" data-toggle="modal" data-target="#remoteModal" class="btn btn-default">Open Modal</a>  -->\n\n<!-- MODAL PLACE HOLDER\n<div class="modal fade" id="remoteModal" tabindex="-1" role="dialog" aria-labelledby="remoteModalLabel" aria-hidden="true">\n<div class="modal-dialog">\n<div class="modal-content"></div>\n</div>\n</div>   -->\n<!--////////////////////////////////////-->\n\n<!--<div class="modal-header">\n<button type="button" class="close" data-dismiss="modal" aria-hidden="true">\n&times;\n</button>\n<h4 class="modal-title" id="myModalLabel">Command List</h4>\n</div>-->\n<div class="modal-body">\n\n	<h1><i class="fa fa-microphone text-muted"></i>&nbsp;&nbsp; SmartAdmin Voice Command</h1>\n	<hr class="simple">\n	<h5>Instruction</h5>\n\n	Click <span class="text-success">"Allow"</span> to access your microphone and activate Voice Command.\n	You will notice a <span class="text-primary"><strong>BLUE</strong> Flash</span> on the microphone icon indicating activation.\n	The icon will appear <span class="text-danger"><strong>RED</strong></span> <span class="label label-danger"><i class="fa fa-microphone fa-lg"></i></span> if you <span class="text-danger">"Deny"</span> access or don\'t have any microphone installed.\n	<br>\n	<br>\n	As a security precaution, your browser will disconnect the microphone every 60 to 120 seconds (sooner if not being used). In which case Voice Command will prompt you again to <span class="text-success">"Allow"</span> or <span class="text-danger">"Deny"</span> access to your microphone.\n	<br>\n	<br>\n	If you host your page over <strong>http<span class="text-success">s</span></strong> (secure socket layer) protocol you can wave this security measure and have an unintrupted Voice Command.\n	<br>\n	<br>\n	<h5>Commands</h5>\n	<ul>\n		<li>\n			<strong>\'show\' </strong> then say the <strong>*page*</strong> you want to go to. For example <strong>"show inbox"</strong> or <strong>"show calendar"</strong>\n		</li>\n		<li>\n			<strong>\'mute\' </strong> - mutes all sound effects for the theme.\n		</li>\n		<li>\n			<strong>\'sound on\'</strong> - unmutes all sound effects for the theme.\n		</li>\n		<li>\n			<span class="text-danger"><strong>\'stop\'</strong></span> - deactivates voice command.\n		</li>\n		<li>\n			<span class="text-primary"><strong>\'help\'</strong></span> - brings up the command list\n		</li>\n		<li>\n			<span class="text-danger"><strong>\'got it\'</strong></span> - closes help modal\n		</li>\n		<li>\n			<strong>\'hide navigation\'</strong> - toggle navigation collapse\n		</li>\n		<li>\n			<strong>\'show navigation\'</strong> - toggle navigation to open (can be used again to close)\n		</li>\n		<li>\n			<strong>\'scroll up\'</strong> - scrolls to the top of the page\n		</li>\n		<li>\n			<strong>\'scroll down\'</strong> - scrollts to the bottom of the page\n		</li>\n		<li>\n			<strong>\'go back\' </strong> - goes back in history (history -1 click)\n		</li>\n		<li>\n			<strong>\'logout\'</strong> - logs you out\n		</li>\n	</ul>\n	<br>\n	<h5>Adding your own commands</h5>\n	Voice Command supports up to 80 languages. Adding your own commands is extreamly easy. All commands are stored inside <strong>app.config.js</strong> file under the <code>var commands = {...}</code>. \n\n	<hr class="simple">\n	<div class="text-right">\n		<button type="button" class="btn btn-success btn-lg" data-dismiss="modal">\n			Got it!\n		</button>\n	</div>\n\n</div>\n<!--<div class="modal-footer">\n<button type="button" class="btn btn-primary" data-dismiss="modal">Got it!</button>\n</div> -->'),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-attribute-form.tpl.html",'<form id="attributeForm" class="form-horizontal"\n      data-bv-message="This value is not valid"\n      data-bv-feedbackicons-valid="glyphicon glyphicon-ok"\n      data-bv-feedbackicons-invalid="glyphicon glyphicon-remove"\n      data-bv-feedbackicons-validating="glyphicon glyphicon-refresh">\n\n    <fieldset>\n        <legend>\n            Set validator options via HTML attributes\n        </legend>\n\n        <div class="alert alert-warning">\n            <code>&lt; input\n                data-bv-validatorname\n                data-bv-validatorname-validatoroption="..." / &gt;</code>\n\n            <br>\n            <br>\n            More validator options can be found here:\n            <a href="http://bootstrapvalidator.com/validators/" target="_blank">http://bootstrapvalidator.com/validators/</a>\n        </div>\n\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Full name</label>\n            <div class="col-lg-4">\n                <input type="text" class="form-control" name="firstName" placeholder="First name"\n                       data-bv-notempty="true"\n                       data-bv-notempty-message="The first name is required and cannot be empty" />\n            </div>\n            <div class="col-lg-4">\n                <input type="text" class="form-control" name="lastName" placeholder="Last name"\n                       data-bv-notempty="true"\n                       data-bv-notempty-message="The last name is required and cannot be empty" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Username</label>\n            <div class="col-lg-5">\n                <input type="text" class="form-control" name="username"\n                       data-bv-message="The username is not valid"\n\n                       data-bv-notempty="true"\n                       data-bv-notempty-message="The username is required and cannot be empty"\n\n                       data-bv-regexp="true"\n                       data-bv-regexp-regexp="^[a-zA-Z0-9_\\.]+$"\n                       data-bv-regexp-message="The username can only consist of alphabetical, number, dot and underscore"\n\n                       data-bv-stringlength="true"\n                       data-bv-stringlength-min="6"\n                       data-bv-stringlength-max="30"\n                       data-bv-stringlength-message="The username must be more than 6 and less than 30 characters long"\n\n                       data-bv-different="true"\n                       data-bv-different-field="password"\n                       data-bv-different-message="The username and password cannot be the same as each other" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Email address</label>\n            <div class="col-lg-5">\n                <input class="form-control" name="email" type="email"\n                       data-bv-emailaddress="true"\n                       data-bv-emailaddress-message="The input is not a valid email address" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Password</label>\n            <div class="col-lg-5">\n                <input type="password" class="form-control" name="password"\n                       data-bv-notempty="true"\n                       data-bv-notempty-message="The password is required and cannot be empty"\n\n                       data-bv-identical="true"\n                       data-bv-identical-field="confirmPassword"\n                       data-bv-identical-message="The password and its confirm are not the same"\n\n                       data-bv-different="true"\n                       data-bv-different-field="username"\n                       data-bv-different-message="The password cannot be the same as username" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Retype password</label>\n            <div class="col-lg-5">\n                <input type="password" class="form-control" name="confirmPassword"\n                       data-bv-notempty="true"\n                       data-bv-notempty-message="The confirm password is required and cannot be empty"\n\n                       data-bv-identical="true"\n                       data-bv-identical-field="password"\n                       data-bv-identical-message="The password and its confirm are not the same"\n\n                       data-bv-different="true"\n                       data-bv-different-field="username"\n                       data-bv-different-message="The password cannot be the same as username" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Languages</label>\n            <div class="col-lg-5">\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="languages[]" value="english"\n                               data-bv-message="Please specify at least one language you can speak"\n                               data-bv-notempty="true" />\n                        English </label>\n                </div>\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="languages[]" value="french" />\n                        French </label>\n                </div>\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="languages[]" value="german" />\n                        German </label>\n                </div>\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="languages[]" value="russian" />\n                        Russian </label>\n                </div>\n                <div class="checkbox">\n                    <label>\n                        <input type="checkbox" name="languages[]" value="other" />\n                        Other </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n     '),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-button-group-form.tpl.html",'<form id="buttonGroupForm" method="post" class="form-horizontal">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Gender</label>\n            <div class="col-lg-9">\n                <div class="btn-group" data-toggle="buttons">\n                    <label class="btn btn-default">\n                        <input type="radio" name="gender" value="male" />\n                        Male </label>\n                    <label class="btn btn-default">\n                        <input type="radio" name="gender" value="female" />\n                        Female </label>\n                    <label class="btn btn-default">\n                        <input type="radio" name="gender" value="other" />\n                        Other </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Languages</label>\n            <div class="col-lg-9">\n                <div class="btn-group" data-toggle="buttons">\n                    <label class="btn btn-default">\n                        <input type="checkbox" name="languages[]" value="english" />\n                        English </label>\n                    <label class="btn btn-default">\n                        <input type="checkbox" name="languages[]" value="german" />\n                        German </label>\n                    <label class="btn btn-default">\n                        <input type="checkbox" name="languages[]" value="french" />\n                        French </label>\n                    <label class="btn btn-default">\n                        <input type="checkbox" name="languages[]" value="russian" />\n                        Russian </label>\n                    <label class="btn btn-default">\n                        <input type="checkbox" name="languages[]" value="italian">\n                        Italian </label>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n'),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-contact-form.tpl.html",'<form id="contactForm" method="post" class="form-horizontal">\n\n    <fieldset>\n        <legend>Showing messages in custom area</legend>\n        <div class="form-group">\n            <label class="col-md-3 control-label">Full name</label>\n            <div class="col-md-6">\n                <input type="text" class="form-control" name="fullName" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-md-3 control-label">Email</label>\n            <div class="col-md-6">\n                <input type="text" class="form-control" name="email" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-md-3 control-label">Title</label>\n            <div class="col-md-6">\n                <input type="text" class="form-control" name="title" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-md-3 control-label">Content</label>\n            <div class="col-md-6">\n                <textarea class="form-control" name="content" rows="5"></textarea>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <!-- #messages is where the messages are placed inside -->\n        <div class="form-group">\n            <div class="col-md-9 col-md-offset-3">\n                <div id="messages"></div>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n'),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-movie-form.tpl.html",'<form id="movieForm" method="post">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class="form-group">\n            <div class="row">\n                <div class="col-md-8">\n                    <label class="control-label">Movie title</label>\n                    <input type="text" class="form-control" name="title" />\n                </div>\n\n                <div class="col-md-4 selectContainer">\n                    <label class="control-label">Genre</label>\n                    <select class="form-control" name="genre">\n                        <option value="">Choose a genre</option>\n                        <option value="action">Action</option>\n                        <option value="comedy">Comedy</option>\n                        <option value="horror">Horror</option>\n                        <option value="romance">Romance</option>\n                    </select>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <div class="row">\n                <div class="col-sm-12 col-md-4">\n                    <label class="control-label">Director</label>\n                    <input type="text" class="form-control" name="director" />\n                </div>\n\n                <div class="col-sm-12 col-md-4">\n                    <label class="control-label">Writer</label>\n                    <input type="text" class="form-control" name="writer" />\n                </div>\n\n                <div class="col-sm-12 col-md-4">\n                    <label class="control-label">Producer</label>\n                    <input type="text" class="form-control" name="producer" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <div class="row">\n                <div class="col-sm-12 col-md-6">\n                    <label class="control-label">Website</label>\n                    <input type="text" class="form-control" name="website" />\n                </div>\n\n                <div class="col-sm-12 col-md-6">\n                    <label class="control-label">Youtube trailer</label>\n                    <input type="text" class="form-control" name="trailer" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="control-label">Review</label>\n            <textarea class="form-control" name="review" rows="8"></textarea>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n\n            <div class="row">\n                <div class="col-sm-12 col-md-12">\n                    <label class="control-label">Rating</label>\n                </div>\n\n                <div class="col-sm-12 col-md-10">\n\n                    <label class="radio radio-inline no-margin">\n                        <input type="radio" name="rating" value="terrible" class="radiobox style-2" />\n                        <span>Terrible</span> </label>\n\n                    <label class="radio radio-inline">\n                        <input type="radio" name="rating" value="watchable" class="radiobox style-2" />\n                        <span>Watchable</span> </label>\n                    <label class="radio radio-inline">\n                        <input type="radio" name="rating" value="best" class="radiobox style-2" />\n                        <span>Best ever</span> </label>\n\n                </div>\n\n            </div>\n\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n\n</form>\n\n '),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-product-form.tpl.html",'<form id="productForm" class="form-horizontal">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class="form-group">\n            <label class="col-xs-2 col-lg-3 control-label">Price</label>\n            <div class="col-xs-9 col-lg-6 inputGroupContainer">\n                <div class="input-group">\n                    <input type="text" class="form-control" name="price" />\n                    <span class="input-group-addon">$</span>\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-xs-2 col-lg-3 control-label">Amount</label>\n            <div class="col-xs-9 col-lg-6 inputGroupContainer">\n                <div class="input-group">\n                    <span class="input-group-addon">&#8364;</span>\n                    <input type="text" class="form-control" name="amount" />\n                </div>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-xs-2 col-lg-3 control-label">Color</label>\n            <div class="col-xs-9 col-lg-6 selectContainer">\n                <select class="form-control" name="color">\n                    <option value="">Choose a color</option>\n                    <option value="blue">Blue</option>\n                    <option value="green">Green</option>\n                    <option value="red">Red</option>\n                    <option value="yellow">Yellow</option>\n                    <option value="white">White</option>\n                </select>\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-xs-2 col-lg-3 control-label">Size</label>\n            <div class="col-xs-9 col-lg-6 selectContainer">\n                <select class="form-control" name="size">\n                    <option value="">Choose a size</option>\n                    <option value="S">S</option>\n                    <option value="M">M</option>\n                    <option value="L">L</option>\n                    <option value="XL">XL</option>\n                </select>\n            </div>\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>\n\n'),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-profile-form.tpl.html",'<form id="profileForm">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class="form-group">\n            <label>Email address</label>\n            <input type="text" class="form-control" name="email" />\n        </div>\n    </fieldset>\n    <fieldset>\n        <div class="form-group">\n            <label>Password</label>\n            <input type="password" class="form-control" name="password" />\n        </div>\n    </fieldset>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>\n'),a.put("build/modules/forms/directives/bootstrap-validation/bootstrap-toggling-form.tpl.html",'<form id="togglingForm" method="post" class="form-horizontal">\n\n    <fieldset>\n        <legend>\n            Default Form Elements\n        </legend>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Full name <sup>*</sup></label>\n            <div class="col-lg-4">\n                <input type="text" class="form-control" name="firstName" placeholder="First name" />\n            </div>\n            <div class="col-lg-4">\n                <input type="text" class="form-control" name="lastName" placeholder="Last name" />\n            </div>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Company <sup>*</sup></label>\n            <div class="col-lg-5">\n                <input type="text" class="form-control" name="company"\n                       required data-bv-notempty-message="The company name is required" />\n            </div>\n            <div class="col-lg-2">\n                <button type="button" class="btn btn-info btn-sm" data-toggle="#jobInfo">\n                    Add more info\n                </button>\n            </div>\n        </div>\n    </fieldset>\n\n    <!-- These fields will not be validated as long as they are not visible -->\n    <div id="jobInfo" style="display: none;">\n        <fieldset>\n            <div class="form-group">\n                <label class="col-lg-3 control-label">Job title <sup>*</sup></label>\n                <div class="col-lg-5">\n                    <input type="text" class="form-control" name="job" />\n                </div>\n            </div>\n        </fieldset>\n\n        <fieldset>\n            <div class="form-group">\n                <label class="col-lg-3 control-label">Department <sup>*</sup></label>\n                <div class="col-lg-5">\n                    <input type="text" class="form-control" name="department" />\n                </div>\n            </div>\n        </fieldset>\n    </div>\n\n    <fieldset>\n        <div class="form-group">\n            <label class="col-lg-3 control-label">Mobile phone <sup>*</sup></label>\n            <div class="col-lg-5">\n                <input type="text" class="form-control" name="mobilePhone" />\n            </div>\n            <div class="col-lg-2">\n                <button type="button" class="btn btn-info btn-sm" data-toggle="#phoneInfo">\n                    Add more phone numbers\n                </button>\n            </div>\n        </div>\n    </fieldset>\n    <!-- These fields will not be validated as long as they are not visible -->\n    <div id="phoneInfo" style="display: none;">\n\n        <fieldset>\n            <div class="form-group">\n                <label class="col-lg-3 control-label">Home phone</label>\n                <div class="col-lg-5">\n                    <input type="text" class="form-control" name="homePhone" />\n                </div>\n            </div>\n        </fieldset>\n        <fieldset>\n            <div class="form-group">\n                <label class="col-lg-3 control-label">Office phone</label>\n                <div class="col-lg-5">\n                    <input type="text" class="form-control" name="officePhone" />\n                </div>\n            </div>\n        </fieldset>\n    </div>\n\n    <div class="form-actions">\n        <div class="row">\n            <div class="col-md-12">\n                <button class="btn btn-default" type="submit">\n                    <i class="fa fa-eye"></i>\n                    Validate\n                </button>\n            </div>\n        </div>\n    </div>\n</form>'),a.put("build/modules/forms/directives/form-layouts/smart-checkout-form.tpl.html",'<form id="checkout-form" class="smart-form" novalidate="novalidate">\n\n<fieldset>\n    <div class="row">\n        <section class="col col-6">\n            <label class="input"> <i class="icon-prepend fa fa-user"></i>\n                <input type="text" name="fname" placeholder="First name">\n            </label>\n        </section>\n        <section class="col col-6">\n            <label class="input"> <i class="icon-prepend fa fa-user"></i>\n                <input type="text" name="lname" placeholder="Last name">\n            </label>\n        </section>\n    </div>\n\n    <div class="row">\n        <section class="col col-6">\n            <label class="input"> <i class="icon-prepend fa fa-envelope-o"></i>\n                <input type="email" name="email" placeholder="E-mail">\n            </label>\n        </section>\n        <section class="col col-6">\n            <label class="input"> <i class="icon-prepend fa fa-phone"></i>\n                <input type="tel" name="phone" placeholder="Phone" data-smart-masked-input="(999) 999-9999">\n            </label>\n        </section>\n    </div>\n</fieldset>\n\n<fieldset>\n<div class="row">\n<section class="col col-5">\n<label class="select">\n<select name="country">\n<option value="0" selected="" disabled="">Country</option>\n    <option value="{{country.key}}" ng-repeat="country in countries" >{{country.value}}</option>\n</select> <i></i> </label>\n</section>\n\n<section class="col col-4">\n    <label class="input">\n        <input type="text" name="city" placeholder="City">\n    </label>\n</section>\n\n<section class="col col-3">\n    <label class="input">\n        <input type="text" name="code" placeholder="Post code">\n    </label>\n</section>\n</div>\n\n<section>\n    <label for="address2" class="input">\n        <input type="text" name="address2" id="address2" placeholder="Address">\n    </label>\n</section>\n\n<section>\n    <label class="textarea">\n        <textarea rows="3" name="info" placeholder="Additional info"></textarea>\n    </label>\n</section>\n</fieldset>\n\n<fieldset>\n    <section>\n        <div class="inline-group">\n            <label class="radio">\n                <input type="radio" name="radio-inline" checked="">\n                <i></i>Visa</label>\n            <label class="radio">\n                <input type="radio" name="radio-inline">\n                <i></i>MasterCard</label>\n            <label class="radio">\n                <input type="radio" name="radio-inline">\n                <i></i>American Express</label>\n        </div>\n    </section>\n\n    <section>\n        <label class="input">\n            <input type="text" name="name" placeholder="Name on card">\n        </label>\n    </section>\n\n    <div class="row">\n        <section class="col col-10">\n            <label class="input">\n                <input type="text" name="card" placeholder="Card number" data-mask="9999-9999-9999-9999">\n            </label>\n        </section>\n        <section class="col col-2">\n            <label class="input">\n                <input type="text" name="cvv" placeholder="CVV2" data-mask="999">\n            </label>\n        </section>\n    </div>\n\n    <div class="row">\n        <label class="label col col-4">Expiration date</label>\n        <section class="col col-5">\n            <label class="select">\n                <select name="month">\n                    <option value="0" selected="" disabled="">Month</option>\n                    <option value="1">January</option>\n                    <option value="1">February</option>\n                    <option value="3">March</option>\n                    <option value="4">April</option>\n                    <option value="5">May</option>\n                    <option value="6">June</option>\n                    <option value="7">July</option>\n                    <option value="8">August</option>\n                    <option value="9">September</option>\n                    <option value="10">October</option>\n                    <option value="11">November</option>\n                    <option value="12">December</option>\n                </select> <i></i> </label>\n        </section>\n        <section class="col col-3">\n            <label class="input">\n                <input type="text" name="year" placeholder="Year" data-mask="2099">\n            </label>\n        </section>\n    </div>\n</fieldset>\n\n<footer>\n    <button type="submit" class="btn btn-primary">\n        Validate Form\n    </button>\n</footer>\n</form>\n'),a.put("build/modules/forms/directives/form-layouts/smart-comment-form.tpl.html",'<form action="/api/plug" method="post" id="comment-form" class="smart-form">\n    <header>\n        Comment form\n    </header>\n\n    <fieldset>\n        <div class="row">\n            <section class="col col-4">\n                <label class="label">Name</label>\n                <label class="input"> <i class="icon-append fa fa-user"></i>\n                    <input type="text" name="name">\n                </label>\n            </section>\n            <section class="col col-4">\n                <label class="label">E-mail</label>\n                <label class="input"> <i class="icon-append fa fa-envelope-o"></i>\n                    <input type="email" name="email">\n                </label>\n            </section>\n            <section class="col col-4">\n                <label class="label">Website</label>\n                <label class="input"> <i class="icon-append fa fa-globe"></i>\n                    <input type="url" name="url">\n                </label>\n            </section>\n        </div>\n\n        <section>\n            <label class="label">Comment</label>\n            <label class="textarea"> <i class="icon-append fa fa-comment"></i> <textarea rows="4"\n                                                                                         name="comment"></textarea>\n            </label>\n\n            <div class="note">\n                You may use these HTML tags and attributes: &lt;a href="" title=""&gt;, &lt;abbr title=""&gt;,\n                &lt;acronym title=""&gt;, &lt;b&gt;, &lt;blockquote cite=""&gt;, &lt;cite&gt;, &lt;code&gt;,\n                &lt;del datetime=""&gt;, &lt;em&gt;, &lt;i&gt;, &lt;q cite=""&gt;, &lt;strike&gt;, &lt;strong&gt;.\n            </div>\n        </section>\n    </fieldset>\n\n    <footer>\n        <button type="submit" name="submit" class="btn btn-primary">\n            Validate Form\n        </button>\n    </footer>\n\n    <div class="message">\n        <i class="fa fa-check fa-lg"></i>\n\n        <p>\n            Your comment was successfully added!\n        </p>\n    </div>\n</form>'),a.put("build/modules/forms/directives/form-layouts/smart-contacts-form.tpl.html",'<form action="/api/plug" method="post" id="contact-form" class="smart-form">\n    <header>Contacts form</header>\n\n    <fieldset>\n        <div class="row">\n            <section class="col col-6">\n                <label class="label">Name</label>\n                <label class="input">\n                    <i class="icon-append fa fa-user"></i>\n                    <input type="text" name="name" id="named">\n                </label>\n            </section>\n            <section class="col col-6">\n                <label class="label">E-mail</label>\n                <label class="input">\n                    <i class="icon-append fa fa-envelope-o"></i>\n                    <input type="email" name="email" id="emaild">\n                </label>\n            </section>\n        </div>\n\n        <section>\n            <label class="label">Subject</label>\n            <label class="input">\n                <i class="icon-append fa fa-tag"></i>\n                <input type="text" name="subject" id="subject">\n            </label>\n        </section>\n\n        <section>\n            <label class="label">Message</label>\n            <label class="textarea">\n                <i class="icon-append fa fa-comment"></i>\n                <textarea rows="4" name="message" id="message"></textarea>\n            </label>\n        </section>\n\n        <section>\n            <label class="checkbox"><input type="checkbox" name="copy" id="copy"><i></i>Send a copy to my\n                e-mail address</label>\n        </section>\n    </fieldset>\n\n    <footer>\n        <button type="submit" class="btn btn-primary">Validate Form</button>\n    </footer>\n\n    <div class="message">\n        <i class="fa fa-thumbs-up"></i>\n\n        <p>Your message was successfully sent!</p>\n    </div>\n</form>'),a.put("build/modules/forms/directives/form-layouts/smart-order-form.tpl.html",'<form id="order-form" class="smart-form" novalidate="novalidate">\n    <header>\n        Order services\n    </header>\n\n    <fieldset>\n        <div class="row">\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-user"></i>\n                    <input type="text" name="name" placeholder="Name">\n                </label>\n            </section>\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-briefcase"></i>\n                    <input type="text" name="company" placeholder="Company">\n                </label>\n            </section>\n        </div>\n\n        <div class="row">\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-envelope-o"></i>\n                    <input type="email" name="email" placeholder="E-mail">\n                </label>\n            </section>\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-phone"></i>\n                    <input type="tel" name="phone" placeholder="Phone" data-smart-masked-input="(999) 999-9999">\n                </label>\n            </section>\n        </div>\n    </fieldset>\n\n    <fieldset>\n        <div class="row">\n            <section class="col col-6">\n                <label class="select">\n                    <select name="interested">\n                        <option value="0" selected="" disabled="">Interested in</option>\n                        <option value="1">design</option>\n                        <option value="1">development</option>\n                        <option value="2">illustration</option>\n                        <option value="2">branding</option>\n                        <option value="3">video</option>\n                    </select> <i></i> </label>\n            </section>\n            <section class="col col-6">\n                <label class="select">\n                    <select name="budget">\n                        <option value="0" selected="" disabled="">Budget</option>\n                        <option value="1">less than 5000$</option>\n                        <option value="2">5000$ - 10000$</option>\n                        <option value="3">10000$ - 20000$</option>\n                        <option value="4">more than 20000$</option>\n                    </select> <i></i> </label>\n            </section>\n        </div>\n\n        <div class="row">\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-calendar"></i>\n                    <input type="text" name="startdate" id="startdate" data-smart-datepicker data-min-restrict="#finishdate" placeholder="Expected start date">\n                </label>\n            </section>\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-calendar"></i>\n                    <input type="text" name="finishdate" id="finishdate" data-smart-datepicker data-max-restrict="#startdate" placeholder="Expected finish date">\n                </label>\n            </section>\n        </div>\n\n        <section>\n            <div class="input input-file">\n                            <span class="button"><input id="file2" type="file" name="file2"\n                                                        onchange="this.parentNode.nextSibling.value = this.value">Browse</span><input\n                    type="text" placeholder="Include some files" readonly="">\n            </div>\n        </section>\n\n        <section>\n            <label class="textarea"> <i class="icon-append fa fa-comment"></i>\n                <textarea rows="5" name="comment" placeholder="Tell us about your project"></textarea>\n            </label>\n        </section>\n    </fieldset>\n    <footer>\n        <button type="submit" class="btn btn-primary">\n            Validate Form\n        </button>\n    </footer>\n</form>\n'),a.put("build/modules/forms/directives/form-layouts/smart-registration-form.tpl.html",'<form id="smart-form-register" class="smart-form">\n    <header>\n        Registration form\n    </header>\n\n    <fieldset>\n        <section>\n            <label class="input"> <i class="icon-append fa fa-user"></i>\n                <input type="text" name="username" placeholder="Username">\n                <b class="tooltip tooltip-bottom-right">Needed to enter the website</b> </label>\n        </section>\n\n\n        <section>\n            <label class="input"> <i class="icon-append fa fa-envelope-o"></i>\n                <input type="email" name="email" placeholder="Email address">\n                <b class="tooltip tooltip-bottom-right">Needed to verify your account</b> </label>\n        </section>\n\n        <section>\n            <label class="input"> <i class="icon-append fa fa-lock"></i>\n                <input type="password" name="password" placeholder="Password" id="password">\n                <b class="tooltip tooltip-bottom-right">Don\'t forget your password</b> </label>\n        </section>\n\n        <section>\n            <label class="input"> <i class="icon-append fa fa-lock"></i>\n                <input type="password" name="passwordConfirm" placeholder="Confirm password">\n                <b class="tooltip tooltip-bottom-right">Don\'t forget your password</b> </label>\n        </section>\n    </fieldset>\n\n    <fieldset>\n        <div class="row">\n            <section class="col col-6">\n                <label class="input">\n                    <input type="text" name="firstname" placeholder="First name">\n                </label>\n            </section>\n            <section class="col col-6">\n                <label class="input">\n                    <input type="text" name="lastname" placeholder="Last name">\n                </label>\n            </section>\n        </div>\n\n        <div class="row">\n            <section class="col col-6">\n                <label class="select">\n                    <select name="gender">\n                        <option value="0" selected="" disabled="">Gender</option>\n                        <option value="1">Male</option>\n                        <option value="2">Female</option>\n                        <option value="3">Prefer not to answer</option>\n                    </select> <i></i> </label>\n            </section>\n            <section class="col col-6">\n                <label class="input"> <i class="icon-append fa fa-calendar"></i>\n                    <input type="text" name="request" placeholder="Request activation on"\n                           data-smart-datepicker data-dateformat=\'dd/mm/yy\'>\n                </label>\n            </section>\n        </div>\n\n        <section>\n            <label class="checkbox">\n                <input type="checkbox" name="subscription" id="subscription">\n                <i></i>I want to receive news and special offers</label>\n            <label class="checkbox">\n                <input type="checkbox" name="terms" id="terms">\n                <i></i>I agree with the Terms and Conditions</label>\n        </section>\n    </fieldset>\n    <footer>\n        <button type="submit" class="btn btn-primary">\n            Validate Form\n        </button>\n    </footer>\n</form>'),a.put("build/modules/forms/directives/form-layouts/smart-review-form.tpl.html",'<form id="review-form" class="smart-form">\n    <header>\n        Review form\n    </header>\n\n    <fieldset>\n        <section>\n            <label class="input"> <i class="icon-append fa fa-user"></i>\n                <input type="text" name="name" id="name" placeholder="Your name">\n            </label>\n        </section>\n\n        <section>\n            <label class="input"> <i class="icon-append fa fa-envelope-o"></i>\n                <input type="email" name="email" id="email" placeholder="Your e-mail">\n            </label>\n        </section>\n\n        <section>\n            <label class="label"></label>\n            <label class="textarea"> <i class="icon-append fa fa-comment"></i>\n                <textarea rows="3" name="review" id="review" placeholder="Text of the review"></textarea>\n            </label>\n        </section>\n\n        <section>\n            <div class="rating">\n                <input type="radio" name="quality" id="quality-5">\n                <label for="quality-5"><i class="fa fa-star"></i></label>\n                <input type="radio" name="quality" id="quality-4">\n                <label for="quality-4"><i class="fa fa-star"></i></label>\n                <input type="radio" name="quality" id="quality-3">\n                <label for="quality-3"><i class="fa fa-star"></i></label>\n                <input type="radio" name="quality" id="quality-2">\n                <label for="quality-2"><i class="fa fa-star"></i></label>\n                <input type="radio" name="quality" id="quality-1">\n                <label for="quality-1"><i class="fa fa-star"></i></label>\n                Quality of the product\n            </div>\n\n            <div class="rating">\n                <input type="radio" name="reliability" id="reliability-5">\n                <label for="reliability-5"><i class="fa fa-star"></i></label>\n                <input type="radio" name="reliability" id="reliability-4">\n                <label for="reliability-4"><i class="fa fa-star"></i></label>\n                <input type="radio" name="reliability" id="reliability-3">\n                <label for="reliability-3"><i class="fa fa-star"></i></label>\n                <input type="radio" name="reliability" id="reliability-2">\n                <label for="reliability-2"><i class="fa fa-star"></i></label>\n                <input type="radio" name="reliability" id="reliability-1">\n                <label for="reliability-1"><i class="fa fa-star"></i></label>\n                Reliability of the product\n            </div>\n\n            <div class="rating">\n                <input type="radio" name="overall" id="overall-5">\n                <label for="overall-5"><i class="fa fa-star"></i></label>\n                <input type="radio" name="overall" id="overall-4">\n                <label for="overall-4"><i class="fa fa-star"></i></label>\n                <input type="radio" name="overall" id="overall-3">\n                <label for="overall-3"><i class="fa fa-star"></i></label>\n                <input type="radio" name="overall" id="overall-2">\n                <label for="overall-2"><i class="fa fa-star"></i></label>\n                <input type="radio" name="overall" id="overall-1">\n                <label for="overall-1"><i class="fa fa-star"></i></label>\n                Overall rating\n            </div>\n        </section>\n    </fieldset>\n    <footer>\n        <button type="submit" class="btn btn-primary">\n            Validate Form\n        </button>\n    </footer>\n</form>')
}])});
define('includes',["auth/module","auth/models/User","layout/module","layout/actions/minifyMenu","layout/actions/toggleMenu","layout/actions/fullScreen","layout/actions/resetWidgets","layout/actions/resetWidgets","layout/actions/searchMobile","layout/directives/demo/demoStates","layout/directives/smartInclude","layout/directives/smartDeviceDetect","layout/directives/smartFastClick","layout/directives/smartLayout","layout/directives/smartSpeech","layout/directives/smartRouterAnimationWrap","layout/directives/smartFitAppView","layout/directives/radioToggle","layout/directives/dismisser","layout/directives/smartMenu","layout/directives/bigBreadcrumbs","layout/directives/stateBreadcrumbs","layout/directives/smartPageTitle","layout/directives/hrefVoid","layout/service/SmartCss","modules/widgets/directives/widgetGrid","modules/widgets/directives/jarvisWidget","dashboard/module","components/boot/module","components/boot/directives/unreadMessagesCount","components/boot/models/InboxConfig","components/boot/models/CalendarEvent","components/language/Language","components/language/languageSelector","components/language/language-controller","components/projects/Project","components/projects/recentProjects","components/activities/activities-controller","components/activities/activities-dropdown-toggle-directive","components/activities/activities-service","components/shortcut/shortcut-directive","components/todo/TodoCtrl","components/todo/models/Todo","components/todo/directives/todoList","components/chat/module","modules/graphs/module","modules/tables/module","modules/forms/module","modules/ui/module","modules/widgets/module","modules/maps/module","modules/app-views/module","modules/misc/module","modules/smart-admin/module","modules/lazy-states/module","smart-templates"],function(){"use strict"});
window.name="NG_DEFER_BOOTSTRAP!",define('main',["require","jquery","angular","domReady","bootstrap","appConfig","app","includes"],function(a,b,c,d){b.sound_path=appConfig.sound_path,b.sound_on=appConfig.sound_on,d(function(a){c.bootstrap(a,["app"]),c.resumeBootstrap()})});
