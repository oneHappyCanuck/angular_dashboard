define(['modules/lazy-states/module', 'lodash'], function(module, _){

    "use strict";

    module.provider('lazyStates', function($futureStateProvider) {

        var loadAll = function() {

            $futureStateProvider.futureState({
                'stateName': 'app.inbox.folder',
                'url': '/inbox',
                'type': 'ocLazyLoad',
                'module': 'app.inbox'
            });

            $futureStateProvider.futureState({
                'stateName': 'app.calendar',
                'url': '/calendar',
                'type': 'ocLazyLoad',
                'module': 'app.calendar'
            });
        };

        return {
            loadAll: loadAll,
            $get: function() {
                return this;
            }
        }

    });
});